-libraryjars 'C:\Program Files\Java\jre1.8.0_131\lib\rt.jar'
-dontoptimize
-dontpreverify
-dontwarn **
-keepattributes *Annotation*,Signature
-dontshrink

-keep class com.quequiere.pixelquest.Pixelquest { *; }
-keep class com.quequiere.pixelquest.api.PixelQuestApi { *; }

-keep class com.quequiere.pixelquest.Location { *; }
-keep class com.pixelmonmod.pixlemon.battles.controller.Experience { *; }
-keep class com.pixelmonmod.pixlemon.battles.controller.Experience_ { *; }


-keepclassmembers class com.quequiere.pixelquest.dungeon.** { <fields>; }
-keep class com.quequiere.pixelquest.dungeon.** { *; }





-keep class com.quequiere.pixelquest.effect.PotionHandler { *; }

-keep class  com.quequiere.pixelquest.map.MapPalette { *; }

-keep class com.quequiere.pixelquest.travel.entity.TravellerEntity { *; }
-keep class com.quequiere.pixelquest.travel.entity.AIFlyingPersistentModified { *; }

-keepclassmembers class com.quequiere.pixelquest.travel.Travel { <fields>; }
-keepclassmembers class com.quequiere.pixelquest.pixelmon.** { <fields>; }

-keep class com.quequiere.pixelquest.tools.** { *; }

-keepclassmembers class com.quequiere.pixelquest.npc.** { <fields>; }
-keepclassmembers class com.quequiere.pixelquest.player.** {<fields>;}
-keep class com.quequiere.pixelquest.player.PlayerProfile {*;}
-keep class com.quequiere.pixelquest.player.quest.QuestPlayerProfile {*;}
-keep class com.quequiere.pixelquest.player.experience.ExperienceProfile {*;}


-keepclassmembers class com.quequiere.pixelquest.quest.Quest { <fields>; }
-keepnames class com.quequiere.pixelquest.quest.Quest 
-keep class com.quequiere.pixelquest.quest.Quest {*;}

-keepclassmembers class com.quequiere.pixelquest.quest.Goal { <fields>; }
-keepclassmembers class com.quequiere.pixelquest.quest.goal.type.** { <fields>; }
-keepnames class com.quequiere.pixelquest.quest.goal.Goal
-keepnames class com.quequiere.pixelquest.quest.goal.type.** 

-keepclassmembers class com.quequiere.pixelquest.quest.QuestTimerBeforeRepeatConfig { <fields>; }
-keepclassmembers class com.quequiere.pixelquest.quest.QuestTimerBeforeRepeat { <fields>; }


-keepclassmembers class com.quequiere.pixelquest.quest.reward.Reward { <fields>; }
-keepclassmembers class com.quequiere.pixelquest.quest.reward.type.** { <fields>; }
-keepnames class com.quequiere.pixelquest.quest.reward.Reward
-keepnames class com.quequiere.pixelquest.quest.reward.type.** 


-keep class com.quequiere.pixelquest.quest.goal.** {*;}
-keep class com.quequiere.pixelquest.quest.reward.** {*;}



-keep class com.quequiere.pixelquest.command.** { *; }
-keep class com.quequiere.pixelquest.event.** { *; }




-keep public class net.minecraft.** {

}

-keepclassmembers enum  * {*;}