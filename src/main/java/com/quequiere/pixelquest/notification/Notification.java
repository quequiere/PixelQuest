package com.quequiere.pixelquest.notification;

import java.util.ArrayList;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.client.gui.custom.overlays.GraphicDisplayTypes;
import com.pixelmonmod.pixelmon.comm.packetHandlers.customOverlays.CustomNoticePacket;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Notification
{
	private ArrayList<String> text;
	private long startedDisplay;
	private long duration;
	private NotificationType type;
	
	private EnumPokemon pokemon;
	private Item item;
	
	
	public static void addNotification(EntityPlayerMP p, String text, long duration)
	{
		
		ArrayList<String> s = new ArrayList<String>();
		s.add(text);
		NotificationHandler.addNotification(p, new Notification(s, duration));
	}
	
	public static void addNotification(EntityPlayerMP p, long duration,String ...text)
	{
		
		ArrayList<String> s = new ArrayList<String>();
		for(String ls:text)
		{
			s.add(ls);
		}
		NotificationHandler.addNotification(p, new Notification(s, duration));
	}
	
	
	public static void addNotification(EntityPlayerMP p, ArrayList<String> text, long duration)
	{
		NotificationHandler.addNotification(p, new Notification(text, duration));
	}
	
	public static void addNotification(EntityPlayerMP p, ArrayList<String> text, long duration, NotificationType type, Object o)
	{
		NotificationHandler.addNotification(p, new Notification(text, duration,type,o));
	}

	private Notification(ArrayList<String> text, long duration)
	{
		if(duration<1000)
		{
			duration = duration*1000;
		}
		this.text = text;
		this.duration = duration;
		this.type = NotificationType.none;
	}

	private Notification(ArrayList<String> text, long duration, NotificationType type, Object o)
	{
		this.text = text;
		this.duration = duration;
		this.type = type;

		if (type.equals(NotificationType.item))
		{
			if(o instanceof Item)
			{
				this.item=(Item) o;
			}
			else
			{
				type=NotificationType.none;
			}
		}
		else if (type.equals(NotificationType.pokemon))
		{
			if(o instanceof EnumPokemon)
			{
				this.pokemon=(EnumPokemon) o;
			}
			else
			{
				type=NotificationType.none;
			}
		}
	}
	
	public boolean isFinished()
	{
		long diff = System.currentTimeMillis()-startedDisplay;
		if(diff>duration)
		{
			return true;
		}
		
		return false;
	}

	public void send(EntityPlayerMP p)
	{
		CustomNoticePacket c = new CustomNoticePacket(this.text);
		c.setEnabled(true);

		if(this.type.equals(NotificationType.item))
		{
			 c.setItem3D(new ItemStack(this.item).getDisplayName(), GraphicDisplayTypes.LeftOnly);
		}
		else if(this.type.equals(NotificationType.pokemon))
		{
			c.setPokemon3D(pokemon, GraphicDisplayTypes.LeftOnly);
		}
		
		Pixelmon.network.sendTo(c, p);
		startedDisplay=System.currentTimeMillis();
	}

	public static void clear(EntityPlayerMP p)
	{
		CustomNoticePacket c = new CustomNoticePacket();
		c.setEnabled(false);
		Pixelmon.network.sendTo(c, p);
	}
}
