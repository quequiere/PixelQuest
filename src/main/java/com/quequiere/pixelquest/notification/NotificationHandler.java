package com.quequiere.pixelquest.notification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.quequiere.pixelquest.Tools;

import net.minecraft.entity.player.EntityPlayerMP;

public class NotificationHandler extends TimerTask
{
	public static NotificationHandler notifHandler;
	private static Timer timer = new Timer();
	
	
	private static HashMap<String, ArrayList<Notification>> cache = new HashMap<String, ArrayList<Notification>>();
	private static HashMap<String, Notification> current = new HashMap<String, Notification>();
	
	public static void addNotification(EntityPlayerMP p,Notification n)
	{
		ArrayList<Notification> pn;
		
		if(cache.get(p.getName())==null)
		{
			pn = new ArrayList<Notification>();
			cache.put(p.getName(), pn);
		}
		else
		{
			pn = cache.get(p.getName());
		}
		
		pn.add(n);
	}

	@Override
	public void run()
	{
		for (EntityPlayerMP p : Tools.getAllPlayers())
		{
			if(current.containsKey(p.getName()))
			{
				Notification notif = current.get(p.getName());
				if(notif.isFinished())
				{
					current.remove(p.getName());
					Notification.clear(p);
				}
			}
			
			
			if(cache.containsKey(p.getName()) && !current.containsKey(p.getName()))
			{
				ArrayList<Notification> notifs = cache.get(p.getName());
				if(notifs.size()>0)
				{
					current.put(p.getName(), notifs.get(0));
					notifs.get(0).send(p);
					notifs.remove(0);
				}
			}
			
		}
	
	}
	
	
	public static void startHandler()
	{
		notifHandler = new NotificationHandler();
		timer.schedule(notifHandler, 1 * 1000,1*1000);
	}

}
