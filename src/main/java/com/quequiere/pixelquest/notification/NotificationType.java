package com.quequiere.pixelquest.notification;

public enum NotificationType
{
	pokemon,
	item,
	none;
}
