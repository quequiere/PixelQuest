package com.quequiere.pixelquest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.tools.ItemStackDeserializer;
import com.quequiere.pixelquest.tools.ItemStackSerializer;

import net.minecraft.item.ItemStack;
import scala.tools.nsc.backend.icode.analysis.TypeFlowAnalysis.MethodTFA.Gen;

public class GeneralConfig
{

	private static File folder = new File(Pixelquest.folder.getAbsolutePath());

	public Location dungeonHub;

	public Location getDungeonHub()
	{
		return dungeonHub;
	}

	public void setDungeonHub(Location dungeonHub)
	{
		this.dungeonHub = dungeonHub;
		this.save();
	}

	public String toJson()
	{
		ItemStackSerializer as = new ItemStackSerializer();
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, as).create();
		return gson.toJson(this);
	}

	public static GeneralConfig fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, new ItemStackDeserializer()).create();
		return gson.fromJson(s, GeneralConfig.class);
	}

	public void save()
	{
		Writer writer = null;
		try
		{

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/generalConfig.json");
			if (!file.exists())
			{
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public static void reloadFile()
	{
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/generalConfig.json");
		
		if (f.exists())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				GeneralConfig d = GeneralConfig.fromJson(everything);
				if (d != null)
				{
					Pixelquest.generalConfig=d;
				}
				else
				{
					System.out.println("Fail load dungeon: " + f.getAbsolutePath());
				}
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			System.out.println("Cant find: " + f.getAbsolutePath());
			try
			{
				f.createNewFile();
				Pixelquest.generalConfig=new GeneralConfig();
				Pixelquest.generalConfig.save();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
