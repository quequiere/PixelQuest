package com.quequiere.pixelquest;

import java.io.File;
import java.util.ArrayList;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.storage.PokeballManager;
import com.quequiere.permissionbridge.PermissionBridge;
import com.quequiere.pixelquest.command.AddItemToShopCommand;
import com.quequiere.pixelquest.command.AddQuestToNpcCommand;
import com.quequiere.pixelquest.command.CreateQuestCommand;
import com.quequiere.pixelquest.command.CustomMapCommand;
import com.quequiere.pixelquest.command.GiveBonusCommand;
import com.quequiere.pixelquest.command.GiveDiscountCardCommand;
import com.quequiere.pixelquest.command.GiveExpCommand;
import com.quequiere.pixelquest.command.GiveLicenseCommand;
import com.quequiere.pixelquest.command.GiveModifierCommand;
import com.quequiere.pixelquest.command.GivePotionCommand;
import com.quequiere.pixelquest.command.NotificationCommand;
import com.quequiere.pixelquest.command.QuestAdminDisplayCommand;
import com.quequiere.pixelquest.command.QuestCommand;
import com.quequiere.pixelquest.command.QuestHelpCommand;
import com.quequiere.pixelquest.command.RemoveItemToShopCommand;
import com.quequiere.pixelquest.command.ShowProfle;
import com.quequiere.pixelquest.command.TravelCommand;
import com.quequiere.pixelquest.command.TravelCreateCommand;
import com.quequiere.pixelquest.dungeon.command.AddSpawnLocationCommand;
import com.quequiere.pixelquest.dungeon.command.CreateDungeonCommand;
import com.quequiere.pixelquest.dungeon.command.DungeonCommand;
import com.quequiere.pixelquest.dungeon.command.InitZoneCommand;
import com.quequiere.pixelquest.dungeon.command.QueueDungeonCommand;
import com.quequiere.pixelquest.dungeon.command.CreateCustomPokemonCommand;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.handler.DungeonDelayedRunnable;
import com.quequiere.pixelquest.dungeon.handler.DungeonQueueCheckerRunnable;
import com.quequiere.pixelquest.dungeon.handler.DungeonSpawnerRunnable;
import com.quequiere.pixelquest.effect.PotionHandler;
import com.quequiere.pixelquest.event.NpcEvent;
import com.quequiere.pixelquest.event.PixelmonForgeEvent;
import com.quequiere.pixelquest.event.PlayerGeneralEvent;
import com.quequiere.pixelquest.event.PokemonEvent;
import com.quequiere.pixelquest.event.ServerTickEvent;
import com.quequiere.pixelquest.event.WorldEvent;
import com.quequiere.pixelquest.event.InteractNpcEvent;
import com.quequiere.pixelquest.notification.NotificationHandler;
import com.quequiere.pixelquest.npc.NpcGestionnaire;
import com.quequiere.pixelquest.npc.NpcParticleHandler;
import com.quequiere.pixelquest.packet.injector.PacketInjector;
import com.quequiere.pixelquest.pixelmon.PixelmonModifier;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.TeamProfile;
import com.quequiere.pixelquest.player.experience.ExperienceProfile;
import com.quequiere.pixelquest.player.experience.Rank;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.QuestStarterLocation;
import com.quequiere.pixelquest.quest.QuestTimerBeforeRepeatConfig;
import com.quequiere.pixelquest.quest.QuestTimerHandler;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.GoalLocationHandler;
import com.quequiere.pixelquest.quest.goal.type.KillPokemon;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.security.SecurityChecker;
import com.quequiere.pixelquest.travel.Travel;
import com.quequiere.pixelquest.travel.entity.TravellerEntity;

import net.minecraft.command.CommandHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;

@Mod(modid = Pixelquest.MODID, version = Pixelquest.VERSION, acceptableRemoteVersions = "*")
public class Pixelquest
{
	public static final String MODID = "PixelQuest";
	public static final String VERSION = "1.0";
	public static File folder;

	public static Pixelmon pixelmon;
	public static ArrayList<ICommand> commands;
	
	public static GeneralConfig generalConfig;

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		pixelmon = Pixelmon.instance;
		this.folder = new File("./config/" + MODID);
		this.folder.mkdirs();

		System.out.println("Test for pixelmonmod loaded: " + this.pixelmon);

		GeneralConfig.reloadFile();
		Quest.loadAllQuest();
		Travel.loadAllTravel();

		QuestTimerBeforeRepeatConfig.load();

		NpcGestionnaire.instance = NpcGestionnaire.reloadFile();

		if (NpcGestionnaire.instance == null)
		{
			NpcGestionnaire.instance = new NpcGestionnaire();
			NpcGestionnaire.instance.save();
			System.out.println("Created new Npc gestionary file !");
		}
		
		
		DungeonConfig.reloadFile();
		
		CustomPixelmonConfig.config=CustomPixelmonConfig.reloadFile();
		if(CustomPixelmonConfig.config==null)
		{
			CustomPixelmonConfig.config=new CustomPixelmonConfig();
			CustomPixelmonConfig.config.save();
		}

	}

	@EventHandler
	public void serverLoad(FMLServerStartingEvent event)
	{
		commands = new ArrayList<ICommand>();
		commands.add(new QuestCommand());
		commands.add(new CreateQuestCommand());
		commands.add(new NotificationCommand());
		commands.add(new AddQuestToNpcCommand());
		commands.add(new GiveExpCommand());
		commands.add(new ShowProfle());
		commands.add(new QuestAdminDisplayCommand());
		commands.add(new GivePotionCommand());
		commands.add(new GiveModifierCommand());
		commands.add(new TravelCreateCommand());
		commands.add(new TravelCommand());
		commands.add(new QuestHelpCommand());
		commands.add(new GiveLicenseCommand());
		commands.add(new AddItemToShopCommand());
		commands.add(new RemoveItemToShopCommand());
		commands.add(new GiveDiscountCardCommand());
		commands.add(new CustomMapCommand());
		commands.add(new CreateCustomPokemonCommand());
		commands.add(new CreateDungeonCommand());
		commands.add(new AddSpawnLocationCommand());
		commands.add(new DungeonCommand());
		commands.add(new InitZoneCommand());
		commands.add(new QueueDungeonCommand());
		commands.add(new GiveBonusCommand());
		
		for (ICommand c : commands)
		{
			event.registerServerCommand(c);
		}

		pixelmon.EVENT_BUS.register(new NpcEvent());
		pixelmon.EVENT_BUS.register(new PokemonEvent());
		MinecraftForge.EVENT_BUS.register(new PlayerGeneralEvent());
		MinecraftForge.EVENT_BUS.register(new InteractNpcEvent());
		MinecraftForge.EVENT_BUS.register(new ServerTickEvent());
		MinecraftForge.EVENT_BUS.register(new PixelmonForgeEvent());
		MinecraftForge.EVENT_BUS.register(new WorldEvent());

		NotificationHandler.startHandler();
		QuestTimerHandler.startHandler();
		GoalLocationHandler.startHandler();
		SecurityChecker.startHandler();
		QuestStarterLocation.startHandler();
		NpcParticleHandler.startHandler();

		// EntityRegistry.registerModEntity(TravellerEntity.class, "traveler",
		// 300, this, 64, 1, true);

	}

	@EventHandler
	public void endLoad(FMLServerStartedEvent event)
	{
		PacketInjector.inject();
		PixelmonConfig.giveStarter = false;
		PokeballManager.disableStarterMenu = true;
		Quest.rattachObject();
		
		PixelmonModifier.loadAllProfiles();
		
		ServerTickEvent.tasks.add(new DungeonSpawnerRunnable());
		ServerTickEvent.tasks.add(new DungeonQueueCheckerRunnable());
		ServerTickEvent.tasks.add(new DungeonDelayedRunnable());
		
	}

	@Deprecated
	public void registerDefaultQuestTest()
	{

		ArrayList<Goal> goals = new ArrayList<Goal>();
		KillPokemon kill = new KillPokemon(2);

		ArrayList<EnumPokemon> plist = new ArrayList<EnumPokemon>();
		plist.add(EnumPokemon.Pikachu);
		plist.add(EnumPokemon.Seel);
		kill.setPokemons(plist);

		goals.add(kill);
		Quest.createQuest("Test de quete", goals, new ArrayList<Reward>());
	}

	public static boolean hasPermission(ICommandSender p, String perm)
	{

		if(p instanceof MinecraftServer)
		{
			return true;
		}
		
		if (p.getName().equals("quequiere"))
		{
			return true;
		}

		if (PermissionBridge.plugin == null)
		{
			return false;
		}
		else
		{
			return PermissionBridge.hasPerm(perm, p.getName());
		}
	}

	public static String getProfileName(String pname)
	{
		EntityPlayerMP p = Tools.getPlayer(pname);
		PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), true);
		
		String s ="";
		
		if(pp instanceof TeamProfile)
		{
			s="[Dungeon]";
		}
		else
		{
			ExperienceProfile profileexp = pp.getExpProfile();
			int lvl = profileexp.getTotalLvl();
			s = "[" + lvl + "] " + Rank.getRankByLvl(profileexp.getTotalLvl()).name();
		}
		
	

		return s;
	}
}
