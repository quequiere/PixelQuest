package com.quequiere.pixelquest;

import javax.vecmath.Vector3d;

import com.quequiere.pixelquest.tools.TeleportUtility;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Teleporter;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;

public class Location extends Vector3d
{
	private String worldName;
	
	public Location(double x,double y,double z,World w)
	{
		super(x,y,z);
		this.worldName=w.getWorldInfo().getWorldName();
	}
	
	public Location(BlockPos pos,World w)
	{
		super(pos.getX(),pos.getY(),pos.getZ());
		this.worldName=w.getWorldInfo().getWorldName();
	}
	
	public void teleportPlayer(EntityPlayerMP p)
	{
		if(p.dimension!=this.getWorld().provider.getDimension())
		{
			//WorldServer ws = (WorldServer) this.getWorld();
			//ws.spawnEntityInWorld(p);
			//p.setWorld(ws);
			BlockPos pos = new BlockPos(this.getX(), this.getY(), this.getZ());
			TeleportUtility.transferPlayerToDimension(p, this.getWorld().provider.getDimension(), pos);
		}
		else
		{
			p.connection.setPlayerLocation(this.getX(),this.getY(),this.getZ(), p.rotationYaw,  p.rotationPitch);
		}
		
		
		//ws.updateEntityWithOptionalForce(p, false);
		
		
		
		//p.dimension=this.getWorld().provider.getDimension();
		//p.changeDimension(this.getWorld().provider.getDimension());
		
		//Chunk c = this.getWorld().getChunkFromBlockCoords(new BlockPos(this.getX(), this.getY(), this.getZ()));
		//c.onChunkLoad();
		
		
	}
	
	public Location(EntityPlayerMP p)
	{
		super(p.posX,p.posY,p.posZ);
		this.worldName=p.worldObj.getWorldInfo().getWorldName();
	}

	public String getWorldName()
	{
		return worldName;
	}
	
	public World getWorld()
	{
		return Tools.getWorldByName(this.getWorldName());
	}
	
	public static Location fromBlockPos(BlockPos bp,World w)
	{
		return new Location(bp.getX(), bp.getY(), bp.getZ(), w);
	}
	
	public double getDistance(double p_getDistance_1_, double p_getDistance_2_, double p_getDistance_3_)
	{
		double lvt_4_1_ = getX() - p_getDistance_1_;
		double lvt_6_1_ = getY() - p_getDistance_2_;
		double lvt_8_1_ = getZ() - p_getDistance_3_;

		return Math.sqrt(lvt_4_1_ * lvt_4_1_ + lvt_6_1_ * lvt_6_1_ + lvt_8_1_ * lvt_8_1_);
	}
	
	public double getDistance(Location l)
	{
		return this.getDistance(l.getX(), l.getY(), l.getZ());
	}
	

}
