package com.quequiere.pixelquest.pixelmon.modifier;

public class PixelmonSizeModifier
{
	private float size;
	
	public PixelmonSizeModifier(float size)
	{
		this.size = size;
	}

	public float getSize()
	{
		return size;
	}

	public void setSize(float size)
	{
		this.size = size;
	}
	
	
}
