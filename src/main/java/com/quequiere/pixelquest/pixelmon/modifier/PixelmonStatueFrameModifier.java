package com.quequiere.pixelquest.pixelmon.modifier;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;

public class PixelmonStatueFrameModifier
{
	private boolean active;
	private int maxFrame = 39;

	public PixelmonStatueFrameModifier(boolean active, int maxFrame)
	{
		this.active = active;
		this.maxFrame = maxFrame;
	}

	public int getMaxFrame()
	{
		return maxFrame;
	}

	public void setMaxFrame(int maxFrame)
	{
		this.maxFrame = maxFrame;
	}

	public boolean isActive()
	{
		return active;
	}

	public void setActive(boolean active)
	{
		this.active = active;
	}

	public void update(EntityStatue pokemon)
	{
		int current = pokemon.getAnimationFrame();
		if (current < this.getMaxFrame())
		{
			current++;
		}
		else
		{
			current = 0;
		}

		pokemon.setAnimationFrame(current);

	}

}
