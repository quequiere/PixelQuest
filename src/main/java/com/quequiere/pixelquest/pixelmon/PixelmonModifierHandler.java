package com.quequiere.pixelquest.pixelmon;

import java.util.HashMap;
import java.util.UUID;

import com.pixelmonmod.pixelmon.entities.pixelmon.Entity3HasStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class PixelmonModifierHandler
{
	public static HashMap<UUID, PixelmonModifier> pixelmonModifier = new HashMap<UUID, PixelmonModifier>();

	public static PixelmonModifier getPixelmonModifierProfile(Entity3HasStats pokemon)
	{
		return getPixelmonModifierProfile(pokemon, false);
	}

	public static PixelmonModifier getPixelmonModifierProfile(Entity3HasStats pokemon, boolean create)
	{
		if (pixelmonModifier.containsKey(pokemon.getPersistentID()))
		{
			return pixelmonModifier.get(pokemon.getPersistentID());
		}
		else
		{
			PixelmonModifier pokem = PixelmonModifier.loadPixelmonProfile(pokemon.getPersistentID());

			if (pokem != null)
			{
				pixelmonModifier.put(pokemon.getPersistentID(), pokem);
			}
			else if (create)
			{
				pokem = new PixelmonModifier(pokemon);
				pokem.save();
				pixelmonModifier.put(pokemon.getPersistentID(), pokem);
			}

			return pokem;
		}
	}

	public static void tryToModifyWithItemStack(ItemStack is, Entity3HasStats pokemon, EntityPlayerMP p)
	{
		if (is.getItem().equals(Items.PAPER))
		{
			if (is.getDisplayName().contains("Size modifier"))
			{
				NBTTagCompound c = is.getTagCompound();
				NBTTagCompound d = c.getCompoundTag("display");
				if (d.hasKey("Lore"))
				{
					NBTTagList nbttaglist3 = d.getTagList("Lore", 8);
					if (!(nbttaglist3.hasNoTags()))
					{
						for (int l1 = 0; l1 < nbttaglist3.tagCount(); ++l1)
						{
							String s = nbttaglist3.getStringTagAt(l1);
							if (s.contains("Size:"))
							{
								String floatString = s.split(":")[1];
								float size = Float.parseFloat(floatString);
								PixelmonModifier modifier = PixelmonModifierHandler.getPixelmonModifierProfile(pokemon, true);
								modifier.getSizeModifier(true).setSize(size);
								modifier.save();
								pokemon.setPixelmonScale(size);
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You successfuly apply a new size !"));
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Recall to apply"));
								is.stackSize--;
							}
						}
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl2 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
					}
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl1 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
				}

			}
			else if (is.getDisplayName().contains("FrameAnimation modifier"))
			{
				if (!(pokemon instanceof EntityStatue))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You can't apply this on a non statue pokemon !"));
					return;
				}

				NBTTagCompound c = is.getTagCompound();
				NBTTagCompound d = c.getCompoundTag("display");
				if (d.hasKey("Lore"))
				{
					NBTTagList nbttaglist3 = d.getTagList("Lore", 8);
					if (!(nbttaglist3.hasNoTags()))
					{
						boolean animated = false;
						int maxframe = -1;
						for (int l1 = 0; l1 < nbttaglist3.tagCount(); ++l1)
						{
							String s = nbttaglist3.getStringTagAt(l1);

							if (s.contains("Animation:"))
							{
								String parsed = s.split(":")[1];

								if (parsed.equals("true"))
								{
									animated = true;
								}
								else if (parsed.equals("false"))
								{
									animated = false;
								}
								else
								{
									p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Can't identity boolean frame type"));
									return;
								}

							}
							else if (s.contains("FrameMax:"))
							{
								String parsed = s.split(":")[1];
								try
								{
									maxframe = Integer.parseInt(parsed);
								}
								catch(NumberFormatException e)
								{
									p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Can't identity maxframe type"));
									return;
								}
							}
						}
						
						if(maxframe<=0)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Max frame can't be 0 or less"));
							return;
						}

						PixelmonModifier modifier = PixelmonModifierHandler.getPixelmonModifierProfile(pokemon, true);
						modifier.getPixelmonStatueFrameModifier(true).setActive(animated);
						modifier.getPixelmonStatueFrameModifier(true).setMaxFrame(maxframe);
						modifier.save();

						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You successfuly apply a frame type !"));
						is.stackSize--;
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl2 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
					}
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl1 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
				}

			}
		}
	}

}
