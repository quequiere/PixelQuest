package com.quequiere.pixelquest.pixelmon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity3HasStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.pixelmon.modifier.PixelmonSizeModifier;
import com.quequiere.pixelquest.pixelmon.modifier.PixelmonStatueFrameModifier;

public class PixelmonModifier
{
	private UUID pokemonId;
	private PixelmonSizeModifier sizeModifier;
	private PixelmonStatueFrameModifier statueFrameModifier;

	public PixelmonModifier(Entity3HasStats p)
	{
		this.pokemonId = p.getPersistentID();
	}

	
	public PixelmonStatueFrameModifier getPixelmonStatueFrameModifier()
	{
		return getPixelmonStatueFrameModifier(false);
	}
	
	public PixelmonStatueFrameModifier getPixelmonStatueFrameModifier(boolean create)
	{
		if(this.statueFrameModifier==null && create)
		{
			this.statueFrameModifier=new PixelmonStatueFrameModifier(true,39);
		}
		return statueFrameModifier;
	}
	
	public PixelmonSizeModifier getSizeModifier()
	{
		return getSizeModifier(false);
	}
	
	public PixelmonSizeModifier getSizeModifier(boolean create)
	{
		if(this.sizeModifier==null && create)
		{
			this.sizeModifier=new PixelmonSizeModifier(1);
		}
		return sizeModifier;
	}

	public void setSizeModifier(PixelmonSizeModifier sizeModifier)
	{
		this.sizeModifier = sizeModifier;
		this.save();
	}

	public UUID getPokemonId()
	{
		return pokemonId;
	}

	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static PixelmonModifier fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, PixelmonModifier.class);
	}

	public void save()
	{
		BufferedWriter writer = null;
		try
		{

			File folder = new File(Pixelquest.folder.getAbsolutePath() + "/pixelmonmodifier/");
			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/" + this.getPokemonId().toString() + ".json");
			if (!file.exists())
			{
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}
	
	public static void loadAllProfiles()
	{
		System.out.println("Reloading all pixelmonModifier profiles !");
		PixelmonModifierHandler.pixelmonModifier.clear();
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/pixelmonmodifier/");
		folder.mkdirs();
		for(String fname:folder.list())
		{
			UUID id = UUID.fromString(fname.replace(".json", ""));
			PixelmonModifier modifier = loadPixelmonProfile(id);
			
			PixelmonModifierHandler.pixelmonModifier.put(modifier.pokemonId, modifier);
		}
		
		System.out.println("Reloaded pixelmonmodifier profiles: "+PixelmonModifierHandler.pixelmonModifier.size());
	}

	protected static PixelmonModifier loadPixelmonProfile(UUID pixelmonId)
	{
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/pixelmonmodifier/");
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/" + pixelmonId.toString() + ".json");

		if (f.exists())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				PixelmonModifier pp = PixelmonModifier.fromJson(everything);
				return pp;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return null;
	}

}
