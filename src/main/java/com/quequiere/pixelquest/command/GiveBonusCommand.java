package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.effect.PotionHandler;
import com.quequiere.pixelquest.player.bonus.BonusType;
import com.quequiere.pixelquest.player.license.EnumLicenseType;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.potion.PotionUtils;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GiveBonusCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, Integer> liste = new HashMap<EntityPlayerMP, Integer>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "givebonus";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "givebonus";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("gb");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		
		EntityPlayerMP p= null;
		
		if (args.length == 3)
		{
			p = Tools.getPlayer(args[2]);
		}
		else if (sender instanceof EntityPlayerMP)
		{
			p = (EntityPlayerMP) sender;
		}
		else if (p == null)
		{
			sender.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find the targeted player !"));
			return;
		}

		String perm = "pixelquest.givebonus";
		if (!Pixelquest.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
			return;
		}

		if (args.length < 1)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "use /givebonus [type]"));
		}
		else
		{
			String ampstr = args[0];

			try
			{
				BonusType type = BonusType.valueOf(ampstr);

				if (args.length < 2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give a during time in min"));
				}
				else
				{
					try
					{

						int time = Integer.parseInt(args[1]);
						int id = Tools.getAleatInt(0, Integer.MAX_VALUE-1);

						ItemStack is = new ItemStack(Items.PAPER);

						is.setStackDisplayName(TextFormatting.AQUA + "Bonus " + type.name() +" "+time+" min");
						NBTTagCompound c = is.getTagCompound();
						NBTTagCompound d = c.getCompoundTag("display");
						d.setTag("Lore", new NBTTagList());
						NBTTagList nbttaglist3 = d.getTagList("Lore", 8);

						nbttaglist3.appendTag(new NBTTagString("Right click to activate this bonus."));
						nbttaglist3.appendTag(new NBTTagString("If you already have a bonus type, the better will be keep."));
						nbttaglist3.appendTag(new NBTTagString("During time: " + time + " min"));
						nbttaglist3.appendTag(new NBTTagString("Type:" + type.name()));
						nbttaglist3.appendTag(new NBTTagString("timeInMinutes:" +time));
						nbttaglist3.appendTag(new NBTTagString("License id:" + id));

						p.inventory.addItemStackToInventory(is);

						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Livraison ..."));
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong time !"));
					}
				}
			}
			catch (IllegalArgumentException e)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "This type doesn't exist. Types: "));
				for (BonusType l : BonusType.values())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "- " + l.name()));
				}

			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
