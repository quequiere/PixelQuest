package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.effect.PotionHandler;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GivePotionCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, Integer> liste = new HashMap<EntityPlayerMP, Integer>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "givepotion";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "givepotion";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

			EntityPlayerMP p= null;
			
			
			if(args.length==3)
			{
				p = Tools.getPlayer(args[2]);
			}
			else if(sender instanceof EntityPlayerMP)
			{
				p = (EntityPlayerMP) sender;
			}
			else if(p==null)
			{
				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find the targeted player !"));
				return;
			}
		
			String perm = "pixelquest.givepotion";
			if (!Pixelquest.hasPermission(sender, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}

			if (args.length < 1)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "use /givepotion amplifier (integer like 1,2 etc...) [type] [playername]"));
			}
			else
			{
				if(args.length<2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to give amplifier and type !"));
				}
				else
				{
					String ampstr = args[0];
					String type = args[1];
					
					if(type.equals("exp")||type.equals("attack"))
					{
						try
						{
							int amp = Integer.parseInt(ampstr);

							PotionHandler.give(p, amp,type);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Livraison ..."));
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong amplifier !"));
						}
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong type, list: exp/attack"));
					}

					
				}
		

			}

		

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
