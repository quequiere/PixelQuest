package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.player.PlayerProfile;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GiveExpCommand implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "giveexp";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "giveexp battle/quest [playername] amount";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		
		
		if(sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;
			String perm = "pixelquest.giveexp";
			if(!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
				return;
			}
		}
		
		if (args.length < 3)
		{
			sender.addChatMessage(new TextComponentString(this.getCommandUsage(sender)));
		}
		else
		{
			String type = args[0];
			String name = args[1];
			int i = 0;
			PlayerProfile pp = PlayerProfile.getPlayerProfile(name,false);
			EntityPlayerMP target = Tools.getPlayer(name);

			
			try
			{
				i = Integer.parseInt(args[2]);
			}
			catch(NumberFormatException e)
			{
				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "amount is integer"));
				return;
			}
			
			if (pp == null)
			{
				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find player"));
			}
			else
			{
				if (type.equals("battle"))
				{
					pp.getExpProfile().addExpBattle(i, target, pp);
					sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Exp added"));
				}
				else if (type.equals("quest"))
				{
					pp.getExpProfile().addExpQuest(i, target, pp);
					sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Exp added"));
				}
				else
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong type"));
				}
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
