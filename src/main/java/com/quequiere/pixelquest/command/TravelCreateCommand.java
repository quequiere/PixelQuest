package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pixelmonmod.pixelmon.AI.AIFlying;
import com.pixelmonmod.pixelmon.AI.AIFlyingPersistent;
import com.pixelmonmod.pixelmon.AI.AIMoveTowardsBlock;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.AIHelper;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.effect.PotionHandler;
import com.quequiere.pixelquest.quest.QuestCreator;
import com.quequiere.pixelquest.travel.Travel;
import com.quequiere.pixelquest.travel.TravelCreator;
import com.quequiere.pixelquest.travel.entity.AIFlyingPersistentModified;
import com.quequiere.pixelquest.travel.entity.TravellerEntity;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIMoveToBlock;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.pathfinding.WalkNodeProcessor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class TravelCreateCommand implements ICommand
{
	private List aliases;
	
	public static HashMap<EntityPlayerMP, TravelCreator> creators = new HashMap<EntityPlayerMP, TravelCreator>();
	public static ArrayList<EntityPlayerMP> fromSelector = new ArrayList<EntityPlayerMP>();
	public static ArrayList<EntityPlayerMP> toSelector = new ArrayList<EntityPlayerMP>();

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "travelcreate";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "travelcreate";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("tc");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;

			String perm = "pixelquest.travelcreate";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}
			
			TravelCreator creator = creators.get(p);
			
			if(creator==null)
			{
				creator=new TravelCreator();
				creators.put(p, creator);
			}
			
			if (args.length > 0)
			{
				String sub = args[0];
				
				if(sub.equals("addpoint"))
				{
					Location l = new Location(p.getPosition().getX(), p.getPosition().getY(), p.getPosition().getZ(), p.getServerWorld());
					creator.pathPoints.add(l);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "New location added !"));
				}
				else if(sub.equals("setname"))
				{
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please give a name !"));
					}
					else
					{
						String name = "";
						for(int x = 1;x<args.length;x++)
						{
							if(name.equals(""))
							{
								name=args[x];
							}
							else
							{
								name+=" "+args[x];
							}
						}
						
						if(Travel.getByName(name)!=null)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry this name already exist !"));
						}
						else
						{
							creator.name=name;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "New travel name: "+creator.name));
						}
						
					}
				}
				else if(sub.equals("setFrom"))
				{
					if(toSelector.contains(p))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Your need to finish or cancel the setTo selection first."));
					}
					else if(fromSelector.contains(p))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Selection canceled !"));
						fromSelector.remove(p);
					}
					else
					{
						fromSelector.add(p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Please select the original Npc"));
					}
				}
				else if(sub.equals("setTo"))
				{

					if(fromSelector.contains(p))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Your need to finish or cancel the setFrom selection first."));
					}
					else if(toSelector.contains(p))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Selection canceled !"));
						toSelector.remove(p);
					}
					else
					{
						toSelector.add(p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Please select the \"to\" Npc"));
					}
				
				}
				else if(sub.equals("view"))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "------------------------"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Current:"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Name: "+creator.name));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "From: "+creator.from));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "To: "+creator.to));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "List size: "+creator.pathPoints.size()));
					p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "------------------------"));
				}
				else if(sub.equals("clear"))
				{
					creators.remove(p);
					creator=new TravelCreator();
					creators.put(p, creator);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cache cleared !"));
				}
				else if(sub.equals("finish"))
				{
					if(creator.pathPoints == null || creator.to ==null ||creator.from==null || creator.name==null || creator.pathPoints.size()<=0)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "The travel creator is not finished !"));
					}
					else
					{
						Travel t = new Travel(creator.pathPoints, creator.from, creator.to, creator.name);
						Travel.registerNewTravel(t);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Travel created !"));
						
						creators.remove(p);
						creator=new TravelCreator();
						creators.put(p, creator);
					}

				}
				else if(sub.equals("reload"))
				{
					Travel.loadAllTravel();
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Travels reloaded !"));
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow sub command"));
				}

			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "More args needed:"));
				
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate addpoint"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate setname [name]"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate setFrom"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate setTo"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate view"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate finish"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate clear"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/travelcreate reload"));
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}
}
