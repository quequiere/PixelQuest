package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class AddItemToShopCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, NpcShopKeeperCreator> liste = new HashMap<EntityPlayerMP, NpcShopKeeperCreator>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "addshopitem";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "addshopitem";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;

			String perm = "pixelquest.addshopitem";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}
			
			if(args.length<2)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Add buy and sell price /addshopitem 20 0"));
				return;
			}
			
			int buy = -1,sell = -1;
			
			try
			{
				buy =Integer.parseInt(args[0]);
				sell =Integer.parseInt(args[1]);
				
			}
			catch(NumberFormatException e)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong price format !"));
				return;
			}

			
			if (liste.containsKey(p))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "ItemStack replaced !"));
				liste.remove(p);
			}
			if(p.getHeldItemMainhand()==null)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Your hand is empty !"));
				return;
			}
			liste.put(p, new NpcShopKeeperCreator(p.getHeldItemMainhand(),buy,sell));
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Now interact with shopKeeperEntity"));

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
