package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.notification.Notification;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class NotificationCommand implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "notification";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "notification all/[playername] durationinSeconds text";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP player = (EntityPlayerMP) sender;
			
			String perm = "pixelquest.notification";
			if(!Pixelquest.hasPermission(player, perm))
			{
				player.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
				return;
			}
			
			
			if(args.length<3)
			{
				sender.addChatMessage(new TextComponentString(this.getCommandUsage(sender)));
			}
			else
			{
				
				int duration = 0;
				
				try
				{
					duration = Integer.parseInt(args[1]) *1000;
				}
				catch(NumberFormatException e)
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.RED+"Duration is in seconds"));
					return;
				}
				
				
				String target = args[0];
				String phrase="";
				
				for(int x=2;x<args.length;x++)
				{
					if(x==2)
					{
						phrase=args[x];
					}
					else
					{
						phrase+=" "+args[x];
					}
				}
				
				if(target.equals("all"))
				{
					for(EntityPlayerMP p:Tools.getAllPlayers())
					{
						Notification.addNotification(p,phrase , duration);
					}
					sender.addChatMessage(new TextComponentString(TextFormatting.GREEN+"Notification sended !"));
				}
				else
				{
					EntityPlayerMP targetp = Tools.getPlayer(target);
					if(targetp==null)
					{
						sender.addChatMessage(new TextComponentString(TextFormatting.RED+"Can't find player !"));
					}
					else
					{
						Notification.addNotification(targetp,phrase , duration);
						sender.addChatMessage(new TextComponentString(TextFormatting.GREEN+"Notification sended !"));
					}
				}
			}
		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
