package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.experience.ExperienceProfile;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class ShowProfle implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "showprofile";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "showprofile [playerName]";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
	
		
		if(!(sender instanceof EntityPlayerMP))
		{
			return;
		}
		
		EntityPlayerMP player = (EntityPlayerMP) sender;
		
		String perm = "pixelquest.showprofile";
		if(!Pixelquest.hasPermission(player, perm))
		{
			player.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
			return;
		}
		
		String name;
		if (args.length < 1)
		{
			name=sender.getName();
		}
		else
		{
			 name = args[0];
		}
		
		PlayerProfile pp = PlayerProfile.getPlayerProfile(name, false);
		
		if(pp!=null)
		{
			ExperienceProfile.display(pp, (EntityPlayerMP) sender);
		}
		else
		{
			sender.addChatMessage(new TextComponentString(TextFormatting.RED+"Can't find this player"));
		}
	

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
