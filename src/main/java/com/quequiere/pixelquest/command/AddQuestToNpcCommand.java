package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class AddQuestToNpcCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, Integer> liste = new HashMap<EntityPlayerMP, Integer>();
	
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "addquestnpc";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "addquestnpc name (if name=-1 ==> unbind)";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;
			
			String perm = "pixelquest.addquestnpc";
			if(!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
				return;
			}
			
			
			
			if(args.length<1)
			{
				sender.addChatMessage(new TextComponentString(this.getCommandUsage(sender)));
			}
			else
			{
				String name="";
				for(int x=0;x<args.length;x++)
				{
					if(name.equals(""))
					{
						name=args[x];
					}
					else
					{
						name+=" "+args[x];
					}
				}
				
				if(name.equals("-1"))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Unbinded !"));
					liste.remove(p);
					return;
				}
				Quest q = Quest.getQuestByName(name);
				if(q==null)
				{
					liste.remove(p);
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this quest !"));
				}
				else
				{
					liste.remove(p);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Now click on NPC shop to add this quest."));
					liste.put(p, q.getId());
				}
			}
		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
