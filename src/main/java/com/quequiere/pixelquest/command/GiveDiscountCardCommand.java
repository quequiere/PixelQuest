package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.effect.PotionHandler;
import com.quequiere.pixelquest.player.license.EnumLicenseType;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.potion.PotionUtils;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GiveDiscountCardCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, Integer> liste = new HashMap<EntityPlayerMP, Integer>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "givediscount";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "givediscount percent player";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		
		EntityPlayerMP p= null;
		EntityPlayerMP tosend = null;
		
		if (args.length == 2)
		{
			tosend = Tools.getPlayer(args[1]);
		}
		else if (sender instanceof EntityPlayerMP)
		{
			p = (EntityPlayerMP) sender;
			tosend=p;
		}
		else if (tosend == null)
		{
			sender.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find the targeted player !"));
			return;
		}

		String perm = "pixelquest.givediscount";
		if (!Pixelquest.hasPermission(sender, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
			return;
		}

		if (args.length < 1)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "use /givediscount [%]"));
		}
		else
		{
			String per = args[0];

			try
			{

				int percent = Integer.parseInt(per);
				int id = Tools.getAleatInt(0, Integer.MAX_VALUE-1);
				if(percent<=0 || percent>100)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't set this discount !"));
					return;
				}

			
				
				ItemStack is = new ItemStack(Items.PAPER);

				is.setStackDisplayName(TextFormatting.AQUA + "Discount card "+percent+" %");
				NBTTagCompound c = is.getTagCompound();
				NBTTagCompound d = c.getCompoundTag("display");
				d.setTag("Lore", new NBTTagList());
				NBTTagList nbttaglist3 = d.getTagList("Lore", 8);

				nbttaglist3.appendTag(new NBTTagString("Right click with this card on a shop keeper"));
				nbttaglist3.appendTag(new NBTTagString("to apply discount for the next purchase"));
				nbttaglist3.appendTag(new NBTTagString("only available for one item !"));
				nbttaglist3.appendTag(new NBTTagString("Percent:"+percent));
				nbttaglist3.appendTag(new NBTTagString("License id:" + id));

				tosend.inventory.addItemStackToInventory(is);

				tosend.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Livraison ..."));
			}
			catch (NumberFormatException e)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong integer format !"));
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
