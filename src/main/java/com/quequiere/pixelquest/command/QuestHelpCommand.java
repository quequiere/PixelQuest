package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.graph.Edges;
import com.quequiere.pixelquest.graph.GraphExporter;
import com.quequiere.pixelquest.graph.Nodes;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.type.CommandServerReward;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class QuestHelpCommand implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "questhelp";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "questhelp";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("qh");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		/*GraphExporter ge = new GraphExporter();
		HashMap<Integer, ArrayList<Integer>> given = new HashMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, ArrayList<Integer>> preRequis = new HashMap<Integer, ArrayList<Integer>>();

		for (Quest q : Quest.getQuestList())
		{
			ge.nodes.add(new Nodes(q.getName(), String.valueOf(q.getId())));
			
			
			if(q.getPreRequis().size()>0)
			{
				
				ArrayList<Integer> prerequiids;

				if (preRequis.containsKey(q.getId()))
				{
					prerequiids = preRequis.get(q.getId());
				}
				else
				{
					prerequiids = new ArrayList<Integer>();
					preRequis.put(q.getId(), prerequiids);
				}
				
				for(Integer idsPre:q.getPreRequis())
				{
					prerequiids.add(idsPre);
				}
			}

			for (Reward r : q.getRewardList())
			{
				if (r instanceof CommandServerReward)
				{
					CommandServerReward sr = (CommandServerReward) r;
					if (sr.getCommand().contains("quest add"))
					{
						String gived = sr.getCommand().split("quest add %p ")[1];
						System.out.println("Dependencies founded: " + gived);
						Quest givedQuest = Quest.getQuestByName(gived);

						ArrayList<Integer> givenList;

						if (given.containsKey(q.getId()))
						{
							givenList = given.get(q.getId());
						}
						else
						{
							givenList = new ArrayList<Integer>();
							given.put(q.getId(), givenList);
						}
						givenList.add(givedQuest.getId());

					}
				}
			}

		}

		for (Integer giverId : given.keySet())
		{
			for (Integer gived : given.get(giverId))
			{
				ge.edges.add(new Edges("Reward", String.valueOf(giverId), String.valueOf(gived), String.valueOf(Tools.getAleatInt(0, Integer.MAX_VALUE-1)),"#343434"));
			}

		}
		
		
		for (Integer needPrerequis : preRequis.keySet())
		{
			for (Integer prerequis : preRequis.get(needPrerequis))
			{
				ge.edges.add(new Edges("PreRequis", String.valueOf(needPrerequis), String.valueOf(prerequis), String.valueOf(Tools.getAleatInt(0, Integer.MAX_VALUE-1)),"#F80000"));
			}

		}*/

		// ge.edges.add(new Edges("Reward", "1", "2", "1"));

	

		sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "-----------------------"));
		sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Command list for pixelquest:"));

		for (ICommand c : Pixelquest.commands)
		{
			sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "/" + c.getCommandName()));
			if(c.getCommandAliases()!=null)
			{
				for (String al : c.getCommandAliases())
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.GRAY + "==> /" + al));
				}
			}
		
		}

		sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "-----------------------"));
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
