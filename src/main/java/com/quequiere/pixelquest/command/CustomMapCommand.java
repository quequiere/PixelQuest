package com.quequiere.pixelquest.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.vecmath.Vector2d;

import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.map.MapTool;
import com.quequiere.pixelquest.player.PlayerProfile;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemMap;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.storage.MapData;

public class CustomMapCommand implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "custommap";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "custommap";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
	
		
		if(sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;
			String perm = "pixelquest.custommap";
			if(!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
				return;
			}
		}
		else 
		{
			return;
		}
		
		EntityPlayerMP p = (EntityPlayerMP) sender;
		
		
		if(args.length>0)
		{
			if(args[0].equals("size"))
			{
				if(args.length<2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to give an url"));
				}
				else
				{
					String url = args[1];
					try
					{
						Vector2d vec = MapTool.getMapSize(url);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You need "+vec.getX()+" line and "+vec.getY()+" col to display this picture"));
						
					}
					catch (IOException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + e.getMessage()));
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Oups ! Something goes wrong"));
					}
				}
			}
			else if(args[0].equals("give"))
			{
				if(args.length<2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to give an url"));
				}
				else
				{
					String url = args[1];
					try
					{
						MapTool.givePlayer(url, p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Map gived !"));
					}
					catch (IOException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while give map"));
						e.printStackTrace();
					}
				
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "unknow sub command !"));
			}
		}
		else
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to add args"));
		}
		
		
		

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
