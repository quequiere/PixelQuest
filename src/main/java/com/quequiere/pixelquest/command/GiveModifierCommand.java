package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.effect.PotionHandler;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.potion.PotionUtils;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GiveModifierCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, Integer> liste = new HashMap<EntityPlayerMP, Integer>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "givemodifier";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "givemodifier";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		EntityPlayerMP p = null;

		if (args.length == 3)
		{
			p = Tools.getPlayer(args[2]);
		}
		else if (sender instanceof EntityPlayerMP)
		{
			p = (EntityPlayerMP) sender;
		}
		
		
		if (p == null)
		{
			sender.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find the targeted player !"));
			return;
		}

		String perm = "pixelquest.givemodifier";
		if (!Pixelquest.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
			return;
		}

		if (args.length < 1)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "use /givemodifier [type]"));
		}
		else
		{
			String ampstr = args[0];

			if (ampstr.equals("size"))
			{
				if (args.length < 2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give a float size (1.0 is normal size)"));
				}
				else
				{
					try
					{

						float size = Float.parseFloat(args[1]);

						if (size < 0.1f)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't use this size !"));
							return;
						}

						int id = Tools.getAleatInt(0, Integer.MAX_VALUE-1);

						ItemStack is = new ItemStack(Items.PAPER);

						is.setStackDisplayName(TextFormatting.AQUA + "Size modifier "+size);
						NBTTagCompound c = is.getTagCompound();
						NBTTagCompound d = c.getCompoundTag("display");
						d.setTag("Lore", new NBTTagList());
						NBTTagList nbttaglist3 = d.getTagList("Lore", 8);

						nbttaglist3.appendTag(new NBTTagString("Right click on a pokemon to change his size definitly."));
						nbttaglist3.appendTag(new NBTTagString("Size:" + size));
						nbttaglist3.appendTag(new NBTTagString("Spell id:" + id));

						p.inventory.addItemStackToInventory(is);

						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Livraison ..."));
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong size !"));
					}
				}
			}
			else if (ampstr.equals("statueframe"))
			{

				if (args.length < 3)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give false or true and max frame"));
				}
				else
				{
					int framemax = 0;
					try
					{
						framemax = Integer.parseInt(args[2]);
					}
					catch (NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong max frame !"));
						return;
					}

					if (args[1].equals("false") || args[1].equals("true"))
					{
						// passing
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Just give true or false please !"));
						return;
					}

					int id = Tools.getAleatInt(0, Integer.MAX_VALUE-1);

					ItemStack is = new ItemStack(Items.PAPER);

					is.setStackDisplayName(TextFormatting.AQUA + "FrameAnimation modifier");
					NBTTagCompound c = is.getTagCompound();
					NBTTagCompound d = c.getCompoundTag("display");
					d.setTag("Lore", new NBTTagList());
					NBTTagList nbttaglist3 = d.getTagList("Lore", 8);

					nbttaglist3.appendTag(new NBTTagString("Right click on a pokemon to change his size definitly."));
					nbttaglist3.appendTag(new NBTTagString("Animation:" + args[1]));
					nbttaglist3.appendTag(new NBTTagString("FrameMax:" + framemax));
					nbttaglist3.appendTag(new NBTTagString("Spell id:" + id));

					p.inventory.addItemStackToInventory(is);

					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Livraison ..."));
				}

			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Types:"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "- size"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "- statueframe"));
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
