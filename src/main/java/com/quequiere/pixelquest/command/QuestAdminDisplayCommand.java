package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.player.PlayerProfile;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class QuestAdminDisplayCommand implements ICommand
{
	
	public static HashMap<String, String> displayerMapDispatcher = new HashMap<String, String>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "questadmindisplay";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "questadmindisplay playername";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		
		
		if(sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;
			String perm = "pixelquest.questadmindisplay";
			if(!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: "+perm));
				return;
			}
			
			PlayerProfile.getPlayerProfile(p.getName(), false).getQuestProfile().forceupdateScoreBoard(p);
			
			if(displayerMapDispatcher.containsKey(p.getName()))
			{
				displayerMapDispatcher.remove(p.getName());
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Old display unbinded !"));
			}
			
			if (args.length < 1)
			{
				sender.addChatMessage(new TextComponentString(this.getCommandUsage(sender)));
			}
			else
			{
				EntityPlayerMP target = Tools.getPlayer(args[0]);
				if(target==null)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "This player is not online ..."));
					return;
				}
				
				displayerMapDispatcher.put(p.getName(), target.getName());
				PlayerProfile pp = PlayerProfile.getPlayerProfile(target.getName(), false);
				pp.getQuestProfile().forceupdateScoreBoard(p, target);
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You can see the player quest now !"));
				
			}
			
		}
		
		
	

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
