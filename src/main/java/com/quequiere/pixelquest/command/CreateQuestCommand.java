package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.quequiere.camerastudio.cinematic.Cinematic;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.objects.RangedInt;
import com.quequiere.pixelquest.event.InteractNpcEvent;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.QuestCreator;
import com.quequiere.pixelquest.quest.QuestTimerBeforeRepeat;
import com.quequiere.pixelquest.quest.QuestTimerBeforeRepeatConfig;
import com.quequiere.pixelquest.quest.reward.PercentedRewardObject;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.RewardType;
import com.quequiere.pixelquest.quest.reward.type.CommandServerReward;
import com.quequiere.pixelquest.quest.reward.type.ExpQuestReward;
import com.quequiere.pixelquest.quest.reward.type.ListCommandServerReward;
import com.quequiere.pixelquest.quest.reward.type.PercentedReward;
import com.quequiere.pixelquest.quest.reward.type.StarterReward;
import com.quequiere.pixelquest.quest.reward.type.TeleportReward;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.GoalType;
import com.quequiere.pixelquest.quest.goal.type.CapturePokemon;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.FightPokemon;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;
import com.quequiere.pixelquest.quest.goal.type.KillPokemon;
import com.quequiere.pixelquest.quest.goal.type.LocationGoal;
import com.quequiere.pixelquest.quest.goal.type.PickupItemGoal;

public class CreateQuestCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, QuestCreator> creators = new HashMap<EntityPlayerMP, QuestCreator>();

	public static ArrayList<EntityPlayerMP> npcRecupGoal = new ArrayList<EntityPlayerMP>();
	
	public static HashMap<EntityPlayerMP, ArrayList<String>> tempStringReward = new HashMap<EntityPlayerMP,  ArrayList<String>>();

	private List aliases;
	
	public static ArrayList<EntityPlayerMP> failRewardAdd = new ArrayList<EntityPlayerMP>();

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "questcreate";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "questcreate setname/addgoal/finish/show/clear/timer/canberepeated/canberepeatedifwin/timebeforerepeat/reload/addReward/startloc/addQuestNeeded/modify/removegoal/intro/outro/canremove/generalTimer/introcinematic/addcommandline/failreward";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("qc");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			final EntityPlayerMP p = (EntityPlayerMP) sender;

			String perm = "pixelquest.createquest";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}

			QuestCreator creator = creators.get(p);
			if (creator == null)
			{
				creator = new QuestCreator();
				creators.put(p, creator);
			}

			if (args.length > 0)
			{
				String sub = args[0];
				if (sub.equals("setname"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Idiot ! Give me the name !"));
					}
					else
					{
						creator.name = args[1];
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Name added !"));
					}

				}
				else if (sub.equals("failreward"))
				{
					if(failRewardAdd.contains(p))
					{
						failRewardAdd.remove(p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Fail reward unbinded !"));
						return;
					}
					else
					{
						failRewardAdd.add(p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Fail reward binded !"));
						return;
					}
				}
				else if (sub.equals("targetEntity"))
				{
					if (!InteractNpcEvent.interactEntity.containsKey(p))
					{
						InteractNpcEvent.interactEntity.put(p, null);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You can choose entity now"));
						return;
					}
					else
					{
						InteractNpcEvent.interactEntity.remove(p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Selection removed !"));
						return;
					}

				}

				else if (sub.equals("addcommandline"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use /qc addcommandline [clear/add/list]"));
						return;
					}
					else 
					{
						String subcc = args[1];
						
						ArrayList<String> coms = tempStringReward.get(p);
						if(coms==null)
						{
							coms=new ArrayList<String>();
						}
						
						tempStringReward.put(p, coms);
						
						if(subcc.equalsIgnoreCase("clear"))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cleared !"));
							return;
						}
						else if(subcc.equalsIgnoreCase("list"))
						{
							for(String s:coms)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "-"+s));
							}
							return;
						}
						if(subcc.equalsIgnoreCase("add"))
						{
							if(args.length<3)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please give your command without /"));
								return;
							}
							String s="";
							for(int x=2;x<args.length;x++)
							{
								
								if( args[x].contains("chance:"))
								{
									continue;
								}
								
								
								if(s.equals(""))
								{
									s=args[x];
								}
								else
								{
									s=s+ " "+args[x];
								}
							}
							
							coms.add(s);
							
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Text added !"));
						}
						
					}
				}
				else if (sub.equals("generalTimer"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use /qc generalTimer [otherQuestToSharedTheTimer] / [Timer if this is the first quest of the shared list in min]"));
						return;
					}

					String name = args[1];
					Quest q = Quest.getQuestByName(name);

					if (q != null)
					{
						QuestTimerBeforeRepeat t = QuestTimerBeforeRepeat.getQuestTimer(q.getId());
						if (t == null)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find this quest in the shared timer list !"));
							return;
						}
						else
						{
							creator.questTimerBeforeRepeat = t;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest will be add to this shared timer list !"));
							return;
						}
					}
					else
					{
						try
						{
							long l = Long.parseLong(args[1]) * 60;
							creator.questTimerBeforeRepeat = new QuestTimerBeforeRepeat(l);
							QuestTimerBeforeRepeatConfig.instance.addToList(creator.questTimerBeforeRepeat);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "This quest will be a shared quest timer."));
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't use this command like this, or we can't find quest in shared list !"));
						}
					}

				}
				else if (sub.equals("startloc"))
				{
					creator.startPos = Location.fromBlockPos(p.getPosition(), p.worldObj);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Start location added !"));
				}
				else if (sub.equals("canremove"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give true or false "));
						return;
					}
					else
					{
						String answer = args[1];
						if (answer.equals("true"))
						{
							creator.canRemove = true;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Done !"));
						}
						else if (answer.equals("false"))
						{
							creator.canRemove = false;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Done !"));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow answer :( !"));
						}
					}
				}
				else if (sub.equals("introcinematic"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give cinematic name"));
						return;
					}

					Cinematic c = Cinematic.load(args[1]);
					if (c == null)
					{
						p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Can't find this cinematic: " + args[1]));
						return;
					}
					else
					{
						p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "Cinematic added !"));
						creator.cinematicname = c.getName();
					}
				}
				else if (sub.equals("intro"))
				{
					creator.intro.clear();
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give string like 'Hey you#this is a new page#good luck !' "));
						return;
					}
					else
					{
						String s = "";
						for (int x = 1; x < args.length; x++)
						{
							if (s.equals(" "))
							{
								s += args[x];
							}
							else
							{
								s += " " + args[x];
							}

						}

						for (String stri : s.split("#"))
						{
							creator.intro.add(stri);
						}

						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Text added !"));
					}
				}
				else if (sub.equals("removegoal"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give goal id"));
						return;
					}
					else
					{
						try
						{
							int id = Integer.parseInt(args[1]);
							for (int x = 0; x < creator.goals.size(); x++)
							{
								Goal g = creator.goals.get(x);
								if (id == g.getInternalId())
								{
									p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Goal removed !"));
									creator.goals.remove(x);
									return;
								}
							}

							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find goal :("));
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give goal id !"));
						}
					}
				}
				else if (sub.equals("modify"))
				{
					
					if (1==1)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry you can't use this command !"));
						return;
					}
					
					
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give quest name"));
						return;
					}
					else
					{
						Quest q = Quest.getQuestByName(args[1]);
						if (q != null)
						{
							creators.remove(p);
							creator = new QuestCreator();
							creators.put(p, creator);
							q.loadInCreator(creator);

							creator.isModify = true;
							creator.questId = q.getId();

							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest loaded in cache"));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "No quest for this name."));
						}
					}
				}
				else if (sub.equals("addQuestNeeded"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give quest name"));
						return;
					}
					else
					{
						Quest q = Quest.getQuestByName(args[1]);
						if (q != null)
						{
							creator.prerequis.add(q.getId());
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest added."));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "No quest for this name."));
						}
					}
				}
				else if (sub.equals("canberepeated"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give true or false"));
						return;
					}
					else
					{
						String subi = args[1];
						if (subi.equals("true"))
						{
							creator.canBeRepeated = true;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Parameter added !"));
						}
						else if (subi.equals("false"))
						{
							creator.canBeRepeated = false;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Parameter added !"));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give true or false please !!!"));
						}
					}
				}
				else if (sub.equals("canberepeatedifwin"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give true or false"));
						return;
					}
					else
					{
						String subi = args[1];
						if (subi.equals("true"))
						{
							creator.canBeRepeatedIfWin = true;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Parameter added !"));
						}
						else if (subi.equals("false"))
						{
							creator.canBeRepeatedIfWin = false;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Parameter added !"));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give true or false please !!!"));
						}
					}
				}
				else if (sub.equals("timebeforerepeat"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give timer in min"));
						return;
					}
					else
					{
						// questcreate timer 5
						int timer = -1;
						try
						{
							timer = Integer.parseInt(args[1]);
							creator.timerInSecondBeforeRepeat = timer * 60;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Timer set"));
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give a number of min !"));
						}
					}
				}
				else if (sub.equals("timer"))
				{
					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give timer in min"));
						return;
					}
					else
					{
						// questcreate timer 5
						int timer = -1;
						try
						{
							timer = Integer.parseInt(args[1]);
							creator.timer = timer * 60;
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Timer set"));
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give a number of min !"));
						}
					}
				}
				else if (sub.equals("clear"))
				{
					creators.remove(p);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Cleared !"));
				}
				else if (sub.equals("reload"))
				{
					Quest.loadAllQuest();
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Reloaded !"));
				}
				else if (sub.equals("show"))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Name: " + creator.name));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Timer in seconds: " + creator.timer));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Timer before repeat in seconds: " + creator.timerInSecondBeforeRepeat));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Can be repeated: " + creator.canBeRepeated));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Can be repeated after win: " + creator.canBeRepeatedIfWin));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "StartPos: " + creator.startPos));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "CanRemove: " + creator.canRemove));

					for (Integer i : creator.prerequis)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Needed quest to start:" + Quest.getQuestById(i)));
					}

					for (Goal g : creator.goals)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Goal==>" + g.getClass().getSimpleName() + ": " + g.toJson()));
					}
					for (Reward r : creator.rewards)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Reward==>" + r.getClass().getSimpleName() + ": " + r.toJson()));
					}
				}
				else if (sub.equals("finish"))
				{
					if (creator.name == null || creator.name.equals("") || Quest.getQuestByName(creator.name) != null && !creator.isModify)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "The quest name is not correct (maybe already exist ?)"));
					}
					else
					{
						if (creator.goals.size() <= 0)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please add goals !"));
						}
						else
						{
							if (creator.isModify)
							{
								for (int x = 0; x < Quest.getQuestList().size(); x++)
								{
									Quest torem = Quest.getQuestList().get(x);
									if (torem.getId() == creator.questId)
									{
										Quest.getQuestList().remove(x);
										break;
									}
								}

							}

							Quest q = Quest.createQuest(creator.name, creator.goals, creator.rewards, creator.timer, creator.canBeRepeated, creator.canBeRepeatedIfWin, creator.timerInSecondBeforeRepeat, creator.startPos);

							for (Goal gg : q.getGoals())
							{
								gg.setQuestId(q.getId());
							}

							if (creator.isModify)
							{
								q.setIdCAUTIONDONTUSE(creator.questId);
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest ovverwrited !"));
							}

							q.setPreRequis(creator.prerequis);
							q.setIntro(creator.intro);
							q.setIntroCinematicName(creator.cinematicname);
							q.setOutro(creator.outro);
							q.setCanRemove(creator.canRemove);
							q.setFailRewards(creator.failReward);

							if (creator.questTimerBeforeRepeat != null)
							{
								q.setHasSharedTimerBeforeRepeat(true);
								creator.questTimerBeforeRepeat.getQuestList().add(q.getId());
								QuestTimerBeforeRepeatConfig.instance.save();
							}

							q.save();
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest created with id: " + q.getId() + " and name: " + q.getName() + " !"));
							creators.remove(p);
						}

					}

				}
				else if (sub.equals("addgoal") || sub.equals("ag"))
				{

					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Idiot ! Give me the type !"));
						return;
					}

					GoalType type = null;
					try
					{
						type = GoalType.valueOf(args[1]);
					}
					catch (IllegalArgumentException e)
					{

					}

					if (type == null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong type ! Please pick:"));
						for (GoalType gt : GoalType.values())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "-" + gt.name()));
						}
						return;
					}

					Goal g = null;

					if (type.equals(GoalType.KillPokemon) || type.equals(GoalType.CapturePokemon)  || type.equals(GoalType.Fightpokemon))
					{
						int count = 1;
						int minlvl = -1;
						int maxlvl = -1;

						ArrayList<EnumType> pokemonTypes = new ArrayList<EnumType>();
						ArrayList<EnumPokemon> pokemons = new ArrayList<EnumPokemon>();

						if (args.length <= 2)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Complete your command with args, example of command: "));
							p.addChatMessage(new TextComponentString(TextFormatting.BLUE + "/questcreate addgoal KillPokemon/CapturePokemon count:X pokemon:a,b,c type:a,b,c minlvl:X maxlvl:X"));
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "If you dont give a field, * will be apply. Example:"));
							p.addChatMessage(new TextComponentString(TextFormatting.BLUE + "/questcreate addgoal CapturePokemon type:fire,water minlvl:15"));
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Ask to capture all pokemons with fire or water type whith lvl 15 or more"));
							return;
						}
						else
						{
							boolean success = true;
							boolean addedSomething = false;

							for (int x = 2; x < args.length; x++)
							{
								String[] parsed = args[x].split(":");
								if (parsed[0].equals("count"))
								{
									try
									{
										count = Integer.parseInt(parsed[1]);
										addedSomething = true;
									}
									catch (NumberFormatException e)
									{
										p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong number format"));
										success = false;
									}

								}
								else if (parsed[0].equals("minlvl"))
								{
									try
									{
										minlvl = Integer.parseInt(parsed[1]);
										addedSomething = true;
									}
									catch (NumberFormatException e)
									{
										p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong number format"));
										success = false;
									}
								}
								else if (parsed[0].equals("maxlvl"))
								{
									try
									{
										maxlvl = Integer.parseInt(parsed[1]);
										addedSomething = true;
									}
									catch (NumberFormatException e)
									{
										p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong number format"));
										success = false;
									}
								}
								else if (parsed[0].equals("type"))
								{
									String[] types = parsed[1].split(",");
									for (String stype : types)
									{
										EnumType et = null;
										try
										{
											et = EnumType.valueOf(stype);
										}
										catch (IllegalArgumentException e)
										{

										}

										if (et == null)
										{
											p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong type: " + stype));
											String s = "";
											for (EnumType ett : EnumType.values())
											{
												s += ", " + ett;
											}

											p.addChatMessage(new TextComponentString(TextFormatting.RED + s));
											success = false;
										}
										else
										{
											pokemonTypes.add(et);
											addedSomething = true;
										}
									}
								}
								else if (parsed[0].equals("pokemon"))
								{
									String[] types = parsed[1].split(",");
									for (String stype : types)
									{
										EnumPokemon et = null;
										try
										{
											et = EnumPokemon.valueOf(stype);
										}
										catch (IllegalArgumentException e)
										{
											p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find pokemon"));
											return;
										}

										if (et == null)
										{
											p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong pokemonname: " + stype));
											success = false;
										}
										else
										{
											pokemons.add(et);
											addedSomething = true;
										}
									}
								}
							}

							if (!addedSomething)
							{
								p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Bad option ..."));
								return;
							}

							if (success)
							{
								KillPokemon gtemp = null;

								if (type.equals(GoalType.KillPokemon))
								{
									gtemp = new KillPokemon(count);
								}
								else if (type.equals(GoalType.CapturePokemon))
								{
									gtemp = new CapturePokemon(count);
								}
								else if (type.equals(GoalType.Fightpokemon))
								{
									gtemp = new FightPokemon(count);
								}
								else
								{
									p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Sorry buy you used unknow type: " + type));
									return;
								}
								gtemp.setMinLvL(minlvl);
								gtemp.setMaxLvL(maxlvl);
								gtemp.setPokemons(pokemons);
								gtemp.setTypes(pokemonTypes);
								g = gtemp;

							}
						}

					}
					else if (type.equals(GoalType.PickupItem))
					{
						if (args.length < 3)
						{
							p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Give number of item: /qc addgoal PickupItem 5"));
							return;
						}
						else
						{
							try
							{
								int count = Integer.parseInt(args[2]);

								ItemStack i = p.getHeldItemMainhand();

								if (i == null)
								{
									p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "You havent items in your hand"));
									return;
								}

								g = new PickupItemGoal(count, i);
								p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "Added goal item !"));
							}
							catch (NumberFormatException e)
							{
								p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Number format exeption"));
								return;
							}

						}

					}
					else if (type.equals(GoalType.Location))
					{
						g = new LocationGoal(new Location(p));
						p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "Added new goal with current location !"));
					}
					else if (type.equals(GoalType.Entity) ||type.equals(GoalType.Fightentity)  )
					{
						if (InteractNpcEvent.interactEntity.containsKey(p))
						{
							Entity target = InteractNpcEvent.interactEntity.get(p);
							if (target == null)
							{
								p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Select entity first !"));
								
								if(type.equals(GoalType.Fightentity))
								{
									p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "BE CARREFUL ! for fightentity goal you need to select non wild trainer that never despawn !"));
								}
								
								return;
							}
							else
							{
								if(type.equals(GoalType.Fightentity))
								{
									FightEntity gg = new FightEntity(target.getPersistentID().toString());
									gg.setEntity(target);
									g = gg;
								}
								else
								{
									InteractEntity gg = new InteractEntity(target.getPersistentID().toString());
									gg.setEntity(target);
									g = gg;
								}
								
								
								p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "Added goal !"));
								InteractNpcEvent.interactEntity.remove(p);
							}
						}
						else
						{
							p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Use '/questcreate targetEntity' to select an entity !"));
							return;
						}

					}
					else
					{
						p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "Unknow type"));
					}

					if (args.length > 2 && g != null)
					{
						int priority = -1;

						for (int x = 2; x < args.length; x++)
						{
							String[] parsed = args[x].split(":");
							if (parsed[0].equals("priority"))
							{
								try
								{
									priority = Integer.parseInt(parsed[1]);
									p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "Priority added ..."));
								}
								catch (NumberFormatException e)
								{
									p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong number format"));
								}
							}

						}

						g.setPriority(priority);
					}

					if (g == null)
					{
						p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "(885) Something is wrong on your command ..."));
						return;
					}

					creator.goals.add(g);
					p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "Added new goal !"));
					p.addChatComponentMessage(new TextComponentString(TextFormatting.BLUE + "Result: " + g.toJson()));

				}
				else if (sub.equals("addReward"))
				{

					if (args.length < 2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Idiot ! Give me the type !"));
						return;
					}

					RewardType type = null;
					try
					{
						type = RewardType.valueOf(args[1]);
					}
					catch (IllegalArgumentException e)
					{

					}

					if (type == null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wrong type ! Please pick:"));
						for (RewardType gt : RewardType.values())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "-" + gt.name()));
						}
						return;
					}

					Reward r = null;
					int chance = 100;

					for (String argdec : args)
					{
						if (argdec.contains("chance:"))
						{
							String[] futurint = argdec.split(":");
							if (futurint.length == 2)
							{
								try
								{
									chance = Integer.parseInt(futurint[1]);
									if (chance >= 0 && chance <= 100)
									{
										p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Reward chance set to " + chance));
									}
									else
									{
										p.addChatMessage(new TextComponentString(TextFormatting.RED + "Chance need to be betweed 0 and 100"));
										return;
									}

								}
								catch (NumberFormatException e)
								{
									p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while parsing chance number"));
									return;
								}
							}
							else
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while parsing reward chance"));
								return;
							}

						}
					}
					

					if (args[1].equals("TeleportReward"))
					{
						r = new TeleportReward(Location.fromBlockPos(p.getPosition(), p.worldObj));
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added your current location to rewards"));
					}
					else if (args[1].equals("expQuestReward"))
					{
						if (args.length < 3)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Give amount"));
							return;
						}

						try
						{
							int exp = Integer.parseInt(args[2]);
							r = new ExpQuestReward(exp);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Reward quest exp added !"));
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Amount is integer"));
							return;
						}

					}
					else if (args[1].equals("listCommandReward"))
					{
						ArrayList<String> coms = tempStringReward.get(p);
						if(coms==null || coms.size()<=0)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use /qc addcommandline first to add line"));
							return;
						}
						
						r=new ListCommandServerReward(coms);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added to reward commands list"));
						tempStringReward.remove(p);
					}
					else if (args[1].equals("CommandReward"))
					{
						String com = "";
						for (int x = 2; x < args.length; x++)
						{
							
							if( args[x].contains("chance:"))
							{
								continue;
							}
							
							if (com.equals(""))
							{
								com = args[x];
							}
							else
							{
								com += " " + args[x];
							}
						}

						if (com.equals(""))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Really ... ?"));
						}
						else
						{
							r = new CommandServerReward(com);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added to reward command: /" + com));
						}

					}
					else if (args[1].equals("starterReward"))
					{
						r = new StarterReward();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added starter reward for this quest !"));
					}
					else if (args[1].equals("percentedReward"))
					{
						
						if(args.length>=3)
						{
							if(args[2].equals("clear"))
							{
								creator.nextIsPercentedReward=false;
								creator.percentedReward.clear();
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Percented cleared !"));
							}
						}
						
						if(!creator.nextIsPercentedReward)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "The next reward will be added to percentedReward !  You must use chance: in args !"));
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "If you want to validate percentedReward, retype this command directly !"));
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You can add clear to clean all percentedRewards"));
							creator.nextIsPercentedReward=true;
							return;
						}
						else
						{
							creator.nextIsPercentedReward=false;
							int maxToFind = 100 - creator.percentedReward.size();
							int tot = -1;
							
							for(RangedInt i : creator.percentedReward.keySet())
							{
								System.out.println("test: "+i.getMin()+" == "+i.getMax());
								tot +=(i.getMax()-i.getMin());
							}
							
							System.out.println(tot+"/"+maxToFind);
							
							if(tot!=maxToFind)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't validate percentedReward who is not set to 100 % in total "));
								return;
							}
							
							
							ArrayList<PercentedRewardObject> liste = new ArrayList<PercentedRewardObject>();
							for(RangedInt ri:creator.percentedReward.keySet())
							{
								liste.add(new PercentedRewardObject(ri,creator.percentedReward.get(ri)));
							}
							
							r = new PercentedReward(liste);
							creator.percentedReward.clear();
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Percented reward validate and added"));
							
						}
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow" + args[1]));
					}
					
					if(!creator.nextIsPercentedReward)
					{
						r.setChanceToGet(chance);
						if(failRewardAdd.contains(p))
						{
							creator.failReward.add(r);
							failRewardAdd.remove(p);
							p.addChatMessage(new TextComponentString(TextFormatting.AQUA + "Reward added to fail rewards !"));
						}
						else
						{
							creator.rewards.add(r);
						}
						
					}
					else
					{
						creator.nextIsPercentedReward=false;
						int max = -1;
						for(RangedInt i : creator.percentedReward.keySet())
						{
							if(i.getMax()>max)
							{
								max=i.getMax();
							}
						}
						
						
						if(chance==100 && creator.percentedReward.size()==0)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't use percented reward without multiple reward, you need to use chance system"));
						}
						else if(chance<0)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Chance can't be 0!"));
						}
						else if(chance<=max)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Next chance can't be inferior than the next value !"));
						}
						else if(chance>100)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Max percent can't be superior to 100"));
						}
						else
						{
							creator.nextIsPercentedReward=false;
							creator.percentedReward.put(new RangedInt(max+1, chance), r);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Percented cached ! Re-use percetedReward reward to add more percentedReward"));
						}
						
					}
					

				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + this.getCommandUsage(p)));
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + this.getCommandUsage(p)));
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
