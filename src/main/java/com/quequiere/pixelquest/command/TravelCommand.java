package com.quequiere.pixelquest.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pixelmonmod.pixelmon.AI.AIFlying;
import com.pixelmonmod.pixelmon.AI.AIFlyingPersistent;
import com.pixelmonmod.pixelmon.AI.AIMoveTowardsBlock;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.AIHelper;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.effect.PotionHandler;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.fly.FlyProfile;
import com.quequiere.pixelquest.travel.Travel;
import com.quequiere.pixelquest.travel.entity.AIFlyingPersistentModified;
import com.quequiere.pixelquest.travel.entity.TravellerEntity;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIMoveToBlock;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.entity.item.EntityBoat;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.pathfinding.WalkNodeProcessor;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class TravelCommand implements ICommand
{

	public static EntityPixelmon pokemon;

	private List aliases;

	public static ArrayList<PathPoint> pathPoints = new ArrayList<PathPoint>();

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "travel";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "travel";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (pokemon != null)
		{
			pokemon.setDead();
		}

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;
			PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
			FlyProfile fp = pp.getFlyprofile();

			if (args.length > 0)
			{
				String subCommand = args[0];
				
				if(subCommand.equals("speed"))
				{
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please choose a speed, 1 to 10, you can use also float number (like 2.5 or 3.5 etc...)"));
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Ex: /travel speed 4"));
					}
					else
					{
						try
						{
							float speed = Float.parseFloat(args[1]);
							if(speed>=1 && speed<=10)
							{
								fp.setFlySpeed(speed);
								pp.save();
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "New fly speed set !"));
							}
							else
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry, you can't set this speed !"));
							}
							
						}
						catch (NumberFormatException e)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry, you used a wront number format !"));
						}
					}
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow subCommand"));
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "--------------------"));
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Current fly speed: "+fp.getFlySpeed()));
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Current metter price: "+(fp.getFlySpeed()*Travel.priceMetter+" $/m")));
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "--------------------"));
				
				p.addChatMessage(new TextComponentString(TextFormatting.AQUA + "Sub command:"));
				p.addChatMessage(new TextComponentString(TextFormatting.AQUA + "/travel speed"));
			}

			

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}
}
