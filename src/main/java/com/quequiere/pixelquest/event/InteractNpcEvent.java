package com.quequiere.pixelquest.event;

import java.util.ArrayList;
import java.util.HashMap;

import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;
import com.quequiere.pixelquest.command.AddItemToShopCommand;
import com.quequiere.pixelquest.command.AddQuestToNpcCommand;
import com.quequiere.pixelquest.command.RemoveItemToShopCommand;
import com.quequiere.pixelquest.command.TravelCreateCommand;
import com.quequiere.pixelquest.npc.NpcGestionnaire;
import com.quequiere.pixelquest.npc.NpcQuestGiver;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.npc.NpcShopKeeperCustom;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.experience.ExperienceProfile;
import com.quequiere.pixelquest.player.quest.GoalAvancement;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.type.StarterReward;
import com.quequiere.pixelquest.travel.Travel;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class InteractNpcEvent
{

	private static HashMap<EntityPlayerMP, Entity> verifStarter = new HashMap<EntityPlayerMP, Entity>();

	private static HashMap<EntityPlayerMP, Long> timeCooldown = new HashMap<EntityPlayerMP, Long>();

	public static HashMap<EntityPlayerMP, Entity> interactEntity = new HashMap<EntityPlayerMP, Entity>();
	
	

	@SubscribeEvent
	public void entityInteractEvent(EntityInteract e)
	{

		// A tester
		if (!e.getHand().equals(EnumHand.MAIN_HAND))
		{
			return;
		}

		try
		{

			EntityPlayerMP p = (EntityPlayerMP) e.getEntityPlayer();

			if (timeCooldown.containsKey(p))
			{
				long diff = System.currentTimeMillis() - timeCooldown.get(p);
				if (diff < 100)
				{
					return;
				}

				timeCooldown.remove(p);
				timeCooldown.put(p, System.currentTimeMillis());
			}
			else
			{
				timeCooldown.put(p, System.currentTimeMillis());
			}

			if (e.getTarget() instanceof Entity)
			{
				Entity entity = (Entity) e.getTarget();

				if (e.getTarget() instanceof EntityPlayerMP)
				{
					ExperienceProfile.display(PlayerProfile.getPlayerProfile(e.getTarget().getName(), false), p);
					return;
				}

				if (interactEntity.containsKey(p))
				{
					interactEntity.remove(p);
					interactEntity.put(p, e.getTarget());
					p.addChatComponentMessage(new TextComponentString(TextFormatting.GREEN + "New entity selected !"));
					e.setCanceled(true);
					return;
				}

				// --- on check si npc goal talk
				PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
				boolean starterGived = false;

				ArrayList<Goal> toUpdate = new ArrayList<Goal>();

				for (Quest q : profile.getQuestProfile().getFollowedQuest())
				{
					for (Goal g : q.getGoals())
					{
						if (g instanceof InteractEntity)
						{
							InteractEntity gg = (InteractEntity) g;

							if (gg.getUuid().equals(entity.getPersistentID().toString()))
							{
								GoalAvancement avan = profile.getQuestProfile().getGoalAvancementForQuest(q.getId(), g.getInternalId());

								if (avan == null)
								{
									continue;
								}

								if (!avan.isFinished() && avan.canUpdateWithPriority(q, g, p))
								{

									// TRES SPECIFIQUE A LA QUETE STARTER, NE
									// PAS
									// MODIFIER
									if (entity instanceof EntityStatue)
									{
										for (Reward r : q.getRewardList())
										{
											if (r instanceof StarterReward && !starterGived)
											{

												EntityStatue statue = (EntityStatue) entity;

												if (!verifStarter.containsKey(p))
												{
													verifStarter.put(p, statue);
													p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Please click again on this pokemon to confirm your choice of starter !"));
													return;
												}

												if (!verifStarter.get(p).equals(statue))
												{
													verifStarter.remove(p);
													verifStarter.put(p, statue);
													p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You changed your mind ? Please click again on this pokemon to confirm your choice of starter !"));
													return;
												}

												p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Choice confirmed !"));

												verifStarter.remove(p);

												starterGived = true;
												StarterReward.giveStarter(p, statue.getPokemonName());
												for (Goal gtemp : q.getGoals())
												{
													GoalAvancement tpa = profile.getQuestProfile().getGoalAvancementForQuest(q.getId(), gtemp.getInternalId());
													if (gtemp instanceof InteractEntity)
													{
														tpa.forceFinish();
													}
												}
												profile.getQuestProfile().forceupdateScoreBoard(p);
												avan.update(gg, p);
												profile.save();
												break;
											}
										}
									}
									// -----------------------------------------------------------------
									toUpdate.add(gg);
								}
							}

						}
					}
				}

				for (Goal goal : toUpdate)
				{
					if (goal == null)
					{
						System.out.println("Fatal error on algo for goal in interact event");
						continue;
					}
					GoalAvancement avancement = profile.getQuestProfile().getGoalAvancementForQuest(goal.getQuestId(), goal.getInternalId());

					if (avancement == null)
					{
						System.out.println("Error, goal avancement seems to be null for " + e.getEntityPlayer());
						System.out.println(profile);
						System.out.println(profile.getQuestProfile());
						System.out.println("Quest id: " + goal.getQuestId() + " goal id: " + goal.getInternalId());

					}
					else
					{
						avancement.update(goal, p);
					}

				}

				profile.save();

				// --------------------

				if (e.getTarget() instanceof NPCShopkeeper)
				{
					NPCShopkeeper shop = (NPCShopkeeper) e.getTarget();
					NpcQuestGiver npcQuest = NpcGestionnaire.instance.getNpcQuestGiver(shop.getPersistentID().toString());

					if (AddQuestToNpcCommand.liste.containsKey(p))
					{
						if (npcQuest == null)
						{
							npcQuest = new NpcQuestGiver();
							NpcGestionnaire.instance.addQuestGiver(shop.getUniqueID().toString(), npcQuest);
						}

						npcQuest.addQuest(AddQuestToNpcCommand.liste.get(p));
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added quest to npc id:" + AddQuestToNpcCommand.liste.get(p)));
						NpcGestionnaire.instance.save();
						e.setCanceled(true);
						AddQuestToNpcCommand.liste.remove(p);
						return;
					}

					if (npcQuest != null)
					{
						npcQuest.displayGui(p, shop);
						e.setCanceled(true);
						return;
					}
					else
					{
						if (TravelCreateCommand.toSelector.contains(p))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added entity to!"));
							TravelCreateCommand.toSelector.remove(p);
							TravelCreateCommand.creators.get(p).to = shop.getPersistentID();
							e.setCanceled(true);
							return;
						}
						else if (TravelCreateCommand.fromSelector.contains(p))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Added entity from!"));
							TravelCreateCommand.fromSelector.remove(p);
							TravelCreateCommand.creators.get(p).from = shop.getPersistentID();
							e.setCanceled(true);
							return;
						}
						else
						{
							ArrayList<Travel> tlist = Travel.getTravelByEntity(shop.getPersistentID());
							if (tlist.size() > 0)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Oppening flight master !"));
								Travel.displayGui(p, shop);
								e.setCanceled(true);
								return;
							}
						}
					}
					
					
					NpcShopKeeperCustom custom = NpcGestionnaire.instance.getnpcShopKeerper(shop.getPersistentID().toString());
					NpcShopKeeperCreator creator = AddItemToShopCommand.liste.get(e.getEntityPlayer());
					if(creator!=null)
					{
						if(custom==null)
						{
							custom = new NpcShopKeeperCustom();
							NpcGestionnaire.instance.addCustomShopKeeper(shop.getPersistentID().toString(),custom);
						}
						
						custom.addItem(creator.is, creator.buyPrice, creator.sellPrice);
						NpcGestionnaire.instance.save();
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Item added !"));
						AddItemToShopCommand.liste.remove(e.getEntityPlayer());
						e.setCanceled(true);
						return;
					}
					
					
					if(RemoveItemToShopCommand.liste.containsKey(p) && custom!=null)
					{
						int index = RemoveItemToShopCommand.liste.get(p);
						index --;
						
						if(index<0 || index+1>custom.getItems().size())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Index error !"));
							return;
						}
						
						custom.getItems().remove(index);
						NpcGestionnaire.instance.save();
						
						
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Item removed !"));
						RemoveItemToShopCommand.liste.remove(p);
						e.setCanceled(true);
						return;
					}
					
					
					if(custom!=null)
					{
						custom.reloaditemList(shop,p);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Special shopKeepr loaded !"));
					}

				}
			}

		}
		catch (NullPointerException ea)
		{
			ea.printStackTrace();
		}

	}

}
