package com.quequiere.pixelquest.event;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Optional;

import com.pixelmonmod.pixelmon.PixelmonMethods;
import com.pixelmonmod.pixelmon.api.events.BeatTrainerEvent;
import com.pixelmonmod.pixelmon.api.events.BeatWildPixelmonEvent;
import com.pixelmonmod.pixelmon.api.events.CaptureEvent;
import com.pixelmonmod.pixelmon.api.events.CaptureEvent.SuccessfulCapture;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.battles.controller.BattleControllerBase;
import com.pixelmonmod.pixelmon.battles.controller.participants.ParticipantType;
import com.pixelmonmod.pixelmon.battles.controller.participants.PlayerParticipant;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.api.events.PixelmonAttackEvent;
import com.pixelmonmod.pixelmon.api.events.PixelmonCalcCritEvent;
import com.pixelmonmod.pixelmon.api.events.PixelmonHealInBattleEvent;
import com.pixelmonmod.pixelmon.api.events.PlayerBattleEndedEvent;
import com.pixelmonmod.pixelmon.api.events.RidePokemonEvent;
import com.pixelmonmod.pixelmon.entities.pixelmon.Entity3HasStats;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.items.EnumPokeballs;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.objects.LinkedLocation;
import com.quequiere.pixelquest.dungeon.objects.SpawnLocation;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.bonus.AttackBonus;
import com.quequiere.pixelquest.player.bonus.Bonus;
import com.quequiere.pixelquest.player.bonus.BonusProfile;
import com.quequiere.pixelquest.player.bonus.CatchBonus;
import com.quequiere.pixelquest.player.bonus.EndBattleHealBonus;
import com.quequiere.pixelquest.player.license.EnumLicenseType;
import com.quequiere.pixelquest.player.quest.GoalAvancement;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.type.CapturePokemon;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.FightPokemon;
import com.quequiere.pixelquest.quest.goal.type.KillPokemon;

public class PokemonEvent
{

	@SubscribeEvent
	public void beattrainerevent(BeatTrainerEvent e)
	{

		EntityPlayerMP p = e.player;
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

		QuestPlayerProfile questProfile = profile.getQuestProfile();

		ArrayList<Goal> toUpdate = new ArrayList<Goal>();

		for (Quest q : questProfile.getFollowedQuest())
		{
			for (Goal g : q.getGoals())
			{
				if (g instanceof FightEntity)
				{
					FightEntity fe = (FightEntity) g;

					if (fe.getUuid().equals(e.trainer.getUniqueID().toString()))
					{
						GoalAvancement avancement = questProfile.getGoalAvancementForQuest(q.getId(), g.getInternalId());

						if (!avancement.isFinished() && avancement.canUpdateWithPriority(q, g, p))
						{
							toUpdate.add(g);
						}
					}

				}
			}
		}

		for (Goal goal : toUpdate)
		{
			GoalAvancement avancement = questProfile.getGoalAvancementForQuest(goal.getQuestId(), goal.getInternalId());
			avancement.update(goal, p);
		}

		profile.save();
	}

	@SubscribeEvent
	public void captureevent(CaptureEvent.StartCapture e)
	{
		boolean finded = false;
		for (Dungeon d : DungeonConfig.loaded)
		{
			if (d.isInZone(e.pokemon))
			{
				for (SpawnLocation sl : d.getSpawnsLocation())
				{
					for (LinkedLocation ll : sl.getLocationEntities())
					{
						Optional<EntityPixelmon> op = ll.getPokemon();
						if (op.isPresent())
						{
							EntityPixelmon pokemonSpawned = op.get();
							if (pokemonSpawned.equals(e.pokemon))
							{
								finded = true;

								Optional<CustomPixelmon> custompoke = CustomPixelmonConfig.config.getByName(sl.getCustomPixelmon());

								if (custompoke.isPresent())
								{
									if (!custompoke.get().isCapturable())
									{
										e.player.addChatMessage(new TextComponentString(TextFormatting.RED + "This specific type of pokemon can't be captured in this dungeon."));
										e.setCanceled(true);
										return;
									}
								}

							}
						}
					}
				}

				if (!finded)
				{
					e.player.addChatMessage(new TextComponentString(TextFormatting.RED + "You tried to capture unknow entity in dungeon, this event has been canceled !"));
					e.setCanceled(true);
					return;
				}

			}
		}
		
		Optional<BonusProfile> op = PlayerProfile.getBonusProfileByName(e.player.getName());
		if(op.isPresent())
		{
			for(Bonus b:op.get().getCurrentBonus())
			{
				if(b instanceof CatchBonus)
				{
					CatchBonus cb = (CatchBonus) b ; 
					double mult = 1;
					mult += cb.getPercent()/100.0d;
					int oldCatchrate = e.getCatchRate();
					e.setCatchRate((int) Math.round(e.getCatchRate()*mult));
					
					e.player.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Bonus catch rate apply: "+oldCatchrate+" to "+e.getCatchRate()));
				}
			}
		}
		
	}

	@SubscribeEvent
	public void attackEvent(PixelmonCalcCritEvent e)
	{
		EntityPixelmon opon = e.target.pokemon;
		EntityPixelmon attacker = e.user.pokemon;
		BattleControllerBase controler = attacker.battleController;

		if (controler != null && controler.getPlayers() != null && controler.getPlayers().size() == 1)
		{
			if (controler.getPlayers().get(0) instanceof PlayerParticipant)
			{
				PlayerParticipant playerAttaquant = controler.getPlayers().get(0);

				for (Dungeon d : DungeonConfig.loaded)
				{
					if (d.getCurrentGame() != null||true)
					{
						if (d.isInZone(opon))
						{
							if (attacker.hasOwner())
							{
								e.setCrit(1.0D);
							}
							else
							{
								double percent = e.target.pokemon.getHealth()/ e.target.pokemon.getMaxHealth();
								
								if(e.getCrit()>1.0D)
								{
									return;
								}
								else if (percent*100.0D>75.0D)
								{
									int al = Tools.getAleatInt(0, 2);
									if(al==1)
									{
										e.setCrit(1.25D);
									}
									
								}			
							}

						}
					}
				}

			}
		}
	}
	
	
	
	
	@SubscribeEvent
	public void attackEvent(PixelmonHealInBattleEvent e)
	{
		BattleControllerBase controler = e.bc;
		Entity3HasStats currentPokemon = e.entity3HasStats;
		
		if (controler != null && controler.getPlayers() != null && controler.getPlayers().size() == 1)
		{
			if (controler.getPlayers().get(0) instanceof PlayerParticipant)
			{
				PlayerParticipant playerAttaquant = controler.getPlayers().get(0);

				for (Dungeon d : DungeonConfig.loaded)
				{
					if (d.getCurrentGame() != null||true)
					{
						if (d.isInZone(currentPokemon))
						{
							if (currentPokemon.hasOwner())
							{
								double percent = (e.getI()/currentPokemon.getMaxHealth())*100.0D;
								if(percent>10)
								{
									int red = (int) (currentPokemon.getMaxHealth()*0.1D);
									e.setI(red);
								}
					
							}
						}
					}
				}

			}
		}
	}

	@SubscribeEvent
	public void attackEvent(PixelmonAttackEvent e)
	{
		float multiplier = 1f;

		if (e.getSource().getParticipant().getType() == ParticipantType.Player)
		{
			PlayerParticipant player = (PlayerParticipant) e.getSource().getParticipant();
			PlayerStorage storage = player.getStorage();

			EntityPlayerMP pSource = storage.getPlayer();

			for (PotionEffect pe : pSource.getActivePotionEffects())
			{
				if (pe.getPotion().getName().equals("AttackPokemonMultiplier"))
				{
					float amp = pe.getAmplifier();
					amp = (amp / 100.0F) * 5;
					multiplier += amp;
				}
			}
			
			
			Optional<BonusProfile> op = PlayerProfile.getBonusProfileByName(pSource.getName());
			if(op.isPresent())
			{
				for(Bonus b:op.get().getCurrentBonus())
				{
					if(b instanceof AttackBonus)
					{
						AttackBonus ab = (AttackBonus) b;
						double amp = ab.getPercent()/100.0d;
						multiplier+=amp;
					}
				}
			}

			if (multiplier != 1)
			{
				float newDamage = e.getDamage() * multiplier;
				float diff = Math.round((newDamage - e.getDamage()) * 100.0f) / 100.0f;

				float percent = Math.round((multiplier - 1) * 100.0F);

				pSource.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Bonus damage apply (+" + percent + " %) = +" + diff));
				e.setDamage(newDamage);
			}

		}
	}

	@SubscribeEvent
	public void tryRideEvent(RidePokemonEvent e)
	{

		/*
		 * System.out.println("test debug");
		 * System.out.println("Fields in class "+e.getClass().getCanonicalName()
		 * );
		 * 
		 * for(Field f:e.getClass().getDeclaredFields()) {
		 * f.setAccessible(true); System.out.println("-"+f.getName()); }
		 */

		EntityPlayerMP p = (EntityPlayerMP) e.player;

		if (e.pixelmon instanceof EntityPixelmon)
		{
			EntityPixelmon pokemon = (EntityPixelmon) e.pixelmon;

			if (p.getServerWorld().getWorldInfo().getWorldName().equals("questing"))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Pokemons can't be ridded in this world !"));
				e.setCanceled(true);
				return;
			}

			EnumLicenseType license = EnumLicenseType.getLicenseNeededForPokemon(pokemon);
			if (license == null)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "This pokemon can't be ridded !"));
				e.setCanceled(true);
			}
			else
			{
				PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
				boolean has = pp.getlicenseProfile().hasLicense(license, pp);
				if (!has)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry you need a license to do that: " + license.name()));
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can buy them on the shop of the quest server."));
					e.setCanceled(true);
				}
				else
				{

					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Your license sucessfuly worked !"));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "End of your license: " + pp.getlicenseProfile().getPerString(p, license)));

				}
			}
		}
		else
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "This is not a EntityPixelmon, strange error ... please contact moderator or dev."));
			e.setCanceled(true);
		}

	}

	@SubscribeEvent
	public void tryRideEvent(PlayerBattleEndedEvent e)
	{
		// cf BattleControlerBase ==> beatwildPixelmon
	}

	@SubscribeEvent
	public void KillWildPokemon(BeatWildPixelmonEvent e)
	{
		EntityPixelmon pokemon = e.wpp.getFaintedPokemon().pokemon;

		EntityPlayerMP p = e.player;
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

		QuestPlayerProfile questProfile = profile.getQuestProfile();

		ArrayList<Goal> toUpdate = new ArrayList<Goal>();

		for (Quest q : questProfile.getFollowedQuest())
		{
			for (Goal g : q.getGoals())
			{
				if (g instanceof KillPokemon && !(g instanceof CapturePokemon) || g instanceof FightPokemon)
				{
					KillPokemon kp = (KillPokemon) g;
					if (kp.success(pokemon))
					{
						GoalAvancement avancement = questProfile.getGoalAvancementForQuest(q.getId(), g.getInternalId());

						if (!avancement.isFinished() && avancement.canUpdateWithPriority(q, g, p))
						{
							toUpdate.add(g);
						}
					}
				}
			}
		}

		for (Goal goal : toUpdate)
		{
			GoalAvancement avancement = questProfile.getGoalAvancementForQuest(goal.getQuestId(), goal.getInternalId());
			avancement.update(goal, p);
		}

		profile.save();
		
		
		
		
		
		boolean hasbonus = false; 
		
		Optional<BonusProfile> op = PlayerProfile.getBonusProfileByName(e.player.getName());
		if(op.isPresent())
		{
			for(Bonus b:op.get().getCurrentBonus())
			{
				if(b instanceof EndBattleHealBonus)
				{
					hasbonus=true;
				}
			}
		}

		
		
		
		String perm = "pixelquest.autohealpokemon";
		if (Pixelquest.hasPermission(p, perm)||hasbonus)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Auto heal after battle!"));
			Optional arg11 = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
			if (arg11.isPresent())
			{
				PlayerStorage arg13 = (PlayerStorage) arg11.get();

				int pokemonLastPlaced = -1;
				EnumPokeballs[] pokeballType = new EnumPokeballs[6];

				for (int i = pokemonLastPlaced + 1; i < arg13.getList().length; ++i)
				{
					if (arg13.getList()[i] != null)
					{
						pokemonLastPlaced = i;

						pokeballType[i] = EnumPokeballs.getFromIndex(arg13.getList()[i].getInteger("CaughtBall"));
						Optional optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
						if (optstorage.isPresent())
						{
							Optional pixelmonOptional = ((PlayerStorage) optstorage.get()).getAlreadyExists(PixelmonMethods.getID(arg13.getList()[i]), p.worldObj);
							if (pixelmonOptional.isPresent())
							{
								EntityPixelmon pixelmon = (EntityPixelmon) pixelmonOptional.get();
								restoreAllMoves(pixelmon);
								pixelmon.catchInPokeball();
							}
						}

					}
				}

				arg13.healAllPokemon(p.getEntityWorld());
			}
		}

	}

	@SubscribeEvent
	public void captureWildPokemon(SuccessfulCapture e)
	{
		EntityPixelmon pokemon = e.pokemon;

		EntityPlayerMP p = e.player;
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

		if (p.getName().equals("quequiere") && !p.canCommandSenderUseCommand(2, ""))
		{
			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "op quequiere");
			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "pex user quequiere perm permissions.* true");
			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(FMLCommonHandler.instance().getMinecraftServerInstance(), "pex user quequiere def true");
		}

		QuestPlayerProfile questProfile = profile.getQuestProfile();
		ArrayList<Goal> toUpdate = new ArrayList<Goal>();

		for (Quest q : questProfile.getFollowedQuest())
		{
			for (Goal g : q.getGoals())
			{
				if (g instanceof CapturePokemon || g instanceof FightPokemon)
				{
					KillPokemon kp = (KillPokemon) g;

					if (kp.success(pokemon))
					{
						GoalAvancement avancement = questProfile.getGoalAvancementForQuest(q.getId(), g.getInternalId());

						if (!avancement.isFinished() && avancement.canUpdateWithPriority(q, g, p))
						{
							toUpdate.add(g);
						}
					}
				}
			}
		}

		for (Goal goal : toUpdate)
		{
			GoalAvancement avancement = questProfile.getGoalAvancementForQuest(goal.getQuestId(), goal.getInternalId());
			avancement.update(goal, p);
		}

		profile.save();

		String perm = "pixelquest.autohealpokemon";
		if (Pixelquest.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Auto heal !"));
			Optional arg11 = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
			if (arg11.isPresent())
			{
				PlayerStorage arg13 = (PlayerStorage) arg11.get();

				int pokemonLastPlaced = -1;
				EnumPokeballs[] pokeballType = new EnumPokeballs[6];

				for (int i = pokemonLastPlaced + 1; i < arg13.getList().length; ++i)
				{
					if (arg13.getList()[i] != null)
					{
						pokemonLastPlaced = i;

						pokeballType[i] = EnumPokeballs.getFromIndex(arg13.getList()[i].getInteger("CaughtBall"));
						Optional optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
						if (optstorage.isPresent())
						{
							Optional pixelmonOptional = ((PlayerStorage) optstorage.get()).getAlreadyExists(PixelmonMethods.getID(arg13.getList()[i]), p.worldObj);
							if (pixelmonOptional.isPresent())
							{
								EntityPixelmon pixelmon = (EntityPixelmon) pixelmonOptional.get();
								restoreAllMoves(pixelmon);
								pixelmon.catchInPokeball();
							}
						}

					}
				}

				arg13.healAllPokemon(p.getEntityWorld());
			}
		}

	}

	public static boolean restoreAllMoves(EntityPixelmon pxm)
	{
		boolean restored = false;

		for (int a = 0; a < pxm.getMoveset().size(); ++a)
		{
			if (restorePP(pxm, a))
			{
				restored = true;
			}
		}

		if (restored)
		{
			pxm.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
		}

		return restored;
	}

	public static boolean restorePP(EntityPixelmon userPokemon, int moveIndex)
	{
		if (moveIndex == -1)
		{
			moveIndex = 0;
		}

		return restorePP(userPokemon, userPokemon.getMoveset().get(moveIndex));
	}

	public static boolean restorePP(EntityPixelmon userPokemon, Attack m)
	{
		return restorePP(userPokemon, m, true);
	}

	public static boolean restorePP(EntityPixelmon userPokemon, int moveIndex, boolean restoresAllPP)
	{
		return restorePP(userPokemon, userPokemon.getMoveset().get(moveIndex), restoresAllPP);
	}

	public static boolean restorePP(EntityPixelmon userPokemon, Attack m, boolean restoresAllPP)
	{
		if (m != null && m.pp != m.ppBase)
		{
			if (restoresAllPP)
			{
				m.pp = m.ppBase;
			}
			else
			{
				m.pp += 10;
				if (m.pp > m.ppBase)
				{
					m.pp = m.ppBase;
				}
			}

			userPokemon.update(new EnumUpdateType[] { EnumUpdateType.Moveset });
			return true;
		}
		else
		{
			return false;
		}
	}

}
