package com.quequiere.pixelquest.event;

import java.util.Map;

import com.pixelmonmod.pixelmon.blocks.apricornTrees.BlockApricornTree;
import com.pixelmonmod.pixelmon.blocks.tileEntities.TileEntityApricornTree;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class WorldEvent
{

	@SubscribeEvent
	public void entityInteractEvent(PlaceEvent e)
	{
		if (e.getPlayer() == null)
			return;

		if (e.getBlockSnapshot().getCurrentBlock().getBlock() instanceof BlockApricornTree)
		{
			int count = 0;
			Map<BlockPos, TileEntity> map = e.getWorld().getChunkFromBlockCoords(e.getPos()).getTileEntityMap();

			for (TileEntity te : map.values())
			{
				if (te instanceof TileEntityApricornTree)
				{
					count++;
				}
			}

			int max = 9;

			if (count > max)
			{
				e.setCanceled(true);
				e.getPlayer().addChatMessage(new TextComponentString(TextFormatting.RED + "You can't plant more Apricorn Tree on this chunk ! (" + (count - 1) + "/" + max + ")"));
			}

		}

	}

}
