package com.quequiere.pixelquest.event;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.pixelmon.PixelmonModifier;
import com.quequiere.pixelquest.pixelmon.PixelmonModifierHandler;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.EntityInteract;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PixelmonForgeEvent
{

	
	@SubscribeEvent
	public void interactWithModifier(EntityInteract e)
	{
		EntityPlayerMP p = (EntityPlayerMP) e.getEntityPlayer();
		ItemStack is = p.getHeldItemMainhand();

		if (e.getTarget() instanceof EntityPixelmon && is != null)
		{
			EntityPixelmon pokemon = (EntityPixelmon) e.getTarget();

			if (!pokemon.hasOwner())
			{
				String perm = "pixelquest.usemodifieronwild";
				if (!Pixelquest.hasPermission(p, perm))
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm " + perm + " to use this on a wild pokemon !"));
					return;
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You used a special admin perm to bypass a rule ! " + perm));
				}
			}
			else if (pokemon.getOwner() != null)
			{
				if (pokemon.getOwner() instanceof EntityPlayerMP)
				{
					EntityPlayerMP owner = (EntityPlayerMP) pokemon.getOwner();
					if (!owner.equals(p))
					{
						String perm = "pixelquest.usemodifieronotherplayer";
						if (!Pixelquest.hasPermission(p, perm))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm " + perm + " to use this on an other owner pokemon !"));
							return;
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You used a special admin perm to bypass a rule ! " + perm));
						}
					}
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "This is not a entityPlayer owner pokemon"));
					return;
				}
			}
			
			PixelmonModifierHandler.tryToModifyWithItemStack(is,pokemon,p);

		}
		else if (e.getTarget() instanceof EntityStatue && is != null)
		{
			EntityStatue pokemon = (EntityStatue) e.getTarget();
			String perm = "pixelquest.usemodifierstatue";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm " + perm + " to use this on a statue pokemon !"));
				return;
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You used a special admin perm to bypass a rule ! " + perm));
				PixelmonModifierHandler.tryToModifyWithItemStack(is, pokemon, p);
			}
		}
	}

	@SubscribeEvent
	public void joinWorld(EntityJoinWorldEvent e)
	{
		if (e.getEntity() instanceof EntityPixelmon)
		{
			EntityPixelmon pokemon = (EntityPixelmon) e.getEntity();
			
			PixelmonModifier modifier = PixelmonModifierHandler.getPixelmonModifierProfile(pokemon);

			if (modifier != null)
			{
				if (modifier.getSizeModifier() != null)
				{
					pokemon.setPixelmonScale(modifier.getSizeModifier().getSize());
				}
			}
		}
	}
}
