package com.quequiere.pixelquest.event;

import java.util.ArrayList;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.handler.DungeonDelayedRunnable;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.notification.Notification;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.bonus.BonusType;
import com.quequiere.pixelquest.player.license.EnumLicenseType;
import com.quequiere.pixelquest.player.quest.GoalAvancement;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.type.PickupItemGoal;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.GameType;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.EntityTravelToDimensionEvent;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.RightClickItem;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;

public class PlayerGeneralEvent
{
	
	@SubscribeEvent
	public void respawnEvent (PlayerRespawnEvent e)
	{
		final EntityPlayerMP p = (EntityPlayerMP) e.player;
		
		for(final Dungeon dcheck:DungeonConfig.loaded)
		{
		
			if(dcheck.getCurrentGame()==null)
			{
				continue;
			}
			
			
			if(dcheck.getCurrentGame().getProfile().getPlayers().contains(p.getName()))
			{
				
				boolean find = false;
				for(EntityPlayerMP inside:dcheck.getCurrentGame().getProfile().getOnlineTeamPlayers())
				{
					if(dcheck.isInZone(inside))
					{
						
						p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Planned teleport ..."));
						
						DungeonDelayedRunnable.toExec.put(System.currentTimeMillis()+1000, new Runnable()
						{
							
							@Override
							public void run()
							{
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Teleport ..."));
								dcheck.getSpawnPoint().teleportPlayer(p);
							}
						});
						
						find =true;
						break;
					}
				}
				
				if(!find)
				{
					for(EntityPlayerMP pp : dcheck.getCurrentGame().getProfile().getOnlineTeamPlayers())
					{
						pp.addChatMessage(new TextComponentString(TextFormatting.RED + "Not enought player in dungeon, dungeon closing !"));
					}
					
					dcheck.close();
					
					return;
				}
				
				
				
				for(EntityPlayerMP pp : dcheck.getCurrentGame().getProfile().getOnlineTeamPlayers())
				{
					pp.addChatMessage(new TextComponentString(TextFormatting.AQUA + "A player of your team died ! Use /d invoke "+p.getName()+" to teleport him inside dungeon !"));
				}
			
				return;
			}
			
		}
		
	}
	
	@SubscribeEvent
	public void changeDimPlayerEvent (EntityTravelToDimensionEvent e)
	{
		if(e.getEntity() instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) e.getEntity();
			p.capabilities.allowFlying=false;
		}
		
	}
	
	@SubscribeEvent
	public void changeDimPlayerEvent (PlayerChangedDimensionEvent e)
	{
		e.player.capabilities.allowFlying=false;
	}

	@SubscribeEvent
	public void leaveEvent (PlayerLoggedOutEvent e)
	{
		for(Dungeon d:DungeonConfig.loaded)
		{
			if(d.getCurrentGame()==null)
			{
				if(d.getQueue().getPlayers().contains(e.player.getName()))
				{
					d.getQueue().getPlayers().remove(e.player.getName());
				}
				
			}
			else
			{
				if(d.getCurrentGame().getProfile().getOnlineTeamPlayers().size()<=0)
				{
					d.close();
					for(EntityPlayerMP p:Tools.getAllPlayers())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.BLUE + d.getName()+" has been closed cause need more player !"));
					}
				}
				
			}
		}
		
		PlayerProfile.unloadProfile(e.player.getName());
		
	}
	
	
	
	
	@SubscribeEvent
	public void joinWorld(PlayerLoggedInEvent e)
	{
		for(EntityPlayerMP ppp:Tools.getAllPlayers())
		{
			ppp.addChatMessage(new TextComponentString(TextFormatting.GREEN + ""+TextFormatting.BOLD+"[+] "+e.player.getName()));
		}
		
	}
	
	@SubscribeEvent
	public void joinWorld(EntityJoinWorldEvent e)
	{
		if (e.getEntity() instanceof EntityPlayerMP)
		{
			Quest.rattachObject();
			EntityPlayerMP p = (EntityPlayerMP) e.getEntity();
			if(p.interactionManager.getGameType().equals(GameType.SPECTATOR))
			{
				p.setGameType(GameType.SURVIVAL);
			}
			
			Notification.clear(p);
			if (p.getName() == null)
			{
				System.out.println("Fatal error while join server ...");
			}
			PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), true);
			profile.getQuestProfile().setHideQuest(false);
			profile.getQuestProfile().refreshQuest(p);

		}
	}

	@SubscribeEvent
	public void pickup(EntityItemPickupEvent e)
	{

		EntityItem ent = e.getItem();
		Item i = ent.getEntityItem().getItem();

		EntityPlayerMP p = (EntityPlayerMP) e.getEntityPlayer();
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

		QuestPlayerProfile questProfile = profile.getQuestProfile();

		ArrayList<Goal> toUpdate = new ArrayList<Goal>();

		boolean success = false;

		for (Quest q : questProfile.getFollowedQuest())
		{
			for (Goal g : q.getGoals())
			{
				if (g instanceof PickupItemGoal)
				{
					PickupItemGoal kp = (PickupItemGoal) g;
					if (kp.isCompatible(ent.getEntityItem()))
					{
						GoalAvancement avancement = questProfile.getGoalAvancementForQuest(q.getId(), g.getInternalId());

						if (!avancement.isFinished() && avancement.canUpdateWithPriority(q, g, p))
						{
							toUpdate.add(g);
							success = true;
						}
					}
				}
			}
		}

		for (Goal goal : toUpdate)
		{
			GoalAvancement avancement = questProfile.getGoalAvancementForQuest(goal.getQuestId(), goal.getInternalId());
			avancement.update(goal, p);
		}
		
		if(toUpdate.size()>0 || success)
		{
			profile.save();
		}

		
		if (success)
		{
			e.setCanceled(true);
			ent.getEntityItem().stackSize--;
		}

	}

	@SubscribeEvent
	public void rightClick(RightClickItem e)
	{
		EntityPlayerMP p = (EntityPlayerMP) e.getEntityPlayer();
		ItemStack is = e.getItemStack();

		if (is == null)
		{
			return;
		}

		if (is.getDisplayName().contains("License rider"))
		{

			NBTTagCompound c = is.getTagCompound();
			NBTTagCompound d = c.getCompoundTag("display");
			if (d.hasKey("Lore"))
			{
				NBTTagList nbttaglist3 = d.getTagList("Lore", 8);
				if (!(nbttaglist3.hasNoTags()))
				{
					EnumLicenseType type = null;
					long timeInMinutes=-1;
					
					for (int l1 = 0; l1 < nbttaglist3.tagCount(); ++l1)
					{
						

						String s = nbttaglist3.getStringTagAt(l1);
						if (s.contains("Type:"))
						{
							String key = s.split(":")[1];
							try
							{
								type = EnumLicenseType.valueOf(key);
							}
							catch (IllegalArgumentException ex)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this license type :" + key + "|"));
							}

						}
						else if (s.contains("timeInMinutes:"))
						{
							String key = s.split(":")[1];
							try
							{
								timeInMinutes = Long.parseLong(key);
							}
							catch (NumberFormatException ex)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse time in minutes :" + key + "|"));
							}

						}
					}
					
					if(type!=null&&timeInMinutes>0)
					{
						PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
						pp.getlicenseProfile().giveLicense(type, timeInMinutes, pp);
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "License successfuly added !"));
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "License end date: "+pp.getlicenseProfile().getPerString(p, type)));
						is.stackSize--;
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while apply this license !"));
					}

					
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl2 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl1 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
			}

		}
		else if (is.getDisplayName().contains("Bonus "))
		{

			NBTTagCompound c = is.getTagCompound();
			NBTTagCompound d = c.getCompoundTag("display");
			if (d.hasKey("Lore"))
			{
				NBTTagList nbttaglist3 = d.getTagList("Lore", 8);
				if (!(nbttaglist3.hasNoTags()))
				{
					BonusType type = null;
					long timeInMinutes=-1;
					
					for (int l1 = 0; l1 < nbttaglist3.tagCount(); ++l1)
					{
						

						String s = nbttaglist3.getStringTagAt(l1);
						if (s.contains("Type:"))
						{
							String key = s.split(":")[1];
							try
							{
								type = BonusType.valueOf(key);
							}
							catch (IllegalArgumentException ex)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this license type :" + key + "|"));
							}

						}
						else if (s.contains("timeInMinutes:"))
						{
							String key = s.split(":")[1];
							try
							{
								timeInMinutes = Long.parseLong(key);
							}
							catch (NumberFormatException ex)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't parse time in minutes :" + key + "|"));
							}

						}
					}
					
					if(type!=null&&timeInMinutes>0)
					{
						PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
						
						System.out.println("todo");
					
						
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Bonus successfuly added !"));
						
						is.stackSize--;
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while apply this license !"));
					}

					
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl2 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheater lvl1 Did you try to cheat ? This has been registred ... you should send a message to moderator."));
			}

		}
	}

}
