package com.quequiere.pixelquest.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityStatue;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.pixelmon.PixelmonModifier;
import com.quequiere.pixelquest.pixelmon.PixelmonModifierHandler;
import com.quequiere.pixelquest.pixelmon.modifier.PixelmonSizeModifier;
import com.quequiere.pixelquest.pixelmon.modifier.PixelmonStatueFrameModifier;
import com.quequiere.pixelquest.travel.Travel;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ServerTickEvent
{

	public static HashMap<EntityPlayerMP, Travel> travelsSync = new HashMap<EntityPlayerMP, Travel>();

	public static ArrayList<Runnable> tasks = new ArrayList<Runnable>();
	
	
	@SubscribeEvent
	public void taksExecute(net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent e)
	{
		try
		{
			for(Runnable r:tasks)
			{
				r.run();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SubscribeEvent
	public void travelSyncro(net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent e)
	{
		ArrayList<EntityPlayerMP> torem = new ArrayList<EntityPlayerMP>();
		for (EntityPlayerMP p : travelsSync.keySet())
		{
			Travel t = travelsSync.get(p);
			t.startFly(p);
			torem.add(p);
		}

		for (EntityPlayerMP p : torem)
		{
			travelsSync.remove(p);
		}
	}

	@SubscribeEvent
	public void statueFrameUpdater(net.minecraftforge.fml.common.gameevent.TickEvent.ServerTickEvent e)
	{

		for (UUID id : PixelmonModifierHandler.pixelmonModifier.keySet())
		{
			PixelmonModifier modifier = PixelmonModifierHandler.pixelmonModifier.get(id);
			PixelmonStatueFrameModifier framemod = modifier.getPixelmonStatueFrameModifier();
			PixelmonSizeModifier sizemod = modifier.getSizeModifier();

			if (sizemod != null)
			{
				Entity ent = Tools.getEntityByUUID(id);

				if (ent != null && ent instanceof EntityStatue)
				{
					if (modifier.getSizeModifier() != null)
					{
						EntityStatue statue = (EntityStatue) ent;
						float size = sizemod.getSize();
						if (statue.getPixelmonScale() != size)
						{
							statue.setPixelmonScale(size);
						}

					}
				}
			}

			if (framemod != null && framemod.isActive())
			{
				Entity ent = Tools.getEntityByUUID(id);

				if (ent != null && ent instanceof EntityStatue)
				{
					EntityStatue statue = (EntityStatue) ent;
					framemod.update(statue);
				}
			}
		}
	}

}
