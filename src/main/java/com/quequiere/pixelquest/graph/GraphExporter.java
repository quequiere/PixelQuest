package com.quequiere.pixelquest.graph;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GraphExporter
{
	public ArrayList<Edges> edges = new ArrayList<Edges>();
	public ArrayList<Nodes> nodes = new ArrayList<Nodes>();
	
	
	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().setPrettyPrinting().create();
		return gson.toJson(this);
	}
}
