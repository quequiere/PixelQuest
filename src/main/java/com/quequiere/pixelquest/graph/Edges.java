package com.quequiere.pixelquest.graph;

public class Edges
{
	public String label;
	public String source;
	public String target;
	public String id;
	public String color;
	
	
	public Edges(String label, String source, String target, String id,String color)
	{
		this.label = label;
		this.source = source;
		this.target = target;
		this.id = id;
		this.color=color;
	}
	
	
}
