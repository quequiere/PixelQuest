package com.quequiere.pixelquest.api;

import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.experience.Rank;

public class PixelQuestApi
{
	public static int getPlayerLevelTotal(String pname)
	{
		PlayerProfile pp = PlayerProfile.getPlayerProfile(pname, false);
		return pp.getExpProfile().getTotalLvl();
	}
	
	public static String getPrefixWithLevel(int level)
	{
		return Rank.getRankByLvl(level).name();
	}
	
	public static Rank[] getRanks()
	{
		return Rank.values();
	}
}
