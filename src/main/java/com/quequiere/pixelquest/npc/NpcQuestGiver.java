package com.quequiere.pixelquest.npc;

import java.util.ArrayList;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.SetNPCData;
import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
import com.pixelmonmod.pixelmon.entities.npcs.registry.BaseShopItem;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItem;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItemWithVariation;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopkeeperChat;
import com.pixelmonmod.pixelmon.enums.EnumGui;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;
import com.quequiere.pixelquest.quest.Quest;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;

public class NpcQuestGiver
{
	private ArrayList<Integer> questList = new ArrayList<Integer>();

	public void addQuest(int id)
	{
		if (!questList.contains(id))
		{
			questList.add(id);
		}
		else
		{
			System.out.println("Npc already have this quest ...");
		}
	}

	public void displayGui(EntityPlayerMP p, NPCShopkeeper shop)
	{
		shop.setName("Quest giver");
		Pixelmon.network.sendTo(new SetNPCData("QuestGiver", new ShopkeeperChat("You can take quest here ! It's free !", "Good luck for you quest !"), this.getItemList(p), new ArrayList<ShopItemWithVariation>()), p);
		p.openGui(Pixelmon.instance, EnumGui.Shopkeeper.getIndex().intValue(), p.getEntityWorld(), 0, 0, 0);

		// reponse client = ShopKeeperPacket

	}

	public ArrayList<ShopItemWithVariation> getItemList(EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		QuestPlayerProfile questProfile = profile.getQuestProfile();

		ArrayList<Integer> toremove = new ArrayList<Integer>();

		ArrayList<ShopItemWithVariation> list = new ArrayList<ShopItemWithVariation>();
		for (Integer i : this.questList)
		{
			Quest q = Quest.getQuestById(i);
			String name = "This is a bug, plz call op";

			if (q != null)
			{
				name = q.getName();
			}

			if (q == null)
			{
				System.out.println("ERROR ON NPC has no existing quest ... we remove it!");
				toremove.add(i);
				continue;
			}

			ItemStack is = new ItemStack(Blocks.STAINED_GLASS_PANE);
			if (questProfile.canDoQuest(q))
			{
				is.setItemDamage(EnumDyeColor.GREEN.getMetadata());
			}
			else
			{
				is.setItemDamage(EnumDyeColor.RED.getMetadata());
			}

			is.setStackDisplayName(name + " quest");
			BaseShopItem base = new BaseShopItem("questid:" + q.getId(), is, 0, -1);
			ShopItem shopitem = new ShopItem(base, 0, 0, false);
			ShopItemWithVariation si = new ShopItemWithVariation(shopitem);
			list.add(si);
		}

		boolean modif = false;
		for (Integer i : toremove)
		{
			this.questList.remove(i);
			modif = true;
		}

		if (modif)
		{
			NpcGestionnaire.instance.save();
		}

		return list;
	}

	public ArrayList<Integer> getQuestList()
	{
		return questList;
	}
	
	

}
