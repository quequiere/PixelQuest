package com.quequiere.pixelquest.npc;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.apache.commons.lang3.SerializationUtils;

import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
import com.pixelmonmod.pixelmon.entities.npcs.registry.BaseShopItem;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItem;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItemWithVariation;
import com.quequiere.pixelquest.player.license.EnumLicenseType;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class NpcShopKeeperCustom
{
	private ArrayList<BaseShopItem> items = new ArrayList<BaseShopItem>();

	public void addItem(ItemStack is, int buy, int sell)
	{
		items.add(new BaseShopItem(is.getDisplayName(), is, buy, sell));
	}

	public ArrayList<BaseShopItem> getItems()
	{
		return items;
	}

	public void reloaditemList(NPCShopkeeper shop, EntityPlayerMP p)
	{
		shop.getItemList().clear();

		for (ShopItemWithVariation item : this.getCustomList(p))
		{
			if (item.getItem() == null)
			{
				System.out.println("Error while loading item !");
				continue;
			}
			shop.getItemList().add(item);
		}
	}

	public static int getPlayerDiscount(EntityPlayerMP p)
	{
		ItemStack is = p.getHeldItemMainhand();
		int discount = -1;

		if (is != null && is.getDisplayName().contains("Discount card ") && is.getItem().equals(Items.PAPER))
		{
			NBTTagCompound c = is.getTagCompound();
			NBTTagCompound d = c.getCompoundTag("display");
			if (d.hasKey("Lore"))
			{
				NBTTagList nbttaglist3 = d.getTagList("Lore", 8);
				if (!(nbttaglist3.hasNoTags()))
				{
					for (int l1 = 0; l1 < nbttaglist3.tagCount(); ++l1)
					{

						String s = nbttaglist3.getStringTagAt(l1);
						if (s.contains("Percent:"))
						{
							try
							{
								discount = Integer.parseInt(s.split(":")[1]);
							}
							catch (NumberFormatException e)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while reading item ! Contact administrator or dev"));
							}
						}

					}
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Cheat try registered. You should contact an administrator !"));
				}
			}

		}
		return discount;
	}

	public ArrayList<ShopItemWithVariation> getCustomList(EntityPlayerMP p)
	{

		ArrayList<ShopItemWithVariation> list = new ArrayList<ShopItemWithVariation>();

		int discount = 0;
		
		if(p!=null)
		{
			discount=getPlayerDiscount(p);
		}

		for (BaseShopItem base : this.items)
		{
			BaseShopItem toadd = base;

			if (discount > 0)
			{

				try
				{
					Field buyf = base.getClass().getDeclaredField("buy");
					buyf.setAccessible(true);
					Field sellf = base.getClass().getDeclaredField("sell");
					sellf.setAccessible(true);

					int originalPrice = buyf.getInt(base);
					float newPrice = originalPrice * (1 - (discount / 100.0f));
					int fiprice = Math.round(newPrice);
					
					if(fiprice<=0)
					{
						fiprice=1;
					}
					
					toadd = new BaseShopItem(base.id, base.getItem(), fiprice, sellf.getInt(base));
				}
				catch (IllegalArgumentException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while write discount ! 1"));
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while write discount ! 2"));
					e.printStackTrace();
				}
				catch (NoSuchFieldException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while write discount ! 4"));
					e.printStackTrace();
				}
				catch (SecurityException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while write discount ! 5"));
					e.printStackTrace();
				}

			}

			ShopItem shopitem = new ShopItem(toadd, 0, 0, false);
			ShopItemWithVariation si = new ShopItemWithVariation(shopitem);
			list.add(si);
		}
		return list;
	}

}
