package com.quequiere.pixelquest.npc;

import net.minecraft.item.ItemStack;

public class NpcShopKeeperCreator
{

	public ItemStack is;
	public int buyPrice,sellPrice;
	
	public NpcShopKeeperCreator(ItemStack is, int buyPrice, int sellPrice)
	{
		super();
		this.is = is;
		this.buyPrice = buyPrice;
		this.sellPrice = sellPrice;
	}
	
	
}
