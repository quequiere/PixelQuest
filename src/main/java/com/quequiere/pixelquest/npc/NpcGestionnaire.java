package com.quequiere.pixelquest.npc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.tools.ItemStackDeserializer;
import com.quequiere.pixelquest.tools.ItemStackSerializer;

import net.minecraft.item.ItemStack;

public class NpcGestionnaire
{
	public static NpcGestionnaire instance;
	private HashMap<String, NpcQuestGiver> npcQuesters = new HashMap<String, NpcQuestGiver>();
	private HashMap<String, NpcShopKeeperCustom> npcShopKeerpers = new HashMap<String, NpcShopKeeperCustom>();

	public void addQuestGiver(String uuid, NpcQuestGiver npc)
	{
		if (npcQuesters.get(uuid) != null)
		{
			System.out.println("Error, try to add existing quester");
			return;
		}
		npcQuesters.put(uuid, npc);
	}

	public Set<String> getUuid()
	{
		return this.npcQuesters.keySet();
	}

	public NpcQuestGiver getNpcQuestGiver(String uuid)
	{
		return npcQuesters.get(uuid);
	}

	public void addCustomShopKeeper(String uuid, NpcShopKeeperCustom npc)
	{
		if (this.getNpcShopKeerpers().get(uuid) != null)
		{
			System.out.println("Error, try to add existing customNpc");
			return;
		}
		this.getNpcShopKeerpers().put(uuid, npc);
	}

	private HashMap<String, NpcShopKeeperCustom> getNpcShopKeerpers()
	{
		if (this.npcShopKeerpers == null)
		{
			npcShopKeerpers = new HashMap<String, NpcShopKeeperCustom>();
			this.save();
		}
		return npcShopKeerpers;
	}

	public NpcShopKeeperCustom getnpcShopKeerper(String uuid)
	{
		return this.getNpcShopKeerpers().get(uuid);
	}

	public String toJson()
	{
		ItemStackSerializer as = new ItemStackSerializer();
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, as).create();
		return gson.toJson(this);
	}

	public static NpcGestionnaire fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, new ItemStackDeserializer()).create();
		return gson.fromJson(s, NpcGestionnaire.class);
	}

	public void save()
	{
		Writer writer = null;
		try
		{

			File folder = new File(Pixelquest.folder.getAbsolutePath() + "/npc/");
			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/questGiver.json");
			if (!file.exists())
			{
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());

			// writer = new BufferedWriter(new FileWriter(file));
			// writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public static NpcGestionnaire reloadFile()
	{
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/npc/");
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/questGiver.json");

		if (f.exists())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				NpcGestionnaire pp = NpcGestionnaire.fromJson(everything);
				return pp;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			System.out.println("Cant find: " + f.getAbsolutePath());
			try
			{
				f.createNewFile();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

}
