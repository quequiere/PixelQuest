package com.quequiere.pixelquest.npc;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.travel.Travel;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.WorldServer;

public class NpcParticleHandler extends TimerTask
{
	public static NpcParticleHandler notifHandler;
	private static Timer timer = new Timer();

	@Override
	public void run()
	{

		for (String uuid : NpcGestionnaire.instance.getUuid())
		{
			NpcQuestGiver giver = NpcGestionnaire.instance.getNpcQuestGiver(uuid);
			Entity e = Tools.getEntityByUUID(uuid);

			if (e != null)
			{
				WorldServer w = (WorldServer) e.getEntityWorld();
				for (EntityPlayerMP p : Tools.getAllPlayers())
				{
					
					if(!w.equals(p.getServerWorld()))
					{
						continue;
					}
					
					BlockPos a = new BlockPos(e.posX, e.posY, e.posZ);
					double dist = a.getDistance((int) p.posX, (int) p.posY, (int) p.posZ);
					if (dist < 32)
					{
						PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
						QuestPlayerProfile questProfile = profile.getQuestProfile();
						boolean can = false;
						
						for(Integer qid :giver.getQuestList())
						{
							Quest q = Quest.getQuestById(qid);
							if(q!=null)
							{
								if(questProfile.canDoQuest(q))
								{
									can=true;
								}
							}
						}
						
						if(can)
						{
							w.spawnParticle(p, EnumParticleTypes.VILLAGER_ANGRY, false, e.posX, e.posY + 2.2, e.posZ, 1, 0.5, 0, 0.5, 1, 0);
						}
						else
						{
							w.spawnParticle(p, EnumParticleTypes.BARRIER, false, e.posX, e.posY + 3, e.posZ, 1, 0, 0, 0, 1, 0);
						}
						
					}

				}
			}
		}
		
		for (UUID uuid : Travel.getAllids())
		{
			Entity e = Tools.getEntityByUUID(uuid);

			if (e != null)
			{
				WorldServer w = (WorldServer) e.getEntityWorld();
				for (EntityPlayerMP p : Tools.getAllPlayers())
				{
					BlockPos a = new BlockPos(e.posX, e.posY, e.posZ);
					double dist = a.getDistance((int) p.posX, (int) p.posY, (int) p.posZ);
					if (dist < 32)
					{
						w.spawnParticle(p, EnumParticleTypes.VILLAGER_HAPPY, false, e.posX, e.posY + 2.2, e.posZ,2 , 0.5 , 0.5, 0.5, 1,0);
					}

				}
			}
		}
	}

	public static void startHandler()
	{
		notifHandler = new NpcParticleHandler();
		timer.schedule(notifHandler, 1 * 1000, 1 * 500);
	}

}
