package com.quequiere.pixelquest.effect;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionUtils;
import net.minecraft.util.text.TextFormatting;

public class PotionHandler
{	
	public static void give(EntityPlayerMP p,int amp, String type)
	{
		Potion pa=null;
		String name ="";
		
		if(type.equals("exp"))
		{
			pa = Potion.getPotionById(11);
			pa.setPotionName("ExpPokemonMultiplier");
			name="Exp multiplicator potion "+amp;
		}
		else if(type.equals("attack"))
		{
			pa = Potion.getPotionById(12);
			pa.setPotionName("AttackPokemonMultiplier");
			name="Attack multiplicator potion "+(amp*5)+" %";
		}
		
		
		PotionEffect pe = new PotionEffect(pa,20*60*3,amp);
		ArrayList<PotionEffect> list = new ArrayList<PotionEffect>();
		list.add(pe);
		
		ItemStack is = PotionUtils.appendEffects(new ItemStack(Items.POTIONITEM), list);
		

		is.setStackDisplayName(TextFormatting.AQUA + name);
		NBTTagCompound c = is.getTagCompound();
		NBTTagCompound d = c.getCompoundTag("display");
		d.setTag("Lore", new NBTTagList());
		NBTTagList nbttaglist3 = d.getTagList("Lore", 8);

		nbttaglist3.appendTag(new NBTTagString(TextFormatting.DARK_AQUA+"This give you a special effect available for all your pokemon"));
		nbttaglist3.appendTag(new NBTTagString(TextFormatting.AQUA+"Drink the same potion type will not add times effets !"));

		
		
		p.inventory.addItemStackToInventory(is);
	}
}
