package com.quequiere.pixelquest.packet.injector;

import java.lang.reflect.Field;
import java.util.EnumMap;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.ShopKeeperPacket;
import io.netty.channel.ChannelHandler;
import net.minecraftforge.fml.common.network.FMLEmbeddedChannel;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleChannelHandlerWrapper;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleIndexedCodec;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketInjector
{

	public static void inject()
	{
		System.out.println("PixelQuest: Trying pixelmonMod packet injection");

		SimpleNetworkWrapper net = Pixelmon.network;
		for (Field f : net.getClass().getDeclaredFields())
		{
			if (f.getName().equals("channels"))
			{
				f.setAccessible(true);
				try
				{
					EnumMap<Side, FMLEmbeddedChannel>channels = (EnumMap<Side, FMLEmbeddedChannel>) f.get(net);
					
					FMLEmbeddedChannel channel = channels.get(Side.SERVER);
					for (ChannelHandler s : channel.pipeline().toMap().values())
					{
						if (s instanceof SimpleChannelHandlerWrapper)
						{
							SimpleChannelHandlerWrapper w = (SimpleChannelHandlerWrapper) s;
							for (Field ff : w.getClass().getDeclaredFields())
							{
								if (ff.getName().equals("messageHandler"))
								{
									ff.setAccessible(true);
									Object o = ff.get(w);
									
									if (o instanceof ShopKeeperPacket.Handler)
									{
										System.out.println("PixelQuest: ShopKeeperPacket.Handle founded ! ");
										channel.pipeline().remove(w);
										System.out.println("PixelQuest: Packet deleted ...");
										String type = channel.findChannelHandlerNameForType(SimpleIndexedCodec.class);
										SimpleChannelHandlerWrapper handler = new SimpleChannelHandlerWrapper(ShopKeeperPacketHander.class, Side.SERVER, ShopKeeperPacket.class);
										System.out.println("PixelQuest: New injector packet created !");
										channel.pipeline().addAfter(type, "test", handler);
										System.out.println("PixelQuest: Hack packet added =D ! ");
										return;
									}
								}
							}
						}
					}
				}
				catch (IllegalArgumentException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

	}
}
