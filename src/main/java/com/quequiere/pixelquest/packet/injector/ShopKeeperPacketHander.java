package com.quequiere.pixelquest.packet.injector;

import java.lang.reflect.Field;
import java.util.ConcurrentModificationException;
import java.util.Optional;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.comm.packetHandlers.clientStorage.UpdateClientPlayerData;
import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.ShopKeeperPacket;
import com.pixelmonmod.pixelmon.entities.npcs.EntityNPC;
import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixelquest.event.ServerTickEvent;
import com.quequiere.pixelquest.npc.NpcGestionnaire;
import com.quequiere.pixelquest.npc.NpcShopKeeperCustom;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.travel.Travel;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ShopKeeperPacketHander extends ShopKeeperPacket.Handler
{

	public IMessage onMessage(ShopKeeperPacket message, MessageContext ctx)
	{
		String ids = null;
		int idnpc = 0;
		int amount = 0;
		for (Field f : ShopKeeperPacket.class.getDeclaredFields())
		{
			f.setAccessible(true);

			if (f.getName().equals("itemID"))
			{
				try
				{
					ids = (String) f.get(message);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}

			}
			else if (f.getName().equals("shopKeeperID"))
			{
				try
				{
					idnpc = f.getInt(message);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
			}
			else if (f.getName().equals("amount"))
			{
				try
				{
					amount = f.getInt(message);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}
			}

		}

		EntityPlayerMP p = ctx.getServerHandler().playerEntity;
		Optional npcOptional = EntityNPC.locateNPCServer(p.worldObj, idnpc, NPCShopkeeper.class);

		if (idnpc == 0)
		{

			PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

			String name = ids.split(":")[0];

			if (name.equals("questid"))
			{
				int id = Integer.parseInt(ids.split(":")[1]);
				Quest q = Quest.getQuestById(id);
				p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Try to give quest from npc giver."));
				if (profile.getQuestProfile().canDoQuest(q))
				{
					profile.getQuestProfile().addQuest(q, p, false);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Quest added !"));
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't do this quest for now."));
				}
			}
			else if (name.equals("flyto"))
			{
				try
				{
					Travel t = Travel.getByName(ids.split(":")[1]);
					if (t == null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error, this fly doesnt exist ! This is a bug, contact admin !"));
						p.closeScreen();
					}
					else
					{

						Optional optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(p);
						if (optstorage.isPresent())
						{
							int cost = t.getCost(p);
							PlayerStorage storage = (PlayerStorage) optstorage.get();
							if (storage.getCurrency() >= cost)
							{
								storage.removeCurrency(cost);
							
								Pixelmon.network.sendTo(new UpdateClientPlayerData(storage.getCurrency()), p);
								p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Starting fly to " + t.getName() + " for " + cost + " $"));

								p.addChatMessage(new TextComponentString(TextFormatting.DARK_PURPLE + "--------------------"));
								p.addChatMessage(new TextComponentString(TextFormatting.DARK_PURPLE + "Did you know it ?"));
								p.addChatMessage(new TextComponentString(TextFormatting.DARK_PURPLE + "You can change the flight speed with /travel speed"));
								p.addChatMessage(new TextComponentString(TextFormatting.DARK_PURPLE + "(Going faster is more expensive, can be change only before fly)"));
								p.addChatMessage(new TextComponentString(TextFormatting.DARK_PURPLE + "--------------------"));
								p.closeScreen();
								// t.startFly(p);
								// pour eviter asynchronique probleme
								ServerTickEvent.travelsSync.put(p, t);
							}
							else
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "You don't have enought money !"));
								p.closeScreen();
							}
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Wooo ... your are not loaded :o ! Please contact an administrator !"));
							p.closeScreen();
						}

					}
				}
				catch (ConcurrentModificationException e)
				{
					e.printStackTrace();
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Fatal error :o !"));
				}

			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Packet error, call admin !"));
				p.closeScreen();
			}

			return null;
		}
		else
		{
			NPCShopkeeper npc = (NPCShopkeeper) npcOptional.get();
			NpcShopKeeperCustom custom = NpcGestionnaire.instance.getnpcShopKeerper(npc.getPersistentID().toString());

			if (custom != null && NpcShopKeeperCustom.getPlayerDiscount(p)>0)
			{
				custom.reloaditemList(npc, p);
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You used a discount card !"));
				p.inventory.removeStackFromSlot(p.inventory.currentItem);
				p.closeScreen();
				
				try
				{
					Field f = message.getClass().getDeclaredField("amount");
					f.setAccessible(true);
					f.set(message, 1);
					return super.onMessage(message, ctx);
				}
				catch (IllegalArgumentException e)
				{
					e.printStackTrace();
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Packet error, call admin !1"));
				}
				catch (IllegalAccessException e)
				{
					e.printStackTrace();
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Packet error, call admin !2"));
				}
				catch (NoSuchFieldException e)
				{
					e.printStackTrace();
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Packet error, call admin !3"));
				}
				catch (SecurityException e)
				{
					e.printStackTrace();
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Packet error, call admin !4"));
				}
				
				return null;
			
			}
			else
			{
				return super.onMessage(message, ctx);
			}

		}

	}
}
