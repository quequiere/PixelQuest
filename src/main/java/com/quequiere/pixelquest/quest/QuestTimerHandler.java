package com.quequiere.pixelquest.quest;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.event.ServerTickEvent;
import com.quequiere.pixelquest.notification.Notification;
import com.quequiere.pixelquest.player.PlayerProfile;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;

public class QuestTimerHandler  implements Runnable
{
	public static QuestTimerHandler questHandler;
	private long last = 0;
	private static long timer= 10*1000;
	
	private HashMap<EntityPlayerMP, HashMap<Integer, Integer>> notified = new HashMap<EntityPlayerMP, HashMap<Integer, Integer>>();

	@Override
	public void run()
	{
		
		long now = System.currentTimeMillis();
		long diff = now-last;

		if(diff<timer)
		{
			return;
		}
		
		last = now;
		
		
		for (EntityPlayerMP p : Tools.getAllPlayers())
		{
			PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(),false);
			profile.getQuestProfile().checkTimerQuest(p);
			
			HashMap<Integer, Integer> map=null;
			
			if(notified.containsKey(p))
			{
				map = notified.get(p);
			}
			else
			{
				map = new HashMap<Integer, Integer>();
				notified.put(p, map);
			}
			

			for (Quest q : profile.getQuestProfile().getFollowedQuest())
			{
				if (q.getTimerSecond() == -1)
				{
					continue;
				}

				float percent = (float) profile.getQuestProfile().getTakedTimeInSec(q) / (float) q.getTimerSecond();
				percent *= 100.0f;
				
				int lastNotifPercent = 0;
				
				if(map.containsKey(q.getId()))
				{
					lastNotifPercent=map.get(q.getId());
				}

				Integer[] percents= {50,70,90,95};
				
				if(percent<lastNotifPercent)
				{
					lastNotifPercent=0;
				}

				for(int per:percents)
				{
					if(percent>=per && lastNotifPercent!=per && !(lastNotifPercent>per))
					{
						lastNotifPercent=per;
						Notification.addNotification(p, 7*1000, TextFormatting.RED+q.getName()+TextFormatting.WHITE+" quest timer: "+TextFormatting.GREEN+per+TextFormatting.GREEN+" %");
					}
				}
				
				
				map.remove(q.getId());
				if(!(percent>95))
				{
					map.put(q.getId(), lastNotifPercent);
				}
				

			}
		}

	}

	public static void startHandler()
	{
		questHandler = new QuestTimerHandler();
		ServerTickEvent.tasks.add(questHandler);
	}

}
