package com.quequiere.pixelquest.quest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.goal.CustomGoalComparator;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.GoalDeserializer;
import com.quequiere.pixelquest.quest.goal.GoalType;
import com.quequiere.pixelquest.quest.goal.type.CapturePokemon;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.FightPokemon;
import com.quequiere.pixelquest.quest.goal.type.GoalRepeatable;
import com.quequiere.pixelquest.quest.goal.type.KillPokemon;
import com.quequiere.pixelquest.quest.goal.type.LocationGoal;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.RewardDeserializer;
import com.quequiere.pixelquest.quest.reward.RewardType;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextFormatting;

public class Quest
{

	private static ArrayList<Quest> questList = new ArrayList<Quest>();

	private Location startPos;
	private ArrayList<Reward> failRewards = new ArrayList<Reward>();
	private ArrayList<Reward> rewardList = new ArrayList<Reward>();
	private ArrayList<Goal> goals = new ArrayList<Goal>();
	private ArrayList<Integer> preRequis = new ArrayList<Integer>();
	private ArrayList<String> intro = new ArrayList<String>();
	private ArrayList<String> outro = new ArrayList<String>();
	private int id;
	private String name;
	private int timerSecond;
	private boolean canRemove = true;

	private boolean canBeRepeated = true;
	private boolean canBeRepeatedIfWin = true;
	private long timerInSecondBeforeRepeat = -1;
	private boolean hasSharedTimerBeforeRepeat = false;
	
	private String introCinematicName ="";

	private Quest(int id, String name, ArrayList<Goal> g, ArrayList<Reward> r, int timer, boolean canBeReapeted, boolean canBeReapetedIfWin, long timebeforeinsec, Location startPos2)
	{
		this.id = id;
		this.name = name;
		this.goals = g;
		this.rewardList = r;
		this.timerSecond = timer;

		this.canBeRepeated = canBeReapeted;
		this.canBeRepeatedIfWin = canBeReapetedIfWin;
		this.timerInSecondBeforeRepeat = timebeforeinsec;

		this.startPos = startPos2;

		if (!this.canBeRepeated)
		{ // ca coule de sens jacqui !
			this.canBeRepeatedIfWin = false;
		}
	}

	public static Quest getQuestByName(String s)
	{
		for (Quest q : questList)
		{
			if (q.getName().equals(s))
			{
				return q;
			}
		}
		return null;
	}
	
	
	

	public void setFailRewards(ArrayList<Reward> failRewards) {
		this.failRewards = failRewards;
	}

	public ArrayList<Reward> getFailRewards() {
		if(this.failRewards==null)
		{
			this.failRewards=new ArrayList<Reward>();
			this.save();
		}
		return failRewards;
	}

	public void forceSortGoals()
	{
		Collections.sort(this.goals, new CustomGoalComparator());
		this.save();
	}

	public ArrayList<Goal> getGoals()
	{
		return goals;
	}

	public int getId()
	{
		return id;
	}

	public int setIdCAUTIONDONTUSE(int i)
	{
		return this.id = i;
	}

	public String getName()
	{
		return name;
	}

	public static Quest createQuest(String name, ArrayList<Goal> g, ArrayList<Reward> r)
	{
		return createQuest(name, g, r, -1, true, true, -1, null);
	}

	public static Quest createQuest(String name, ArrayList<Goal> g, ArrayList<Reward> r, int timer, boolean canBeReapeted, boolean canBeReapetedIfWin, long timebeforerepinsec, Location startPos)
	{
		int id = Tools.getAleatInt(0, Integer.MAX_VALUE-1);
		System.out.println("Try to create quest with id: " + id);
		while (getQuestById(id) != null)
		{
			id = Tools.getAleatInt(0, Integer.MAX_VALUE-1);
			System.out.println("Fail creat quest, new id: " + id);
		}
		Quest q = new Quest(id, name, g, r, timer, canBeReapeted, canBeReapetedIfWin, timebeforerepinsec, startPos);
		q.forceSortGoals();
		questList.add(q);
		
		
		for(Goal gg:q.getGoals())
		{
			gg.setQuestId(q.getId());
		}
		
		
		q.save();
		return q;
	}

	public static Quest getQuestById(int id)
	{
		for (Quest q : questList)
		{
			if (q.getId() == id)
			{
				return q;
			}
		}
		return null;
	}

	public static ArrayList<Quest> getQuestList()
	{
		return questList;
	}

	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static Quest fromJson(String s)
	{
		GoalDeserializer deserializerVocation = new GoalDeserializer("type");

		for (GoalType gt : GoalType.values())
		{
			deserializerVocation.registerRaceType(gt.cla.getSimpleName(), gt.cla);
		}

		RewardDeserializer rewardDeserializer = new RewardDeserializer("type");
		for (RewardType rt : RewardType.values())
		{
			rewardDeserializer.registerRaceType(rt.cla.getSimpleName(), rt.cla);
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Goal.class, deserializerVocation).registerTypeAdapter(Reward.class, rewardDeserializer).create();

		return gson.fromJson(s, Quest.class);
	}

	public static void rattachObject()
	{
		int i = 0;
		for (Quest q : Quest.getQuestList())
		{
			for (Goal g : q.getGoals())
			{
				if (g instanceof InteractEntity)
				{
					InteractEntity tnp = (InteractEntity) g;
					Entity e = Tools.getEntityByUUID(UUID.fromString(tnp.getUuid()));
					if (e != null && tnp.getEntity() == null)
					{
						tnp.setEntity(e);
						i++;
					}
					else
					{
						// System.out.println("ERROR can't load entity for
						// quest: "+q.getName()+" for goal:
						// "+tnp.getInternalId()+" for entity: "+tnp.getUuid());
					}
				}
				else if (g instanceof FightEntity)
				{
					FightEntity tnp = (FightEntity) g;
					Entity e = Tools.getEntityByUUID(UUID.fromString(tnp.getUuid()));
					if (e != null && tnp.getEntity() == null)
					{
						tnp.setEntity(e);
						i++;
					}
					else
					{
						// System.out.println("ERROR can't load entity for
						// quest: "+q.getName()+" for goal:
						// "+tnp.getInternalId()+" for entity: "+tnp.getUuid());
					}
				}
			}
		}

		// System.out.println("Pixelquest: "+i+" npc loaded for talk goals !");
	}

	public static void loadAllQuest()
	{
		System.out.println("Loadgin quest ...");
		questList.clear();
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/quest/");
		folder.mkdirs();
		for (File f : folder.listFiles())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				Quest q = Quest.fromJson(everything);

				boolean modified = false;
				for (Goal g : q.getGoals())
				{
					if (g.getQuestId() == 0)
					{
						System.out.println("Error on quest: " + q.getName() + " old format. Missing quest ids on goals, reformated !");
						g.setQuestId(q.getId());
						modified=true;
					}
					else if(g.getQuestId()!=q.getId())
					{
						System.out.println("Erron on goal ids for quest: "+q.getName()+" new id for this goal: "+g.getInternalId());
						g.setQuestId(q.getId());
						modified=true;
					}
				}
				
				for(Reward r:q.getRewardList())
				{
					if(r.getChanceToGet()==0)
					{
						System.out.println("Translate old reward too 100%");
						modified=true;
						r.setChanceToGet(100);
					}
				}

				if(modified)
				{
					q.save();
					System.out.println("Quest resaved !");
				}
				
				System.out.println("Loaded: " + q.getName());
				questList.add(q);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		System.out.println(questList.size() + " quests loaded !");

	}

	public void save()
	{
		BufferedWriter writer = null;
		try
		{

			File file = new File(Pixelquest.folder.getAbsolutePath() + "/quest/" + this.getName() + ".json");
			if (!file.exists())
			{
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public void display(EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		boolean hasquest = profile.getQuestProfile().getFollowedQuest().contains(this);

		ArrayList<String> lines = new ArrayList<String>();

		for (String s : this.getIntro())
		{
			lines.add(s);
		}

		String sup = "";
		if (hasquest && this.getTimerSecond() > -1)
		{
			float percent = (float) profile.getQuestProfile().getTakedTimeInSec(this) / (float) this.getTimerSecond();
			percent = percent * 100.0f;
			percent = Math.round(percent);
			sup += TextFormatting.WHITE + " Your timer state: " + TextFormatting.LIGHT_PURPLE + percent + TextFormatting.WHITE + " %";
		}

		lines.add("Id: " + TextFormatting.GRAY + this.getId() + TextFormatting.WHITE + " Goals count: " + TextFormatting.GREEN + this.getGoals().size() + TextFormatting.WHITE + " Active: " + TextFormatting.GREEN + hasquest + TextFormatting.WHITE + " Timer in seconds: " + TextFormatting.BLUE + this.getTimerSecond() + sup);

		for (Goal g : this.getGoals())
		{
			String phrase = "";
			if (g instanceof GoalRepeatable)
			{
				int count = ((GoalRepeatable) g).getCount();

				if (g instanceof KillPokemon)
				{
					KillPokemon kp = (KillPokemon) g;
					phrase = "You need to kill " + TextFormatting.RED + count + TextFormatting.WHITE;

					if (g instanceof CapturePokemon)
					{
						phrase = "You need to capture " + TextFormatting.RED + count + TextFormatting.WHITE;
					}
					else if(g instanceof FightPokemon)
					{
						phrase = "You need to fight " + TextFormatting.RED + count + TextFormatting.WHITE;
					}

					if (kp.getPokemons().size() <= 0)
					{
						phrase += " pokemon(s)";
					}
					else
					{
						phrase += TextFormatting.WHITE + " one of these pokemon ";
						phrase += TextFormatting.GREEN;
						for (EnumPokemon poke : kp.getPokemons())
						{
							phrase += poke.name + ", ";
						}
					}

					if (kp.getTypes().size() <= 0)
					{
						// phrase+=" with no particular type.";
					}
					else
					{
						phrase += TextFormatting.WHITE + " with one of these types: ";
						phrase += TextFormatting.BLUE;
						for (EnumType poke : kp.getTypes())
						{
							phrase += poke.getName() + ", ";
						}
					}

					if (kp.getMinLvL() != -1)
					{
						phrase += TextFormatting.WHITE + "and min lvl " + TextFormatting.RED + kp.getMinLvL();
					}

					if (kp.getMaxLvL() != -1)
					{
						phrase += TextFormatting.WHITE + "and max lvl " + TextFormatting.RED + kp.getMaxLvL();
					}

				}

			}
			else if (g instanceof InteractEntity)
			{
				InteractEntity in = (InteractEntity) g;
				phrase += "Interact with entity ";
			}
			else if (g instanceof LocationGoal)
			{
				LocationGoal in = (LocationGoal) g;
				phrase += "Go to location: " + in.getLoc().getX() + " : " + in.getLoc().getY() + " : " + in.getLoc().getZ() + "";
			}
			else
			{
				phrase += "To dev plz !";
			}

			phrase += TextFormatting.WHITE + "Priority: " + TextFormatting.BLUE + g.getPriority();

			if (hasquest)
			{
				phrase += TextFormatting.GRAY + " Your state: ";
				int c = profile.getQuestProfile().getGoalAvancementForQuest(this.getId(), g.getInternalId()).getCount();
				phrase += TextFormatting.RED + "" + c;
			}

			if (phrase == null || phrase.equals(""))
			{
				phrase = "ERROR CALL OPERATOR !";
			}
			lines.add(phrase);
		}

		Tools.openGuiMessage(p, TextFormatting.RED + "Quest " + this.getName() + " info", lines);
	}

	public int getTimerSecond()
	{
		return timerSecond;
	}

	public boolean isCanBeRepeated()
	{
		return canBeRepeated;
	}

	public boolean isCanBeRepeatedIfWin()
	{
		return canBeRepeatedIfWin;
	}

	public long getTimerInSecondBeforeRepeat()
	{
		return timerInSecondBeforeRepeat;
	}

	public int getMaxPriority()
	{
		int max = -1;
		for (Goal g : this.getGoals())
		{
			if (g.getPriority() > max)
			{
				max = g.getPriority();
			}
		}
		return max;
	}

	public ArrayList<Reward> getRewardList()
	{
		return rewardList;
	}

	public void setRewardList(ArrayList<Reward> rf)
	{
		rewardList = rf;
	}

	public Location getStartPos()
	{
		return startPos;
	}

	public void setStartPos(Location startPos)
	{
		this.startPos = startPos;
	}

	public ArrayList<Integer> getPreRequis()
	{
		return preRequis;
	}

	public void setPreRequis(ArrayList<Integer> preRequis)
	{
		this.preRequis = preRequis;
	}

	public ArrayList<String> getIntro()
	{
		if (intro == null)
		{
			intro = new ArrayList<String>();
		}
		return intro;
	}

	public ArrayList<String> getOutro()
	{
		if (outro == null)
		{
			outro = new ArrayList<String>();
		}
		return outro;
	}

	public void setIntro(ArrayList<String> intro)
	{
		this.intro = intro;
	}

	public void setOutro(ArrayList<String> outro)
	{
		this.outro = outro;
	}

	public void loadInCreator(QuestCreator c)
	{
		c.canBeRepeated = this.canBeRepeated;
		c.canBeRepeatedIfWin = this.canBeRepeatedIfWin;
		c.goals = (ArrayList<Goal>) this.goals.clone();
		c.name = this.name;
		c.prerequis = this.getPreRequis();
		c.rewards = this.rewardList;
		c.startPos = this.startPos;
		c.timer = this.timerSecond;
		c.timerInSecondBeforeRepeat = this.timerInSecondBeforeRepeat;
		c.intro = this.getIntro();
		c.outro = this.getOutro();
		c.cinematicname=this.getIntroCinematicName();
		c.canRemove=this.canRemove;
	}

	public boolean isCanRemove()
	{
		return canRemove;
	}

	public void setCanRemove(boolean canRemove)
	{
		this.canRemove = canRemove;
	}

	public boolean isHasSharedTimerBeforeRepeat()
	{
		return hasSharedTimerBeforeRepeat;
	}

	public void setHasSharedTimerBeforeRepeat(boolean hasSharedTimerBeforeRepeat)
	{
		this.hasSharedTimerBeforeRepeat = hasSharedTimerBeforeRepeat;
	}

	public String getIntroCinematicName()
	{
		return introCinematicName;
	}

	public void setIntroCinematicName(String introCinematicName)
	{
		this.introCinematicName = introCinematicName;
	}
	
	
	

}
