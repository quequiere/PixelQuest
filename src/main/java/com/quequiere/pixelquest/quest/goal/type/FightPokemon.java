package com.quequiere.pixelquest.quest.goal.type;

import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import net.minecraft.entity.player.EntityPlayerMP;

public class FightPokemon extends KillPokemon
{

	public FightPokemon(int count)
	{
		super(count);
	}

	
	public String getDisplayName(EntityPlayerMP p)
	{
		String names = "";
		for(EnumPokemon en:this.getPokemons())
		{
			names+=en.name()+" ";
		}
		
		if(this.getPokemons().size()>0)
		{
			return "Fight "+names+":";
		}
		
		return "Fight pokemon:";
	}

	
}
