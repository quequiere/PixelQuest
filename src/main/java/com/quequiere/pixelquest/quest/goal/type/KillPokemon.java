package com.quequiere.pixelquest.quest.goal.type;

import java.util.ArrayList;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.pixelmonmod.pixelmon.enums.EnumType;
import net.minecraft.entity.player.EntityPlayerMP;

public class KillPokemon extends GoalRepeatable
{

	private ArrayList<EnumPokemon> pokemons = new ArrayList<EnumPokemon>();
	private ArrayList<EnumType> types = new ArrayList<EnumType>();
	private int minLvL = -1;
	private int maxLvL = -1;

	public KillPokemon(int count)
	{
		super(count);
	}

	public boolean success(EntityPixelmon p)
	{
		boolean successMinLvl = true;
		boolean successMaxLvl = true;
		boolean successType = true;
		boolean successPokemon = true;

		if (types.size() > 0)
		{
			successType = false;
			for (EnumType pname : types)
			{
				for (EnumType ptype : p.type)
				{
					if (pname.equals(ptype))
					{
						successType = true;
						break;
					}
				}

				if (successType)
				{
					break;
				}
			}
		}

		if (minLvL != -1)
		{
			if (!(p.getLvl().getLevel() >= minLvL))
			{
				successMinLvl = false;
			}
		}

		if (maxLvL != -1)
		{
			if (p.getLvl().getLevel() > maxLvL)
			{
				successMaxLvl = false;
			}

		}

		if (pokemons.size() > 0)
		{
			successPokemon = false;

			for (EnumPokemon pname : pokemons)
			{
				if (pname.equals(EnumPokemon.valueOf(p.getPokemonName())))
				{
					successPokemon = true;
					break;
				}
			}
		}

		if (successMinLvl && successMaxLvl && successType && successPokemon)
		{
			return true;
		}

		return false;
	}

	public ArrayList<EnumPokemon> getPokemons()
	{
		return pokemons;
	}

	public void setPokemons(ArrayList<EnumPokemon> pokemons)
	{
		this.pokemons = pokemons;
	}

	public ArrayList<EnumType> getTypes()
	{
		return types;
	}

	public void setTypes(ArrayList<EnumType> types)
	{
		this.types = types;
	}

	public int getMinLvL()
	{
		return minLvL;
	}

	public void setMinLvL(int minLvL)
	{
		this.minLvL = minLvL;
	}

	public int getMaxLvL()
	{
		return maxLvL;
	}

	public void setMaxLvL(int maxLvL)
	{
		this.maxLvL = maxLvL;
	}

	public String getDisplayName(EntityPlayerMP p)
	{
		String names = "";
		for (EnumPokemon en : this.pokemons)
		{
			names += en.name() + " ";
		}

		if (this.pokemons.size() > 0)
		{
			return "Kill " + names + ":";
		}

		return "Kill pokemon:";
	}

}
