package com.quequiere.pixelquest.quest.goal;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.handler.DungeonQueueCheckerRunnable;
import com.quequiere.pixelquest.event.ServerTickEvent;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.quest.GoalAvancement;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.goal.type.LocationGoal;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;

import net.minecraft.entity.player.EntityPlayerMP;

public class GoalLocationHandler implements Runnable
{
	public static GoalLocationHandler notifHandler;
	private long last = 0;
	private static long timer= 1*1000;

	@Override
	public void run()
	{
		
		long now = System.currentTimeMillis();
		long diff = now-last;

		if(diff<timer)
		{
			return;
		}
		
		last = now;
		
		

		try
		{

			for (EntityPlayerMP p : Tools.getAllPlayers())
			{
				PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
				profile.getQuestProfile().checkTimerQuest(p);

				ArrayList<Goal> toUpdate = new ArrayList<Goal>();

				try
				{
					for (Quest q : profile.getQuestProfile().getFollowedQuest())
					{
						
						if(q.getTimerSecond()>0)
						{
							profile.getQuestProfile().forceupdateScoreBoard(p);
						}
						
						for (Goal g : q.getGoals())
						{
							
							if (g instanceof FightEntity)
							{
								GoalAvancement avan = profile.getQuestProfile().getGoalAvancementForQuest(q.getId(), g.getInternalId());
								if (avan == null)
								{
									continue;
								}

								if (!avan.isFinished())
								{
									profile.getQuestProfile().forceupdateScoreBoard(p);
								}
							}
							if (g instanceof LocationGoal)
							{
								try
								{
									GoalAvancement avan = profile.getQuestProfile().getGoalAvancementForQuest(q.getId(), g.getInternalId());

									if (avan == null)
									{
										continue;
									}

									if (!avan.isFinished() && avan.canUpdateWithPriority(q, g, p))
									{
										LocationGoal lg = (LocationGoal) g;
										Location ploc = new Location(p);

										if (lg.getLoc().getWorld().equals(ploc.getWorld()))
										{
											if (ploc.getDistance(lg.getLoc().getX(), lg.getLoc().getY(), lg.getLoc().getZ()) < 2)
											{
												toUpdate.add(g);
											}
											else
											{
												profile.getQuestProfile().forceupdateScoreBoard(p);
											}
										}

									}

								}
								catch (NullPointerException e)
								{
									e.printStackTrace();
								}
							}

							if (g instanceof InteractEntity)
							{
								GoalAvancement avan = profile.getQuestProfile().getGoalAvancementForQuest(q.getId(), g.getInternalId());
								if (avan == null)
								{
									continue;
								}

								if (!avan.isFinished())
								{
									profile.getQuestProfile().forceupdateScoreBoard(p);
								}
							}
						}
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}

				
		

				for (Goal goal : toUpdate)
				{
					GoalAvancement avancement = profile.getQuestProfile().getGoalAvancementForQuest(goal.getQuestId(), goal.getInternalId());
					avancement.update(goal, p);
				}

				profile.save();
			}

			Quest.rattachObject();

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public static void startHandler()
	{
		notifHandler = new GoalLocationHandler();
		ServerTickEvent.tasks.add(notifHandler);
	}

}
