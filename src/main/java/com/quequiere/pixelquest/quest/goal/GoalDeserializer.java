package com.quequiere.pixelquest.quest.goal;

import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class GoalDeserializer implements JsonDeserializer<Goal>
{
	
	private final String auctionTypeElementName;
	private final HashMap<String, Class<? extends Goal>> raceTypeRegistry;
	private final Gson gson;

	public GoalDeserializer(String AuctionTypeElementName)
	{
		this.auctionTypeElementName = AuctionTypeElementName;
		gson = new Gson();
		raceTypeRegistry = new HashMap<String, Class<? extends Goal>>();
	}
	
	public void registerRaceType(String auctionTypeName, Class<? extends Goal> auctionType)
	{
		raceTypeRegistry.put(auctionTypeName, auctionType);
	}
	
	@Override
	public Goal deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		JsonObject object = json.getAsJsonObject();
		JsonElement animalTypeElement = object.get(auctionTypeElementName);
		String type = animalTypeElement.getAsString(); 
		Class<? extends Goal> raceType = raceTypeRegistry.get(type);
		Goal auction = gson.fromJson(object, raceType);
		
		
		return auction;
	}

}
