package com.quequiere.pixelquest.quest.goal;

import java.util.Comparator;

public class CustomGoalComparator implements Comparator<Goal>
{
	@Override
	public int compare(Goal o1, Goal o2)
	{
		if(o1.getPriority()>o2.getPriority())
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
}