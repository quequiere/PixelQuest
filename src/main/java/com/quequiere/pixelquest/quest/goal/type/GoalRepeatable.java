package com.quequiere.pixelquest.quest.goal.type;

import com.quequiere.pixelquest.quest.goal.Goal;

public abstract class GoalRepeatable extends Goal{
	
	private int count;
	
	public GoalRepeatable(int count)
	{
		this.count=count;
	}

	public int getCount() {
		return count;
	}
	
	

}
