package com.quequiere.pixelquest.quest.goal.type;

import com.quequiere.pixelquest.quest.goal.Goal;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;

public class InteractEntity extends Goal
{
	private transient Entity entity;
	private String uuid;

	public InteractEntity(String uuid)
	{
		this.uuid = uuid;
	}

	public Entity getEntity()
	{
		return entity;
	}

	public void setEntity(Entity e)
	{
		this.entity = e;
	}

	public String getUuid()
	{
		return uuid;
	}

	public String getDisplayName(EntityPlayerMP p)
	{
		return "Interact Entity:";
	}

}
