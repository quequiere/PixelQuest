package com.quequiere.pixelquest.quest.goal.type;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.quest.goal.Goal;

import net.minecraft.entity.player.EntityPlayerMP;

public class LocationGoal extends Goal
{
	private Location loc;
	
	public LocationGoal(Location loc)
	{
		this.loc=loc;
	}

	public Location getLoc()
	{
		return loc;
	}
	
	
	public String getDisplayName(EntityPlayerMP p)
	{
		return "Distance:";
	}
	
}
