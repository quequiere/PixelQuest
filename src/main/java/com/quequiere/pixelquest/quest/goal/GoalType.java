package com.quequiere.pixelquest.quest.goal;

import com.quequiere.pixelquest.quest.goal.type.CapturePokemon;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.FightPokemon;
import com.quequiere.pixelquest.quest.goal.type.KillPokemon;
import com.quequiere.pixelquest.quest.goal.type.LocationGoal;
import com.quequiere.pixelquest.quest.goal.type.PickupItemGoal;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;

public enum GoalType
{
	KillPokemon(KillPokemon.class),
	CapturePokemon(CapturePokemon.class),
	Location(LocationGoal.class),
	Entity(InteractEntity.class),
	PickupItem(PickupItemGoal.class),
	Fightentity(FightEntity.class),
	Fightpokemon(FightPokemon.class);
	
	public Class cla;
	
	GoalType(Class<? extends Goal> c)
	{
		this.cla=c;
	}
}
