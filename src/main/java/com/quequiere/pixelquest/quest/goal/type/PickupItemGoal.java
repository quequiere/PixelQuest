package com.quequiere.pixelquest.quest.goal.type;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class PickupItemGoal extends GoalRepeatable
{

	private int itemId;
	private int metaData=0;

	public PickupItemGoal(int count,ItemStack is)
	{
		super(count);
		this.itemId=Item.getIdFromItem(is.getItem());
		this.metaData=is.getMetadata();
	}

	public int getItemId()
	{
		return itemId;
	}

	public Item getItem()
	{
		return Item.getItemById(this.getItemId());
	}
	
	public int getMetaData()
	{
		return metaData;
	}

	public boolean isCompatible(ItemStack is)
	{
		if(this.getItem().equals(is.getItem()) && this.getMetaData()==is.getMetadata())
		{
			return true;
		}
		
		return false;
	}


	public String getDisplayName(EntityPlayerMP p)
	{
		String name = new ItemStack(this.getItem(), 1,this.getMetaData()).getDisplayName();
		return "Pickup "+name;
	}

}
