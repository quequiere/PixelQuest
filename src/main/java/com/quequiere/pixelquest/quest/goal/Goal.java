package com.quequiere.pixelquest.quest.goal;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.quest.Quest;

import net.minecraft.entity.player.EntityPlayerMP;

public abstract class Goal {
	
	private int timer;
	private int internalId;
	private String type;
	private int priority = -1;
	private int questId = 0;
	
	public Goal()
	{
		this.internalId=Tools.getAleatInt(0, Integer.MAX_VALUE-1);
		this.type=this.getClass().getSimpleName();
	}

	public int getInternalId() {
		return internalId;
	}
	
	public String getDisplayName(EntityPlayerMP p)
	{
		return "A girl has no name";
	}
	
	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public int getPriority()
	{
		return priority;
	}

	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	public int getQuestId()
	{
		return questId;
	}

	public void setQuestId(int questId)
	{
		this.questId = questId;
	}
	
	public Quest getQuest(Goal g)
	{
		Quest q = Quest.getQuestById(this.getQuestId());
		if(q==null)
		{
			System.out.println("ERROR on quest id missing, goal: "+this.getInternalId()+" with quest id==> "+this.getQuestId());
			return null;
		}
		
		return q;
	}
	
}
