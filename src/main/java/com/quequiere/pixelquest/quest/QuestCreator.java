package com.quequiere.pixelquest.quest;

import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.dungeon.objects.RangedInt;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.type.PercentedReward;

public class QuestCreator
{
	public String name;
	public ArrayList<Integer> prerequis= new ArrayList<Integer>();
	public ArrayList<Goal> goals= new ArrayList<Goal>();
	public ArrayList<Reward> rewards= new ArrayList<Reward>();
	public int timer=-1;
	
	public boolean canBeRepeated = true;
	public boolean canBeRepeatedIfWin = true;
	public long timerInSecondBeforeRepeat = -1;
	public Location startPos;
	
	public boolean isModify =false;
	public int questId = -1;
	
	public ArrayList<String> intro = new ArrayList<String>();
	public ArrayList<String> outro = new ArrayList<String>();
	
	public QuestTimerBeforeRepeat questTimerBeforeRepeat = null;
	
	public boolean canRemove = true;
	public String cinematicname = "";
	
	public boolean nextIsPercentedReward = false;
	public HashMap<RangedInt, Reward> percentedReward= new HashMap<RangedInt, Reward>();

	public ArrayList<Reward> failReward = new ArrayList<Reward>();
	
}
