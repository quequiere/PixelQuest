package com.quequiere.pixelquest.quest;

import java.util.ArrayList;

public class QuestTimerBeforeRepeat
{


	private long timerInSecondBeforeRepeat = -1;
	private ArrayList<Integer> questList = new ArrayList<Integer>();

	public QuestTimerBeforeRepeat(long timerInSecondBeforeRepeat)
	{
		this.timerInSecondBeforeRepeat = timerInSecondBeforeRepeat;
	}

	public long getTimerInSecondBeforeRepeat()
	{
		return timerInSecondBeforeRepeat;
	}

	public ArrayList<Integer> getQuestList()
	{
		return questList;
	}

	public static QuestTimerBeforeRepeat getQuestTimer(int questid)
	{
		for (QuestTimerBeforeRepeat t : QuestTimerBeforeRepeatConfig.instance.list)
		{
			if (t.getQuestList().contains(questid))
			{
				return t;
			}
		}
		return null;
	}


	


}
