package com.quequiere.pixelquest.quest.reward;

import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.quequiere.pixelquest.quest.goal.Goal;

public class RewardDeserializer implements JsonDeserializer<Reward>
{
	
	private final String auctionTypeElementName;
	private final HashMap<String, Class<? extends Reward>> raceTypeRegistry;
	private final Gson gson;

	public RewardDeserializer(String AuctionTypeElementName)
	{
		this.auctionTypeElementName = AuctionTypeElementName;
		
		
	

		gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Reward.class, this).create();

		
		
		
		raceTypeRegistry = new HashMap<String, Class<? extends Reward>>();
	}
	
	public void registerRaceType(String auctionTypeName, Class<? extends Reward> auctionType)
	{
		raceTypeRegistry.put(auctionTypeName, auctionType);
	}
	
	@Override
	public Reward deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		JsonObject object = json.getAsJsonObject();
		JsonElement animalTypeElement = object.get(auctionTypeElementName);
		String type = animalTypeElement.getAsString(); 
		
		
		
		
		
		
		Class<? extends Reward> raceType = raceTypeRegistry.get(type);
		
	
		
		
		Reward auction = gson.fromJson(object, raceType);
		
		
		return auction;
	}

}
