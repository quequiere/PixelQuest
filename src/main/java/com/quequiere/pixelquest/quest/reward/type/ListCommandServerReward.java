package com.quequiere.pixelquest.quest.reward.type;

import java.util.ArrayList;

import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class ListCommandServerReward extends Reward
{
	private ArrayList<String> command;

	public ListCommandServerReward(ArrayList<String> s)
	{
		super();
		this.command = s;
	}

	@Override
	public void give(EntityPlayerMP p)
	{
		for(String sc:command)
		{
			String finalCommand = replaceVar(sc, p);
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Execute server command " + finalCommand));

			MinecraftServer e = FMLCommonHandler.instance().getMinecraftServerInstance();

			FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(e, finalCommand);
		}


	}
	
	

	public ArrayList<String> getCommand()
	{
		return command;
	}

	public String replaceVar(String s, EntityPlayerMP p)
	{
		s = s.replace("%p", p.getName());
		return s;
	}

}
