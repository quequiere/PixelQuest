package com.quequiere.pixelquest.quest.reward;

import com.quequiere.pixelquest.dungeon.objects.RangedInt;

public class PercentedRewardObject
{
	public RangedInt range;
	public Reward reward;
	public PercentedRewardObject(RangedInt range, Reward reward)
	{
		super();
		this.range = range;
		this.reward = reward;
	}

	
}
