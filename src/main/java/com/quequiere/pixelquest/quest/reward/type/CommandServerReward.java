package com.quequiere.pixelquest.quest.reward.type;

import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class CommandServerReward extends Reward
{
	private String command;

	public CommandServerReward(String s)
	{
		super();
		this.command = s;
	}

	@Override
	public void give(EntityPlayerMP p)
	{
		String finalCommand = replaceVar(this.command, p);
		
		if(!finalCommand.contains("iz "))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Execute server command " + finalCommand));
		}
	

		MinecraftServer e = FMLCommonHandler.instance().getMinecraftServerInstance();
		

		FMLCommonHandler.instance().getMinecraftServerInstance().getCommandManager().executeCommand(e, finalCommand);

	}
	
	

	public String getCommand()
	{
		return command;
	}

	public String replaceVar(String s, EntityPlayerMP p)
	{
		s = s.replace("%p", p.getName());
		return s;
	}

}
