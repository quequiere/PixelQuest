package com.quequiere.pixelquest.quest.reward.type;

import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.objects.RangedInt;
import com.quequiere.pixelquest.quest.reward.PercentedRewardObject;
import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;

public class PercentedReward extends Reward
{
	
	private ArrayList<PercentedRewardObject> liste = new ArrayList<PercentedRewardObject>();

	public PercentedReward(ArrayList<PercentedRewardObject> l)
	{
		super();
		liste = l ;
		
		
	}
	
	

	public ArrayList<PercentedRewardObject> getListe()
	{
		return liste;
	}



	@Override
	public void give(EntityPlayerMP p)
	{
		int rand = Tools.getAleatInt(0, 100);
		
		for(PercentedRewardObject ri:liste)
		{
			if(ri.range.isInside(rand))
			{
				Reward r = ri.reward;
				r.give(p);
				break;
			}
		}
		
		
	}
	
	


}
