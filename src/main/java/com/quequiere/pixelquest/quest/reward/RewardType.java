package com.quequiere.pixelquest.quest.reward;

import com.quequiere.pixelquest.quest.reward.type.CommandServerReward;
import com.quequiere.pixelquest.quest.reward.type.ExpQuestReward;
import com.quequiere.pixelquest.quest.reward.type.ListCommandServerReward;
import com.quequiere.pixelquest.quest.reward.type.PercentedReward;
import com.quequiere.pixelquest.quest.reward.type.StarterReward;
import com.quequiere.pixelquest.quest.reward.type.TeleportReward;

public enum RewardType
{
	TeleportReward(TeleportReward.class),
	CommandReward(CommandServerReward.class),
	expQuestReward(ExpQuestReward.class),
	starterReward(StarterReward.class),
	listCommandReward(ListCommandServerReward.class),
	percentedReward(PercentedReward.class);
	
	
	public Class cla;
	
	RewardType(Class<? extends Reward> c)
	{
		this.cla=c;
	}
}
