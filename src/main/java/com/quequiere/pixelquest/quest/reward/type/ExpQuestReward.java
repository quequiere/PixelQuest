package com.quequiere.pixelquest.quest.reward.type;

import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;

public class ExpQuestReward extends Reward
{

	private int exp;
	
	
	public ExpQuestReward(int exp)
	{
		this.exp = exp;
	}

	@Override
	public void give(EntityPlayerMP p)
	{
		PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
		if(pp!=null)
		{
			pp.getExpProfile().addExpQuest(exp, p, pp);
		}
	}


}
