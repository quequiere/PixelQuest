package com.quequiere.pixelquest.quest.reward.type;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class TeleportReward extends Reward
{
	private Location pos;
	
	public TeleportReward(Location pos)
	{
		super();
		this.pos=pos;
	}

	@Override
	public void give(EntityPlayerMP p)
	{
		p.addChatMessage(new TextComponentString(TextFormatting.GREEN+"Execute teleport reward ..."));
		if(pos.getWorld().provider.getDimension()!=p.dimension)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN+"Teleport to an other dimension ..."));
			p.changeDimension(pos.getWorld().provider.getDimension());
		}
		p.connection.setPlayerLocation(pos.getX(),pos.getY(),pos.getZ(), p.rotationYaw,  p.rotationPitch);
	}

}
