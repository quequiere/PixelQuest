package com.quequiere.pixelquest.quest.reward.type;

import java.util.Optional;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.TickHandler;
import com.pixelmonmod.pixelmon.api.enums.ReceiveType;
import com.pixelmonmod.pixelmon.api.events.PixelmonReceivedEvent;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.customStarters.CustomStarters;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;

public class StarterReward extends Reward
{

	public StarterReward()
	{
		
	}

	@Override
	public void give(EntityPlayerMP p)
	{
		
	}
	
	public static void giveStarter(EntityPlayerMP player,String pokemonName)
	{

		Optional optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
		if (optstorage.isPresent())
		{
			PlayerStorage s = (PlayerStorage) optstorage.get();
			if (s.starterPicked)
			{
				return;
			}

			EntityPixelmon p = (EntityPixelmon) PixelmonEntityList.createEntityByName(pokemonName, player.getEntityWorld());
			p.getLvl().setLevel(CustomStarters.starterLevel);
			if (CustomStarters.shinyStarter)
			{
				p.setIsShiny(CustomStarters.shinyStarter);
			}

			/*
			if (p.hasForms())
			{
				p.setForm(StarterList.starterForms[message.starterIndex]);
			}*/

			p.func_70606_j((float) p.stats.HP);
			p.loadMoveset();
			//p.caughtBall = p.caughtBall.PokeBall;
			p.friendship.initFromCapture();
			s.addToParty(p);
			s.starterPicked = true;
			TickHandler.deregisterStarterList(player);
			Pixelmon.EVENT_BUS.post(new PixelmonReceivedEvent(player, ReceiveType.Starter, p));
		}

		return;
	
	}


}
