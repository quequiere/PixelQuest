package com.quequiere.pixelquest.quest.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Tools;

import net.minecraft.entity.player.EntityPlayerMP;

public abstract class Reward
{
	private String type;
	private int chanceToGet=100;
	
	public Reward()
	{
		this.type=this.getClass().getSimpleName();
	}
	
	public boolean tryToGetWithChance()
	{
		if(chanceToGet>=100)
		{
			return true;
		}
		else if(chanceToGet<=0)
		{
			return false;
		}
		else
		{
			int rand = Tools.getAleatInt(1, 100);
			
			// si je tire 25 et que chance est de 30
			// 25 <= 30 ==> ca passe
			
			if(rand<=chanceToGet)
			{
				return true;
			}
			else
			{
				return false;
			}
			
		}
	}
	
	
	
	public int getChanceToGet() {
		return chanceToGet;
	}

	public void setChanceToGet(int chanceToGet)
	{
		this.chanceToGet = chanceToGet;
	}

	public abstract void give(EntityPlayerMP p);
	
	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
}
