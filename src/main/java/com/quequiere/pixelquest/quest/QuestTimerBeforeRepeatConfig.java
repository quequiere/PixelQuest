package com.quequiere.pixelquest.quest;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Pixelquest;

public class QuestTimerBeforeRepeatConfig
{
	public static QuestTimerBeforeRepeatConfig instance = new QuestTimerBeforeRepeatConfig();

	public ArrayList<QuestTimerBeforeRepeat> list = new ArrayList<QuestTimerBeforeRepeat>();

	public void addToList(QuestTimerBeforeRepeat t)
	{
		list.add(t);
		this.save();
	}
	
	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
	
	public static void load()
	{
		instance = QuestTimerBeforeRepeatConfig.loadFile();
		if(instance==null)
		{
			instance = new QuestTimerBeforeRepeatConfig();
			instance.save();
		}
	}

	public static QuestTimerBeforeRepeatConfig fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, QuestTimerBeforeRepeatConfig.class);
	}

	private static QuestTimerBeforeRepeatConfig loadFile()
	{
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/");
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/QuestTimerBeforeRepeatConfig.json");

		if (f.exists())
		{
			System.out.println("Loading quest timer before repeat from files !");
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();

				return QuestTimerBeforeRepeatConfig.fromJson(everything);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			System.out.println("Cant find: " + f.getAbsolutePath());
			try
			{
				f.createNewFile();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	public void save()
	{
		BufferedWriter writer = null;
		try
		{

			File folder = new File(Pixelquest.folder.getAbsolutePath() + "/");
			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/QuestTimerBeforeRepeatConfig.json");
			if (!file.exists())
			{
				file.mkdirs();
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}
}
