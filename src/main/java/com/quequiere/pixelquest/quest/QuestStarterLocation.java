package com.quequiere.pixelquest.quest;

import java.util.Timer;
import java.util.TimerTask;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.player.PlayerProfile;

import net.minecraft.entity.player.EntityPlayerMP;

public class QuestStarterLocation extends TimerTask
{
	public static QuestStarterLocation questHandler;
	private static Timer timer = new Timer();


	@Override
	public void run()
	{
		
		for(Quest q:Quest.getQuestList())
		{
			Location location = q.getStartPos();
			if(location!=null)
			{
				for(EntityPlayerMP p:Tools.getAllPlayers())
				{
					PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(),false);
					
					if(!profile.getQuestProfile().hasQuest(q.getId()))
					{
						Location playerloc =  Location.fromBlockPos(p.getPosition(), p.worldObj);
						double distance = location.getDistance(playerloc);
						if(distance<=3)
						{
							profile.getQuestProfile().addQuest(q, p,false);
						}
					}
				}
			}
		}
	}

	public static void startHandler()
	{
		
		questHandler = new QuestStarterLocation();
		timer.schedule(questHandler, 1 * 1000, 1 * 1000);
	}

}
