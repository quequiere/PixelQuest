package com.quequiere.pixelquest.map;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.vecmath.Vector2d;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraft.world.storage.MapData;

public class MapTool
{
	// "https://i.ytimg.com/vi/YmqgTNhZqng/hqdefault.jpg"

	public static Optional<MapData> getMapData(int id, World w)
	{
		String s = "map_" + id;
		MapData mapdata = (MapData) w.loadItemData(MapData.class, s);

		return Optional.of(mapdata);
	}

	public static void givePlayer(String url, EntityPlayerMP p) throws MalformedURLException, IOException
	{
		BufferedImage bufferedImage = ImageIO.read(new URL(url));

		int imageX = bufferedImage.getWidth();
		int imageY = bufferedImage.getHeight();

		while (imageX % 128 != 0)
		{
			imageX++;
		}

		while (imageY % 128 != 0)
		{
			imageY++;
		}

		int xPanes = imageX / 128;
		int yPanes = imageY / 128;


		Image img = bufferedImage.getScaledInstance(xPanes * 128, yPanes * 128, Image.SCALE_SMOOTH);

		if (img instanceof BufferedImage)
		{
			bufferedImage = (BufferedImage) img;
		}

		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		bufferedImage = bimage;

		for (int x = 0; x < xPanes; x++)
		{
			for (int y = 0; y < yPanes; y++)
			{
				try
				{
					BufferedImage paneImage = bufferedImage.getSubimage(x * 128, y * 128, 128, 128);
					@SuppressWarnings("deprecation")
					byte[] pixels = MapPalette.imageToBytes(MapPalette.resizeImage(paneImage));
					int id = p.getEntityWorld().getUniqueDataId("map");
					String name = "map_" + id;
					MapData mapdata = new MapData(name);
					p.getEntityWorld().setItemData(name, mapdata);
					mapdata.colors = pixels;
					mapdata.scale = 0;
					mapdata.dimension = p.getEntityWorld().provider.getDimension();
					mapdata.trackingPosition = false;
					mapdata.markDirty();
					giveMap(p, id);
				}
				catch (RasterFormatException e)
				{

				}

			}
		}

	}

	private static void giveMap(EntityPlayerMP p, int id)
	{
		ItemStack itemstack = new ItemStack(Items.FILLED_MAP, 1, id);
		p.inventory.addItemStackToInventory(itemstack);
	}

	public static Vector2d getMapSize(String url) throws MalformedURLException, IOException
	{
		BufferedImage bufferedImage = ImageIO.read(new URL(url));

		int imageX = bufferedImage.getWidth();
		int imageY = bufferedImage.getHeight();

		while (imageX % 128 != 0)
		{
			imageX++;
		}

		while (imageY % 128 != 0)
		{
			imageY++;
		}

		int xPanes = imageX / 128;
		int yPanes = imageY / 128;

		return new Vector2d(xPanes, yPanes);
	}

}
