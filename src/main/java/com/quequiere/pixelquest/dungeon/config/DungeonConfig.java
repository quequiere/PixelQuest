package com.quequiere.pixelquest.dungeon.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.tools.ItemStackDeserializer;
import com.quequiere.pixelquest.tools.ItemStackSerializer;

import net.minecraft.item.ItemStack;

public class DungeonConfig
{
	
	
	private static File folder = new File(Pixelquest.folder.getAbsolutePath()+"/dungeon/instance");
	public static ArrayList<Dungeon> loaded = new ArrayList<Dungeon>();
	
	
	public static Optional<Dungeon> getByName(String name)
	{
		for(Dungeon d:loaded)
		{
			if(d.getName().equalsIgnoreCase(name))
			{
				return Optional.of(d);
			}
		}
		return Optional.empty();
	}
	
	
	
	public static void save(Dungeon d)
	{
		Writer writer = null;
		try
		{

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/"+d.getName()+".json");
			if (!file.exists())
			{
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(d.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public static void reloadFile()
	{
		folder.mkdirs();
		loaded.clear();
		
		for(File f:folder.listFiles())
		{
			if (f.exists())
			{
				BufferedReader br = null;
				try
				{
					br = new BufferedReader(new FileReader(f));
					StringBuilder sb = new StringBuilder();
					String line = br.readLine();

					while (line != null)
					{
						sb.append(line);
						sb.append(System.lineSeparator());
						line = br.readLine();
					}
					String everything = sb.toString();
					Dungeon d = Dungeon.fromJson(everything);
					if(d!=null)
					{
						loaded.add(d);
						System.out.println("Loaded dungeon: "+d.getName());
					}
					else
					{
						System.out.println("Fail load dungeon: "+f.getAbsolutePath());
					}
				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				finally
				{
					try
					{
						br.close();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
			else
			{
				System.out.println("Cant find: " + f.getAbsolutePath());
				try
				{
					f.createNewFile();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

	

		
	}

}
