package com.quequiere.pixelquest.dungeon.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.tools.ItemStackDeserializer;
import com.quequiere.pixelquest.tools.ItemStackSerializer;

import net.minecraft.item.ItemStack;

public class CustomPixelmonConfig
{
	public static CustomPixelmonConfig config = null;
	
	private static File folder = new File(Pixelquest.folder.getAbsolutePath()+"/dungeon/");
	public ArrayList<CustomPixelmon> loaded = new ArrayList<CustomPixelmon>();
	
	
	public Optional<CustomPixelmon> getByName(String name)
	{
		for(CustomPixelmon d:loaded)
		{
			if(d.getName().equalsIgnoreCase(name))
			{
				return Optional.of(d);
			}
		}
		return Optional.empty();
	}
	
	public String toJson()
	{
		ItemStackSerializer as = new ItemStackSerializer();
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, as).create();
		return gson.toJson(this);
	}

	public static CustomPixelmonConfig fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, new ItemStackDeserializer()).create();
		return gson.fromJson(s, CustomPixelmonConfig.class);
	}
	
	
	public void save()
	{
		Writer writer = null;
		try
		{

			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() + "/customPixelmons.json");
			if (!file.exists())
			{
				file.createNewFile();
			}

			writer = new OutputStreamWriter(new FileOutputStream(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public static CustomPixelmonConfig reloadFile()
	{
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath() + "/customPixelmons.json");

		if (f.exists())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				CustomPixelmonConfig pp = CustomPixelmonConfig.fromJson(everything);
				return pp;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			System.out.println("Cant find: " + f.getAbsolutePath());
			try
			{
				f.createNewFile();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

}
