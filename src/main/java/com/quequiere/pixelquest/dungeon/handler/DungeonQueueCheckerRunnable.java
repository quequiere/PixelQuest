package com.quequiere.pixelquest.dungeon.handler;

import java.util.ArrayList;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.queue.DungeonQueue;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class DungeonQueueCheckerRunnable implements Runnable
{
	private long last = 0;
	private static long timer= 10*1000;

	@Override
	public void run()
	{
		long now = System.currentTimeMillis();
		long diff = now-last;

		if(diff<timer)
		{
			return;
		}
		
		last = now;
		this.go();
	}

	private void go()
	{
		long timeS = System.currentTimeMillis()/1000;
		for(Dungeon dl:DungeonConfig.loaded)
		{
			if(dl.getCurrentGame()==null)
			{
				DungeonQueue dq = dl.getQueue();
				
				if(dq.getRealSize()<dq.getRange().getMin())
				{
					dq.setStartedTimerSeconds(-1);
				}
				else if(dq.getStartedTimerSeconds()==-1)
				{
					dq.setStartedTimerSeconds(timeS);
					
					for(EntityPlayerMP p :Tools.getAllPlayers())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + dl.getName()+" dungeon will start soon. Join with "+TextFormatting.BOLD+"/dq"));
					}
				}
				else
				{
					long diff = timeS-dq.getStartedTimerSeconds();
					
					if(diff>=dl.getMinTimeBeforeRunningSeconds()||dq.getRealSize()>=dq.getRange().getMax())
					{
						ArrayList<String> pn = new ArrayList<String>();
						
						for(EntityPlayerMP p:dl.getQueue().getOnlinePlayers())
						{
							pn.add(p.getName());
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Dungeon is starting !"));
						}
						
						dl.initializeGame(pn);
						
						for(EntityPlayerMP p :Tools.getAllPlayers())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + dl.getName()+" dungeon has been started ! (/dq for more info)"));
						}
					}
					
				}
				
			}
		}
		
	}


}
