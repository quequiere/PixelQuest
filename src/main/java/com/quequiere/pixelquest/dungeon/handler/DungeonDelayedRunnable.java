package com.quequiere.pixelquest.dungeon.handler;

import java.util.HashMap;

public class DungeonDelayedRunnable implements Runnable
{
	private long last = 0;
	private static long timer= 1*500;
	public static HashMap<Long, Runnable> toExec = new HashMap<Long, Runnable>();

	@Override
	public void run()
	{
		long now = System.currentTimeMillis();
		long diff = now-last;

		if(diff<timer)
		{
			return;
		}
		
		last = now;
		this.go();
	}

	private void go()
	{
		long now = System.currentTimeMillis();
		 HashMap<Long, Runnable> liste = (HashMap<Long, Runnable>) toExec.clone();
		 
		 for(Long l:liste.keySet())
		 {
			 long diff = l-now;
			 
			 if(diff<=0)
			 {
				 liste.get(l).run();
				 toExec.remove(l);
			 }
			 
		 }
		 
	}


}
