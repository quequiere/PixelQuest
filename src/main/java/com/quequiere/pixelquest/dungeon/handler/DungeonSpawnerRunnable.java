package com.quequiere.pixelquest.dungeon.handler;

import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.objects.SpawnLocation;

public class DungeonSpawnerRunnable implements Runnable
{
	private long last = 0;
	private static long timer= 3*1000;

	@Override
	public void run()
	{
		long now = System.currentTimeMillis();
		long diff = now-last;

		if(diff<timer)
		{
			return;
		}
		
		last = now;
		this.go();
	}

	private void go()
	{
		for(Dungeon dl:DungeonConfig.loaded)
		{
			if(dl.getCurrentGame()!=null)
			{
				for(SpawnLocation sl:dl.getSpawnsLocation())
				{
					if(sl.isNeedToBeUpdate())
					{
						sl.update();
					}
				}
			}
		}
		
	}


}
