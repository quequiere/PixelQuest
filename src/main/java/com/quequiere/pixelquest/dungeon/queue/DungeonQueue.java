package com.quequiere.pixelquest.dungeon.queue;

import java.util.ArrayList;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.objects.RangedInt;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class DungeonQueue
{
	
	private RangedInt range;
	private ArrayList<String> players = new ArrayList<String>();
	private long startedTimerSeconds = -1;
	
	
	public DungeonQueue (RangedInt range)
	{
		this.range=range;
	}
	
	public boolean isToTheMax()
	{
		if(this.getRealSize()>=range.getMax())
		{
			return true;
		}
		return false;
	}
	
	public void addPlayer(String player)
	{
		if(!players.contains(player))
		{
			for(EntityPlayerMP pp:this.getOnlinePlayers())
			{
				pp.addChatMessage(new TextComponentString(TextFormatting.GREEN + "+ "+player+" joined the queue."));
			}
			
			players.add(player);
		}
	}
	
	public ArrayList<EntityPlayerMP> getOnlinePlayers()
	{
		 ArrayList<EntityPlayerMP> plist = new ArrayList<EntityPlayerMP>();
		 this.checkOnlinePlayers();
		 
		 for(String pn:players )
		 {
			 EntityPlayerMP ppp=Tools.getPlayer(pn);
			 if(ppp!=null)
			 {
				 plist.add(ppp);
			 }
			
		 }
		 
		 return plist;
		
	}
	
	public int getRealSize()
	{
		this.checkOnlinePlayers();
		return players.size();
	}
	
	private void checkOnlinePlayers()
	{
		ArrayList<String> cl = (ArrayList<String>) players.clone();
		for(String pname:cl)
		{
			if(Tools.getPlayer(pname)==null)
			{
				players.remove(pname);
			}
		}
	}

	public RangedInt getRange()
	{
		return range;
	}

	public ArrayList<String> getPlayers()
	{
		this.checkOnlinePlayers();
		return players;
	}

	public long getStartedTimerSeconds()
	{
		return startedTimerSeconds;
	}

	public void setStartedTimerSeconds(long startedTimerSeconds)
	{
		this.startedTimerSeconds = startedTimerSeconds;
	}
	
	

}
