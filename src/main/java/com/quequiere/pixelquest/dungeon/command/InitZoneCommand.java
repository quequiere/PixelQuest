package com.quequiere.pixelquest.dungeon.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.objects.SpawnLocation;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class InitZoneCommand implements ICommand
{
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "initzone";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "initzone";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("iz");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

			String perm = "pixelquest.initzone";
			if (!Pixelquest.hasPermission(sender, perm))
			{
				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}
			
			// /iz dname zonename
			
			
			if(args.length<2)
			{
				sender.addChatMessage(new TextComponentString(TextFormatting.RED + "/iz dungeonname zonename"));
				return;
			}
			else
			{
				Dungeon d = null;
				for(Dungeon dl:DungeonConfig.loaded)
				{
					if(dl.getName().equals(args[0]))
					{
						d=dl;
						break;
					}
				}
				
				if(d==null)
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find this dungeon !"));
					return;
				}
				
				Optional<SpawnLocation> zoneO = d.getSpawnLocationByName(args[1]);
				
				if(!zoneO.isPresent())
				{
					sender.addChatMessage(new TextComponentString(TextFormatting.RED + "We can't find this zone !"));
					return;
				}
				
				zoneO.get().forceAllSpawn();
				zoneO.get().setNeedToBeUpdate(true);
				sender.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Zone is now initialized !"));
			}
			
		

		

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}


}
