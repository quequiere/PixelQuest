package com.quequiere.pixelquest.dungeon.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.objects.SpawnLocation;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class AddSpawnLocationCommand implements ICommand
{
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "addspawnlocation";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "addspawnlocation";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("asl");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;

			String perm = "pixelquest.addspawnlocation";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}
			
			Dungeon d = null;
			for(Dungeon dl:DungeonConfig.loaded)
			{
				if(dl.isInZone(p))
				{
					d=dl;
					break;
				}
			}
			
			if(d==null)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to be inside dungeon to do that !"));
				return;
			}
			
			// /als addloc [NAME]
			// /als create [name] [CustomPixel] [RepopTime] [maxspawn]
			
			if(args.length==2)
			{
				if(args[0].equals("addloc"))
				{
					Optional<SpawnLocation> sl = d.getSpawnLocationByName(args[1]);
					if(!sl.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This spawn loc doesn't exist in this dungeon"));
						return;
					}
					
					
					sl.get().addLocation(Location.fromBlockPos(p.getPosition().add(0, 1, 0), p.getEntityWorld()));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Location added !"));
					DungeonConfig.save(d);
				}
				else if(args[0].equals("remove"))
				{
					Optional<SpawnLocation> sl = d.getSpawnLocationByName(args[1]);
					if(!sl.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This spawn loc doesn't exist in this dungeon"));
						return;
					}
					
					
					
					d.getSpawnsLocation().remove(sl.get());
					sl.get().deleteEntities();
					sl.get().setNeedToBeUpdate(false);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Location removed !"));
					DungeonConfig.save(d);
				}
			}
			else if(args.length==1 && args[0].equals("list"))
			{
					for(SpawnLocation sl:d.getSpawnsLocation())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "- "+sl.getName()));
					}
					
				
			}
			else if(args.length==5)
			{
				if(args[0].equals("create"))
				{
					Optional<SpawnLocation> sl = d.getSpawnLocationByName(args[1]);
					if(sl.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This spawn loc already exist un dungeon"));
						return;
					}
					
					Optional<CustomPixelmon> cus = CustomPixelmonConfig.config.getByName(args[2]);
					if(!cus.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This custom pokemon doesn't exist"));
						return;
					}
					
					try
					{
						long rep = Long.parseLong(args[3]);
						int maxspawn = Integer.parseInt(args[4]);
						d.addSpawnLocation(new SpawnLocation(args[1], cus.get(), rep, maxspawn));
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Spawn location added"));
					}
					catch(NumberFormatException e)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while parsing numbers"));
						return;
					}
					
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/als list"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/asl addloc [NAME]"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/asl remove [NAME]"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/asl create [name] [CustomPixel] [RepopTime] [maxspawn]"));
				return;
			}
			
		

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}


}
