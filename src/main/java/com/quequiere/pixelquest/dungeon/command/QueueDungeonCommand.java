package com.quequiere.pixelquest.dungeon.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.objects.SpawnLocation;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class QueueDungeonCommand implements ICommand
{
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "dungeonqueue";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "dungeonqueue";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("dq");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p = sender;

		String perm = "pixelquest.dungeonqueue";
		if (!Pixelquest.hasPermission(p, perm))
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
			return;
		}

		if (args.length < 1)
		{
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/dq status"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/dq join [dungeonName]"));
			p.addChatMessage(new TextComponentString(TextFormatting.RED + "/dq leave"));
			
			return;
		}
		else
		{
			String subc = args[0];

			if (subc.equalsIgnoreCase("status"))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "--- Dungeon status ---"));
				for (Dungeon d : DungeonConfig.loaded)
				{
					if (d.getCurrentGame() != null)
					{
						String li = "";
						for (String pl : d.getCurrentGame().getProfile().getPlayers())
						{
							li += " " + pl;
						}
						
						
						p.addChatMessage(new TextComponentString(TextFormatting.RED + d.getName() + ": " + TextFormatting.GRAY + " game currently running: "+li));
					}
					else
					{

						String li = "";
						for (String pl : d.getQueue().getPlayers())
						{
							li += " " + pl;
						}

						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + d.getName() + ": " + TextFormatting.GREEN + " join queue with /dq join " + d.getName()));
						p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "" + d.getQueue().getRealSize() + "/" + d.getQueue().getRange().getMax()));
						p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Min players: " + d.getQueue().getRange().getMin() + " Players list:" + li));
						
						long timeS = System.currentTimeMillis()/1000;
						long diff = timeS-d.getQueue().getStartedTimerSeconds();
						long rem = d.getMinTimeBeforeRunningSeconds()-diff;
						if(rem>0)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Time remaining before start in seconds: "+rem));
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.GRAY + "Waiting for more players"));
						}
						
					}
				}
			}
			else if (subc.equalsIgnoreCase("leave"))
			{
				for(Dungeon dcheck:DungeonConfig.loaded)
				{
				
					if(dcheck.getCurrentGame()==null && dcheck.getQueue()!=null && dcheck.getQueue().getPlayers()!=null &&dcheck.getQueue().getPlayers().contains(p.getName()))
					{
						dcheck.getQueue().getPlayers().remove(p.getName());
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You leave queue for "+dcheck.getName()));
					}
					
				}
				
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Command executed !"));
			}
			else if (subc.equalsIgnoreCase("join"))
			{
				if(args.length<2)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please provide a dungeon name !"));
					return;
				}
				else
				{
					Optional<Dungeon> doo = DungeonConfig.getByName(args[1]);
					if(!doo.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Sorry we can't find this dungeon !"));
						return;
					}
					
					Dungeon d = doo.get();
					
					
					if(!d.isOpen())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This dungeon is closed for now !"));
						return;
					}
					
					if(d.getCurrentGame()!=null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't join a running dungeon !"));
						return;
					}
					
					if(d.getQueue().isToTheMax())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You can't join the queue, max player reached !"));
						return;
					}
					
					
					for(Dungeon dcheck:DungeonConfig.loaded)
					{
					
						if(dcheck.getCurrentGame()==null && dcheck.getQueue().getPlayers().contains(p.getName()))
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "You are already in a queue !"));
							return;
						}
						
					}
					
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You joined the queue for dungeon "+d.getName()));
					d.getQueue().addPlayer(p.getName());
					
				}
			}
			else
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow sub command"));
				return;
			}

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}

}
