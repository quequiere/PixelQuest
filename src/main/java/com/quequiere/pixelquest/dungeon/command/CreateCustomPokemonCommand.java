package com.quequiere.pixelquest.dungeon.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.RangedFloat;
import com.quequiere.pixelquest.dungeon.objects.RangedInt;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CreateCustomPokemonCommand implements ICommand
{

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "createcustompixelmon";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "createcustompixelmon";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("ccp");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;

			String perm = "pixelquest.createcustompixelmon";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}
			
			// /ccp name level-level size-size hp-hp pname
			
			if(args.length==1 && args[0].equalsIgnoreCase("list"))
			{
				for(CustomPixelmon poke:CustomPixelmonConfig.config.loaded)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "- "+poke.getName()));
				}
				return;
			}
			
			if(args.length<6)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/ccp name level-level size-size hp-hp pokemonname capturable[true/false]"));
				
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/ccp list"));
				return;
			}
			else
			{
				String name = args[0];
				int minlvl = 0;
				int maxlvl = 0;
				float minsize = 0;
				float maxsize = 0;
				int minhp =0;
				int maxhp=0;
				EnumPokemon pt = null;
				boolean capturable = false;
				
				if(CustomPixelmonConfig.config.getByName(name).isPresent())
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Already exist"));
					return;
				}
				
				try
				{
					minlvl=Integer.parseInt(args[1].split("-")[0]);
					maxlvl=Integer.parseInt(args[1].split("-")[1]);
					
					minsize=Float.parseFloat(args[2].split("-")[0]);
					maxsize=Float.parseFloat(args[2].split("-")[1]);
					
					minhp=Integer.parseInt(args[3].split("-")[0]);
					maxhp=Integer.parseInt(args[3].split("-")[1]);
					
				}
				catch(NumberFormatException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while parsing number"));
					return;
				}
				catch(ArrayIndexOutOfBoundsException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while parsing range"));
					return;
				}
				
				
				try
				{
					pt = EnumPokemon.valueOf(args[4]);
				}
				catch(IllegalArgumentException e)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while searching pokemon name"));
					return;
				}
				
				
				String bool = args[5];
				if(bool.equalsIgnoreCase("true"))
				{
					capturable=true;
				}
				else if(bool.equalsIgnoreCase("false"))
				{
					capturable=false;
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Error while set capturable state !"));
					return;
				}
				
				
				CustomPixelmon cp = new CustomPixelmon(name, new RangedInt(minlvl, maxlvl), new RangedFloat(minsize, maxsize), new RangedInt(minhp, maxhp),pt,capturable);
				CustomPixelmonConfig.config.loaded.add(cp);
				CustomPixelmonConfig.config.save();
				
				p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "New pokemon type added !"));
			}
		
			
			

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}

	
}
