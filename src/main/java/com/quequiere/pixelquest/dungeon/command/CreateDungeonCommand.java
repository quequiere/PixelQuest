package com.quequiere.pixelquest.dungeon.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.quest.Quest;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class CreateDungeonCommand implements ICommand
{

	public static HashMap<EntityPlayerMP, Dungeon> liste = new HashMap<EntityPlayerMP, Dungeon>();

	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "createdungeon";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "createdungeon";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{

		if (sender instanceof EntityPlayerMP)
		{
			EntityPlayerMP p = (EntityPlayerMP) sender;

			String perm = "pixelquest.createdungeon";
			if (!Pixelquest.hasPermission(p, perm))
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
				return;
			}
			
		
			if(!liste.containsKey(p))
			{
				if(args.length<1)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Please give a name"));
					return;
				}
				else if(args.length==1)
				{
					String name = args[0];
					if(DungeonConfig.getByName(name).isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Already exist"));
						return;
					}
					else
					{
						Dungeon d = new Dungeon(name);
						d.setSpawnPoint(new Location(p));
						liste.put(p, d);
						p.addChatMessage(new TextComponentString(TextFormatting.BLUE + "Going to pos1 then retype command"));
						return;
					}
				}
				else if(args.length==2)
				{
					// /createdungeon dname questname
					String name = args[0];
					Optional<Dungeon> d = DungeonConfig.getByName(name);
					if(d.isPresent())
					{
						Quest q = Quest.getQuestByName(args[1]);
						if(q!=null)
						{
							d.get().setQuestOpening(q.getName());
							DungeonConfig.save(d.get());
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "quest added !"));
							
						}
						else
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "This quest doesn't exist !"));
						}
						
						return;
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Can't find this dungeon !"));
						return;
					}
					
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.BLUE + "Error in subcommand!"));
					return;
				}
			}
			else
			{
				Dungeon d = liste.get(p);
				if(d.getL1()==null)
				{
					d.setL1(Location.fromBlockPos(p.getPosition(), p.getEntityWorld()));
					p.addChatMessage(new TextComponentString(TextFormatting.BLUE + "Going to pos2 and retype command"));
					return;
				}
				else
				{
					d.setL2(Location.fromBlockPos(p.getPosition(), p.getEntityWorld()));
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Dungeon created !"));
					DungeonConfig.loaded.add(d);
					DungeonConfig.save(d);
					liste.remove(p);
					return;
				}
			}
		

		}

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}


}
