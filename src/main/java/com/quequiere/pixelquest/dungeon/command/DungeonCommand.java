package com.quequiere.pixelquest.dungeon.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.CustomPixelmon;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.dungeon.objects.SpawnLocation;
import com.quequiere.pixelquest.npc.NpcShopKeeperCreator;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.tools.TeleportUtility;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.server.FMLServerHandler;

public class DungeonCommand implements ICommand
{
	private List aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getCommandName()
	{
		// TODO Auto-generated method stub
		return "dungeon";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return "dungeon";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList();
		aliases.add("d");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
		ICommandSender p =sender;
	
	
			if(args.length<1)
			{
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use args:"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d leave"));
				
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d setcanjoin [dungeName] [true/false]"));
				
				
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d close [dungeonName]"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d maxspawn [dungeonName]"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d start [dungeonName] player1 player2 ..."));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d setgeneralSpawn"));
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d invoke [playerName]"));
				
				
				p.addChatMessage(new TextComponentString(TextFormatting.RED + "/d add [dungeonName] [playerName]"));
				
				return;
			}
			else
			{

				
				String subc = args[0];
			
				if(subc.equalsIgnoreCase("leave") && sender instanceof EntityPlayerMP)
				{
					EntityPlayerMP pp = (EntityPlayerMP) sender;
					boolean find = false;
					
					for(Dungeon d:DungeonConfig.loaded)
					{
						if(d.getCurrentGame()!=null)
						{
							d.getCurrentGame().removePlayer(p.getName());
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Leave: "+d.getName()));
							find = true;
							if(d.getCurrentGame().getProfile().getOnlineTeamPlayers().size()<=0)
							{
								d.close();
								for(EntityPlayerMP pt:Tools.getAllPlayers())
								{
									pt.addChatMessage(new TextComponentString(TextFormatting.BLUE + d.getName()+" has been closed cause need more player !"));
								}
							}
						}
						
						
					
					}
					
					
					if(find)
					{
						PlayerProfile plap = PlayerProfile.getPlayerProfile(pp.getName(), false);
						plap.getQuestProfile().forceupdateScoreBoard(pp);
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You are not in dungeon !"));
					}
					
					Pixelquest.generalConfig.getDungeonHub().teleportPlayer(pp);
				
					
					
				}
				else if(subc.equalsIgnoreCase("setcanjoin"))
				{
					String perm = "pixelquest.dungeon.admin";
					if (!Pixelquest.hasPermission(p, perm))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
						return;
					}
					
					if(args.length<3)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need dungeonname and true/false"));
						return;
					}
					
					String dname = args[1];
					Optional<Dungeon> d = DungeonConfig.getByName(dname);
					if(!d.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This dungeon doesn't exist"));
						return;
					}
					
					String option = args[2];
					
					if(option.equalsIgnoreCase("true"))
					{
						if(d.get().isOpen())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Dungeon already opened !"));
							return;
						}
						else
						{
							d.get().setOpen(true);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Dungeon now opened !"));
							
							for(EntityPlayerMP tpp:Tools.getAllPlayers())
							{
								tpp.addChatMessage(new TextComponentString(TextFormatting.AQUA + "Queue for dungeon "+d.get().getName()+" is now opened !"));
							}
							
							return;
						}
					}
					else if(option.equalsIgnoreCase("false"))
					{
						if(!d.get().isOpen())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Dungeon already closed !"));
							return;
						}
						else
						{
							d.get().setOpen(false);
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Dungeon now closed !"));
							
							for(EntityPlayerMP tpp:Tools.getAllPlayers())
							{
								tpp.addChatMessage(new TextComponentString(TextFormatting.AQUA + "Queue for dungeon "+d.get().getName()+" is now closed !"));
							}
							
							return;
						}
					}
					else
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Use true or false"));
						return;
					}
					
					
					
					
					
				}
				else if(subc.equalsIgnoreCase("invoke") && sender instanceof EntityPlayerMP)
				{
			
					EntityPlayerMP pp = (EntityPlayerMP) sender;
					
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need player name !"));
						return;
					}
					
					String target = args[1];
					EntityPlayerMP t = Tools.getPlayer(target);
					
					if(t==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This player is not online !"));
						return;
					}
					
					for(Dungeon dcheck:DungeonConfig.loaded)
					{
						if(dcheck.getCurrentGame()!=null && dcheck.getCurrentGame().getProfile().getPlayers().contains(pp.getName()))
						{
							if(dcheck.isInZone(pp))
							{
								if(!dcheck.isInZone(t))
								{
									if( dcheck.getCurrentGame().getProfile().getPlayers().contains(t.getName()))
									{
										new Location(pp).teleportPlayer(t);
										p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Player teleported !"));
										return;
									}
									else
									{
										p.addChatMessage(new TextComponentString(TextFormatting.RED + "This player has not join the dungeon !"));
										return;
									}
								
									
								}
								else
								{
									
									Location l = dcheck.getSpawnPoint();
									Location ploc = new Location(t);
									if(l.getDistance(ploc)>5)
									{
										p.addChatMessage(new TextComponentString(TextFormatting.RED + "The player need to be at least at 5 meter of the dungeon spawn location."));
										t.addChatMessage(new TextComponentString(TextFormatting.RED + "A player wants to teleport you at his location but you need to be at least at 5 meter of the dungeon spawn location."));
										return;
									}
									else
									{
										p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Invoke successful !"));
										t.addChatMessage(new TextComponentString(TextFormatting.AQUA + p.getName()+" teleported you at his location"));
										new Location(pp).teleportPlayer(t);
										return;
										
									}
									
								}
							}
							else
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need to be in dungeon to do that !"));
								return;
							}
						}
					}	
					
					
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "You are not currently in a dungeon !"));
					
				}
				else if(subc.equalsIgnoreCase("maxspawn")&& sender instanceof EntityPlayerMP)
				{
					String perm = "pixelquest.dungeon.admin";
					if (!Pixelquest.hasPermission(p, perm))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
						return;
					}
					
					
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need more args !"));
						return;
					}
					else
					{
						String name = args[1];
						Optional<Dungeon> d = DungeonConfig.getByName(name);
						if(!d.isPresent())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "This dungeon doesn't exist"));
							return;
						}
						else
						{
							d.get().removeAllPokemonInArea();
							d.get().forMaxSpawn();
							p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Dungeon maxspawned"));
						}
					}
					
					
				}
				else if(subc.equalsIgnoreCase("setgeneralSpawn") && sender instanceof EntityPlayerMP)
				{
					EntityPlayerMP pp = (EntityPlayerMP) sender;
					String perm = "pixelquest.dungeon.admin";
					if (!Pixelquest.hasPermission(p, perm))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
						return;
					}
					
					Location l  = new Location(pp);
					Pixelquest.generalConfig.setDungeonHub(l);
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Hub setted !"));
					
				}
				else if(subc.equalsIgnoreCase("add"))
				{
					String perm = "pixelquest.dungeon.admin";
					if (!Pixelquest.hasPermission(p, perm))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
						return;
					}
					
					if(args.length<3)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need dungeonname and playername"));
						return;
					}
					
					String dname = args[1];
					Optional<Dungeon> d = DungeonConfig.getByName(dname);
					if(!d.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This dungeon doesn't exist"));
						return;
					}
					
					if(d.get().getCurrentGame()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "None dungeon running !"));
						return;
					}
					
					EntityPlayerMP target = Tools.getPlayer(args[2]);
					if(target==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This player is not online"));
						return;
					}
					
					if(d.get().getCurrentGame().getProfile().getPlayers().contains(target.getName()))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This player is already in dungeon"));
						return;
					}
					
					d.get().getCurrentGame().getProfile().getPlayers().add(target.getName());
					d.get().getCurrentGame().getProfile().getQuestProfile().refreshQuest(target);
					
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Player added to dungeon !"));
					target.addChatMessage(new TextComponentString(TextFormatting.AQUA + "You have been added to dungeon "+d.get().getName()+", ask to a player inside to /d invoke you."));
					
					for(EntityPlayerMP ppp:d.get().getCurrentGame().getProfile().getOnlineTeamPlayers())
					{
						ppp.addChatMessage(new TextComponentString(TextFormatting.AQUA +target.getName()+ " has been added to you current dungeon game. Use /d invoke "+target.getName()+" to teleport him in the dungeon !"));
					}
					
					
				}
				else if(subc.equalsIgnoreCase("close"))
				{
					String perm = "pixelquest.dungeon.admin";
					if (!Pixelquest.hasPermission(p, perm))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
						return;
					}
					
					if(args.length<2)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need dungeonname"));
						return;
					}
					
					String dname = args[1];
					Optional<Dungeon> d = DungeonConfig.getByName(dname);
					if(!d.isPresent())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "This dungeon doesn't exist"));
						return;
					}
					
					if(d.get().getCurrentGame()==null)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "None dungeon running !"));
						return;
					}
					
					d.get().close();
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Closed !"));
					
					for(EntityPlayerMP pp:Tools.getAllPlayers())
					{
						p.addChatMessage(new TextComponentString(TextFormatting.AQUA + d.get().getName() +" dungeon has been ended !"));
					}
					
				}
				else if(subc.equalsIgnoreCase("start"))
				{
					String perm = "pixelquest.dungeon.admin";
					if (!Pixelquest.hasPermission(p, perm))
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "You need perm: " + perm));
						return;
					}
					
					if(args.length<4)
					{
						p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need dungeonname and more players"));
						return;
					}
					
						String dname = args[1];
						Optional<Dungeon> d = DungeonConfig.getByName(dname);
						if(!d.isPresent())
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "This dungeon doesn't exist"));
							return;
						}
						
						if(d.get().getCurrentGame()!=null)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "A dungeon is currently running !"));
							return;
						}
						
						
						
						ArrayList<String> players = new ArrayList<String>();
						for(int x=2;x<args.length;x++)
						{
							String name = args[x];
							EntityPlayerMP target = Tools.getPlayer(name);
							if(target==null)
							{
								p.addChatMessage(new TextComponentString(TextFormatting.RED + "A player is not online: "+name));
								return;
							}
							
							players.add(target.getName());
							
						}
						
						if(players.size()<2)
						{
							p.addChatMessage(new TextComponentString(TextFormatting.RED + "Need more players !"));
							return;
						}
						
						d.get().initializeGame(players);
						
						p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Dungeon started !"));
						
					
				}
				else
				{
					p.addChatMessage(new TextComponentString(TextFormatting.RED + "Unknow command"));
					return;
				}
				
			}

			
		

		

	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		// TODO Auto-generated method stub
		return new ArrayList();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		// TODO Auto-generated method stub
		return false;
	}


}
