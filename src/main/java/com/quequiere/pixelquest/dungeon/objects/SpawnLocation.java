package com.quequiere.pixelquest.dungeon.objects;

import java.util.ArrayList;
import java.util.Optional;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.CustomPixelmonConfig;

public class SpawnLocation
{

	private String name;
	private ArrayList<LinkedLocation> locationEntities = new ArrayList<LinkedLocation>();
	private String customPixelmon;

	private long repopTime = -1;
	private transient long lastCheck = 0;

	private int maxSpawn = 2;
	
	private transient boolean needToBeUpdate = false;

	public SpawnLocation(String name, CustomPixelmon pokemon, long repopTime, int maxSpawn)
	{
		this.name = name;
		this.customPixelmon = pokemon.getName();
		this.repopTime = repopTime;
		this.maxSpawn = maxSpawn;
	}
	
	
	

	public ArrayList<LinkedLocation> getLocationEntities()
	{
		return locationEntities;
	}




	public boolean isNeedToBeUpdate() {
		return needToBeUpdate;
	}


	public void setNeedToBeUpdate(boolean needToBeUpdate) {
		this.needToBeUpdate = needToBeUpdate;
	}


	public String getName()
	{
		return name;
	}

	public void deleteEntities()
	{
		for (LinkedLocation l : locationEntities)
		{
			l.removePokemon();
		}
	}

	private void domaxSpawn()
	{
		for (int x = 0; x < this.maxSpawn; x++)
		{
			Optional<LinkedLocation> opt = this.getPossibleLocation();
			if (opt.isPresent())
			{
				EntityPixelmon pokeEntity = CustomPixelmonConfig.config.getByName(customPixelmon).get().spawn(opt.get().getLoc());
				opt.get().setPokemon(pokeEntity);
			}
		}
	}

	public void forceAllSpawn()
	{
		this.deleteEntities();
		this.domaxSpawn();
	}

	public void update()
	{
		long now = System.currentTimeMillis() / 1000;
		long diff = now - lastCheck;
		
		if(repopTime==-1)
			return;

		if (diff > repopTime)
		{
			lastCheck = now;
			if (countOnlinePokemon() < maxSpawn)
			{
				Optional<LinkedLocation> opt = this.getPossibleLocation();
				if (opt.isPresent())
				{
					EntityPixelmon pokeEntity = CustomPixelmonConfig.config.getByName(customPixelmon).get().spawn(opt.get().getLoc());
					opt.get().setPokemon(pokeEntity);
				}
			}
		}

	}

	private int countOnlinePokemon()
	{
		int count = 0;
		for (LinkedLocation l : locationEntities)
		{
			if (l.hasAvailablePokemon())
			{
				count++;
			}
		}
		return count;
	}

	public void addLocation(Location l)
	{
		this.locationEntities.add(new LinkedLocation(l));
	}

	private Optional<LinkedLocation> getPossibleLocation()
	{
		ArrayList<LinkedLocation> locationsPossible = new ArrayList<LinkedLocation>();

		for (LinkedLocation l : locationEntities)
		{

			if (!l.hasAvailablePokemon())
			{
				locationsPossible.add(l);
			}
		}

		if (locationsPossible.size() == 0)
		{
			return Optional.empty();
		}
		else if (locationsPossible.size() == 1)
		{
			return Optional.of(locationsPossible.get(0));
		}

		int index = Tools.getAleatInt(0, locationsPossible.size() - 1);
		return Optional.of(locationsPossible.get(index));
	}




	public String getCustomPixelmon()
	{
		return customPixelmon;
	}
	
	

}
