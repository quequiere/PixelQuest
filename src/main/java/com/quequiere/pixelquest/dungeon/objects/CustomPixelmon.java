package com.quequiere.pixelquest.dungeon.objects;

import java.lang.reflect.Field;
import java.util.Set;
import java.util.UUID;

import com.pixelmonmod.pixelmon.AI.AIFlyingPersistent;
import com.pixelmonmod.pixelmon.AI.AITargetNearest;
import com.pixelmonmod.pixelmon.comm.EnumUpdateType;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.EnumAggression;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.AIHelper;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.quequiere.pixelquest.Location;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.ai.attributes.AbstractAttributeMap;
import net.minecraft.entity.ai.attributes.AttributeMap;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.ai.attributes.ModifiableAttributeInstance;
import net.minecraft.entity.ai.attributes.RangedAttribute;

public class CustomPixelmon
{

	private String name;
	private RangedInt level;
	private RangedFloat size;
	private RangedInt hp;
	private EnumPokemon pokemonType;
	private boolean isCapturable = false;

	public CustomPixelmon(String name, RangedInt level, RangedFloat size, RangedInt hp, EnumPokemon pokemonType,boolean isCapturable)
	{
		this.name = name;
		this.level = level;
		this.size = size;
		this.hp = hp;
		this.pokemonType = pokemonType;
		this.isCapturable=isCapturable;
	}

	public EntityPixelmon spawn(Location loc)
	{
		int levelfinal = this.level.getAleatInt();
		float sizefinal = this.size.getAleatFloat();
		int hpfinal = this.hp.getAleatInt();

		EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityByName(pokemonType.name, loc.getWorld());
		pokemon.setPosition(loc.getX(), loc.getY(), loc.getZ());
		pokemon.getLvl().setLevel(levelfinal);
		pokemon.setPixelmonScale(sizefinal);
		pokemon.setIsShiny(false);
		
		

		AttributeMap map = (AttributeMap) pokemon.getAttributeMap();
		Field f1 = map.getClass().getDeclaredFields()[0];
		f1.setAccessible(true);
		try
		{
			Set<IAttributeInstance> f1o = (Set<IAttributeInstance>) f1.get(map);

			IAttribute temp = new RangedAttribute(null, "generic.maxHealth", 20.0D, 0.0D, Double.MAX_VALUE).setDescription("Max Health").setShouldWatch(true);

			for (Object ze : f1o.toArray())
			{
				ModifiableAttributeInstance zee = (ModifiableAttributeInstance) ze;

				Field atf = zee.getClass().getDeclaredFields()[1];
				atf.setAccessible(true);

				RangedAttribute ranged = (RangedAttribute) atf.get(zee);

				if (ranged.getDescription().contains("Health"))
				{
					atf.set(zee, temp);
				}

			}

		}
		catch (IllegalArgumentException e)
		{

			e.printStackTrace();
		}
		catch(IllegalAccessException e)
		{
			e.printStackTrace();
		}

		// vont de paire indique ce qu'on peut donner en pv au max, pas le
		// nombre de pv max !
		pokemon.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(hpfinal);
		pokemon.setHealth(hpfinal);

		// pv en cours
		pokemon.stats.HP = hpfinal;
		
		
		
		

		//System.out.println(pokemon.getHealth() + "/" + pokemon.getMaxHealth() + " ==> " + pokemon.hasFullHealth() + "base: " + pokemon.stats.HP);

		pokemon.canDespawn = false;
		pokemon.specialDungeonAvoidDespawn = true;
		pokemon.aggression = EnumAggression.aggressive;
		pokemon.setSpawnLocation(pokemon.getDefaultSpawnLocation());

		loc.getWorld().spawnEntityInWorld(pokemon);

		return pokemon;
	}

	public String getName()
	{
		return name;
	}

	public boolean isCapturable()
	{
		return isCapturable;
	}

	
}
