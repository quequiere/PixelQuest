package com.quequiere.pixelquest.dungeon.objects;

import java.util.ArrayList;

import com.quequiere.pixelquest.player.TeamProfile;

public class DungeonGame {

	private transient TeamProfile profile;
	
	public DungeonGame(Dungeon d,ArrayList<String> players)
	{
		profile=new TeamProfile("Dungeon_"+d.getName(),players);
	}

	public TeamProfile getProfile() {
		return profile;
	}
	
	public void removePlayer(String player)
	{
		this.getProfile().getPlayers().remove(player);
	}
	
	
	
}
