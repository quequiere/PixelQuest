package com.quequiere.pixelquest.dungeon.objects;

import java.util.List;

import com.google.common.base.Predicate;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.quequiere.pixelquest.Location;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;

public class Zone {
	
	protected Location l1, l2;
	protected String name;

	public Zone(Location l1, Location l2, String name)
	{
		this.l1 = l1;
		this.l2 = l2;
		this.name = name;
	}
	
	

	public void setL1(Location l1)
	{
		this.l1 = l1;
	}



	public void setL2(Location l2)
	{
		this.l2 = l2;
	}



	public String getName()
	{
		return name;
	}

	public Location getL1()
	{
		return l1;
	}

	public Location getL2()
	{
		return l2;
	}

	public Location getMinLoc()
	{
		return new Location(Math.min(this.l1.getX(), this.l2.getX()), Math.min(this.l1.getY(), this.l2.getY()), Math.min(this.l1.getZ(), this.l2.getZ()), this.l1.getWorld());
	}

	public Location getMaxLoc()
	{
		return new Location(Math.max(this.l1.getX(), this.l2.getX()), Math.max(this.l1.getY(), this.l2.getY()), Math.max(this.l1.getZ(), this.l2.getZ()), this.l1.getWorld());
	}

	public boolean isInZone(Entity p)
	{
		Location l = new Location(p.posX, p.posY, p.posZ, p.worldObj);
		if (this.isInZone(l))
		{
			return true;
		}
		return false;
	}

	public boolean isInZone(Location l)
	{
		if (!this.getL1().getWorld().equals(l.getWorld()))
		{
			return false;
		}

		Location max = this.getMaxLoc();
		Location min = this.getMinLoc();

		if (min.getX() <= l.getX() && l.getX() < max.getX())
		{
			if (min.getY() <= l.getY() && l.getY() < max.getY())
			{
				if (min.getZ() <= l.getZ() && l.getZ() < max.getZ())
				{
					return true;
				}
			}
		}

		return false;
	}
	
	public List<Entity> getEntitiesInAera(Class<? extends Entity> ent, Predicate<? super Entity> predi)
	{
		return this.l1.getWorld().getEntities(ent,predi);
	}

}
