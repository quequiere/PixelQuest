package com.quequiere.pixelquest.dungeon.objects;

import java.util.ArrayList;
import java.util.Optional;

import com.google.common.base.Predicate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.queue.DungeonQueue;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.tools.ItemStackDeserializer;
import com.quequiere.pixelquest.tools.ItemStackSerializer;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

public class Dungeon extends Zone {
	
	
	private ArrayList<ModificableZone> modificableZone = new ArrayList<ModificableZone>();

	private ArrayList<SpawnLocation> spawnsLocation = new  ArrayList<SpawnLocation>();
	private transient DungeonGame currentGame=null;
	
	
	private RangedInt queueSize = new RangedInt(2, 10);
	private int minTimeBeforeRunningSeconds = 3*60; 
	private transient DungeonQueue queue;
	private Location spawnPoint;
	
	private boolean open = false;
	
	private String questOpening = "";

	public Dungeon(String name)
	{
		super(null, null, name);
	}
	
	public DungeonQueue getQueue()
	{
		if(this.queue==null)
		{
			this.queue=new DungeonQueue(queueSize);
		}
		return this.queue;
	}
	
	
	
	
	public String getQuestOpening()
	{
		return questOpening;
	}

	public void setQuestOpening(String questOpening)
	{
		this.questOpening = questOpening;
	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
		DungeonConfig.save(this);
	}

	public RangedInt getQueueSize()
	{
		return queueSize;
	}

	public Location getSpawnPoint()
	{
		return spawnPoint;
	}

	public void setSpawnPoint(Location spawnPoint)
	{
		this.spawnPoint = spawnPoint;
	}

	public int getMinTimeBeforeRunningSeconds()
	{
		return minTimeBeforeRunningSeconds;
	}

	public void initializeGame(ArrayList<String> players)
	{
		EntityPlayerMP target = null;
		
		
		this.currentGame=new DungeonGame(this,players);
		for(String p:players)
		{
			EntityPlayerMP pp = Tools.getPlayer(p) ;
			if(pp!=null)
			{
				if(target==null)
				{
					target=pp;
				}
				spawnPoint.teleportPlayer(pp);
			}
			
		}
		this.queue=null;
		
		if(this.getQuestOpening()!=null && !this.getQuestOpening().equals(""))
		{
			Quest q = Quest.getQuestByName(this.getQuestOpening());
			if(q!=null)
			{
				PlayerProfile pp = PlayerProfile.getPlayerProfile(target.getName(), false);
				pp.getQuestProfile().addQuest(q, target, false);
			}
			else
			{
				System.out.println("can't find quest for dungeon "+this.getName()+" ==> "+this.getQuestOpening());
			}
		}
	
		
		
	}
	
	public void close()
	{
		ArrayList<String> machin = (ArrayList<String>) this.getCurrentGame().getProfile().getPlayers().clone();
		
		
		for(String aa:machin)
		{
			this.getCurrentGame().removePlayer(aa);
		}
		
		this.queue=null;
		this.currentGame=null;
		for(SpawnLocation sl:this.getSpawnsLocation())
		{
			sl.setNeedToBeUpdate(false);
			sl.deleteEntities();
		}
		this.removeAllPokemonInArea();
		
		

		for(String aa:machin)
		{
			EntityPlayerMP p = Tools.getPlayer(aa);
			if(p!=null)
			{
				PlayerProfile plap = PlayerProfile.getPlayerProfile(aa, false);
				plap.getQuestProfile().forceupdateScoreBoard(p);
				Pixelquest.generalConfig.getDungeonHub().teleportPlayer(p);
			}
		}
	}
	
	
	public DungeonGame getCurrentGame() {
		return currentGame;
	}



	public void setCurrentGame(DungeonGame currentGame) {
		this.currentGame = currentGame;
	}



	public ArrayList<SpawnLocation> getSpawnsLocation() {
		return spawnsLocation;
	}



	public Optional<SpawnLocation> getSpawnLocationByName(String name)
	{
		for(SpawnLocation sl:spawnsLocation)
		{
			if(sl.getName().equalsIgnoreCase(name))
			{
				return Optional.of(sl);
			}
		}
		return Optional.empty();
	}
	
	public void addSpawnLocation(SpawnLocation sl)
	{
		spawnsLocation.add(sl);
		DungeonConfig.save(this);
	}
	
	public void removeAllPokemonInArea()
	{
		final Dungeon d = this;
		Predicate<Entity> pre = new Predicate<Entity>()
		{

			@Override
			public boolean apply(Entity e)
			{
				if(d.isInZone(e))
				{
					return true;
				}
				return false;
			}
		};
		
		for(Entity pixel : this.getEntitiesInAera(EntityPixelmon.class, pre))
		{
			pixel.setDead();
		}
		
	}
	
	public void forMaxSpawn()
	{
		for(SpawnLocation sl:spawnsLocation)
		{
			sl.forceAllSpawn();
		}
	}
	
	public String toJson()
	{
		ItemStackSerializer as = new ItemStackSerializer();
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, as).create();
		return gson.toJson(this);
	}

	public static Dungeon fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(ItemStack.class, new ItemStackDeserializer()).create();
		return gson.fromJson(s, Dungeon.class);
	}
}
