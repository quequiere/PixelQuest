package com.quequiere.pixelquest.dungeon.objects;

public class RangedInt {
	
	private int x,y;

	public RangedInt(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getMin()
	{
		return Math.min(x, y);
	}
	
	public int getMax()
	{
		return Math.max(x, y);
	}
	
	public int getAleatInt()
	{
		return this.getMax() + (int) (Math.random() * ((this.getMin() - this.getMax()) + 1));
	}
	
	public boolean isInside(int i)
	{
		if(i>=x && i<=y)
			return true;
		return false;
	}
	

}
