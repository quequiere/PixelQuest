package com.quequiere.pixelquest.dungeon.objects;

import java.util.Optional;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.quequiere.pixelquest.Location;

public class LinkedLocation
{

	private Location loc;
	private transient EntityPixelmon pokemon;
	
	
	public LinkedLocation(Location loc)
	{
		this.loc = loc;
	}


	public Optional<EntityPixelmon> getPokemon()
	{
		if(pokemon==null)
		{
			return Optional.empty();
		}
		return Optional.of(pokemon);
	}


	public void setPokemon(EntityPixelmon pokemon)
	{
		this.removePokemon();
		this.pokemon = pokemon;
	}


	public Location getLoc()
	{
		return loc;
	}
	
	public void removePokemon()
	{
		Optional<EntityPixelmon> poke = this.getPokemon();
		if(poke.isPresent())
		{
			poke.get().specialDungeonAvoidDespawn=false;
			poke.get().setDead();
		}
		
		this.pokemon=null;
	}
	
	public boolean hasAvailablePokemon()
	{
		if(this.getPokemon().isPresent())
		{
			if(this.getPokemon().get().isEntityAlive())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	
	
	
}
