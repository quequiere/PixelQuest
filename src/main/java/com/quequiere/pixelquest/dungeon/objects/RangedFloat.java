package com.quequiere.pixelquest.dungeon.objects;

public class RangedFloat {
	
	private float x,y;

	public RangedFloat(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float getMin()
	{
		return Math.min(x, y);
	}
	
	public float getMax()
	{
		return Math.max(x, y);
	}
	
	public float getAleatFloat()
	{
		return (float) (this.getMax() +  (Math.random() * ((this.getMin() - this.getMax()) + 1)));
	}
	
	

}
