package com.quequiere.pixelquest.travel.entity;

import com.pixelmonmod.pixelmon.entities.pixelmon.Entity7HasAI;

import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class AIFlyingPersistentModified extends EntityAIBase
{
	Entity7HasAI pixelmon;
	Path path;
	EntityPlayerMP p;
	Entity chair;
	AxisAlignedBB bounding;

	float speed;

	double lastDistance = -1;
	long lastSlow = 0;

	public AIFlyingPersistentModified(Entity7HasAI entity, Path patp, EntityPlayerMP p, Entity chair, float speed)
	{
		this.pixelmon = entity;
		this.path = patp;
		this.speed = speed;
		this.p = p;
		this.chair = chair;
		this.pixelmon.getNavigator().setPath(path, 1024);

		bounding = p.getEntityBoundingBox();

	}

	public boolean shouldExecute()
	{
		return true;
	}

	public boolean continueExecuting()
	{
		this.pixelmon.renderYawOffset += (-((float) Math.atan2(this.pixelmon.motionX, this.pixelmon.motionZ)) * 180.0F / 3.1415927F - this.pixelmon.renderYawOffset) * 0.1F;
		return true;
	}

	// 0.02 signifie max de la vitesse en cours pour vitesse = 1
	public void updateTask()
	{

		double maxV = Math.max(Math.abs(this.pixelmon.motionX), Math.abs(this.pixelmon.motionZ));
		boolean xBetter = false;
		if (Math.abs(this.pixelmon.motionX) > Math.abs(this.pixelmon.motionZ))
		{
			xBetter = true;
		}

		double currentSpeed = maxV;
		// System.out.println(currentSpeed);

		if (this.pixelmon.battleController != null || path == null)
		{
			super.updateTask();
		}
		else
		{
			if (!path.isFinished())
			{

				double maxSpeed = (this.speed / 10.0) * 1.5;

				PathPoint target = this.path.getPathPointFromIndex(this.path.getCurrentPathIndex());
				PathPoint pokemonLocation = new PathPoint((int) this.pixelmon.posX, (int) this.pixelmon.posY, (int) this.pixelmon.posZ);
				float dist = pokemonLocation.distanceTo(target);

				boolean isGoodDirection = true;

				double minSpeed = 0.2;
				double palier = 0.2f;

				// System.out.println(dist);

				if (dist > lastDistance)
				{
					isGoodDirection = false;
					// System.out.println("Wrong direction !");
				}

				boolean starting = false;

				if (this.path.getCurrentPathIndex() > 0)
				{
					PathPoint last = this.path.getPathPointFromIndex(this.path.getCurrentPathIndex() - 1);
					float distlast = pokemonLocation.distanceTo(last);
					if (distlast <= 5)
					{
						starting = true;
					}
				}

				long diff = System.currentTimeMillis() - lastSlow;

				if (!isGoodDirection)
				{
					if (diff > 500)
					{
						this.pixelmon.motionX *= 0.01;
						this.pixelmon.motionZ *= 0.01;
						lastSlow = System.currentTimeMillis();
					}

				}
				else if (diff < 2000)
				{
					// on le laisse se replacer tranquille
				}
				else if (starting)
				{
					// on le laisse se positionner a l'aise !
				}

				else if (dist <= (10 * maxSpeed))
				{
					if (currentSpeed > minSpeed)
					{
						this.pixelmon.motionX *= palier;
						this.pixelmon.motionZ *= palier;
					}
				}

				else
				{
					if (currentSpeed < maxSpeed)
					{
						if (xBetter)
						{
							double rapport = maxSpeed / Math.abs(this.pixelmon.motionX);
							this.pixelmon.motionX *= rapport;
							this.pixelmon.motionZ *= rapport;
						}
						else
						{
							double rapport = maxSpeed / Math.abs(this.pixelmon.motionZ);
							this.pixelmon.motionX *= rapport;
							this.pixelmon.motionZ *= rapport;
						}

						// System.out.println("----------");
						// System.out.println(" "+this.pixelmon.motionX);
						// System.out.println(" "+this.pixelmon.motionZ);
					}
					else
					{

					}
				}

				PathPoint currentPoint = this.path.getPathPointFromIndex(this.path.getCurrentPathIndex());
				p.setEntityBoundingBox(new AxisAlignedBB(0, 0, 0, 0, 0, 0));

				double diffY = this.pixelmon.posY - currentPoint.yCoord;
				if (diffY < 0.5)
				{
					this.pixelmon.setPositionAndUpdate(this.pixelmon.posX, this.pixelmon.posY + 0.2, this.pixelmon.posZ);
				}
				else if (diffY > 0.5)
				{
					this.pixelmon.setPositionAndUpdate(this.pixelmon.posX, this.pixelmon.posY - 0.2, this.pixelmon.posZ);
				}

				lastDistance = dist;
			}
			else
			{
				if (p != null)
				{
					p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Travel finished !"));
					p.setEntityBoundingBox(bounding);
				}
				else
				{

				}

				if (chair != null)
				{
					chair.setDead();
				}

				if (pixelmon != null)
				{
					pixelmon.setDead();
				}

			}

			super.updateTask();
		}
	}

}
