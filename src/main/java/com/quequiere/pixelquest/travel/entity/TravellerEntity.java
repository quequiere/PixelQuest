package com.quequiere.pixelquest.travel.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.world.World;

public class TravellerEntity extends EntityArmorStand
{
	public Entity ep;

	public TravellerEntity(World p_i45854_1_,Entity e)
	{
		super(p_i45854_1_);
		this.setInvisible(true);
		this.ep=e;
		this.setSize(0, 0);
	}
	
	protected void collideWithNearbyEntities()
	{
		
	}
	

	
	public void onUpdate()
	{
		
		setPositionAndUpdate(ep.posX, ep.posY+1, ep.posZ);
	}
	
	
	public void onEntityUpdate()
	{
		setPositionAndUpdate(ep.posX, ep.posY+1, ep.posZ);
	}

	public boolean hasNoGravity()
	{
		return true;
	}
	
	public void moveEntityWithHeading(float p_moveEntityWithHeading_1_, float p_moveEntityWithHeading_2_)
	{
		this.motionY=0;
		this.motionX=0;
		this.motionZ=0;
		setPositionAndUpdate(ep.posX, ep.posY+1, ep.posZ);
	}

}
