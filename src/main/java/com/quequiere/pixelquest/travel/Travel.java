package com.quequiere.pixelquest.travel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.AI.AIFlyingPersistent;
import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.SetNPCData;
import com.pixelmonmod.pixelmon.config.PixelmonEntityList;
import com.pixelmonmod.pixelmon.entities.npcs.NPCShopkeeper;
import com.pixelmonmod.pixelmon.entities.npcs.registry.BaseShopItem;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItem;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopItemWithVariation;
import com.pixelmonmod.pixelmon.entities.npcs.registry.ShopkeeperChat;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.helpers.AIHelper;
import com.pixelmonmod.pixelmon.enums.EnumGui;
import com.pixelmonmod.pixelmon.enums.EnumPokemon;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.player.fly.FlyProfile;
import com.quequiere.pixelquest.travel.entity.AIFlyingPersistentModified;
import com.quequiere.pixelquest.travel.entity.TravellerEntity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathPoint;

public class Travel
{
	private static ArrayList<Travel> loaded = new ArrayList<Travel>();
	public static float priceMetter = 0.01F;

	private ArrayList<Location> locationPoints = new ArrayList<Location>();
	private UUID from;
	private UUID to;
	private String name;
	private int customPrice = -1;

	public Travel(ArrayList<Location> pathPoints, UUID from, UUID to, String name)
	{
		this.locationPoints = pathPoints;
		this.from = from;
		this.to = to;
		this.name = name;
	}
	
	public static ArrayList<UUID> getAllids()
	{
		ArrayList<UUID> temp = new ArrayList<UUID>();
		for (Travel t : loaded)
		{
			if(!temp.contains(t.from))
			{
				temp.add(t.from);
			}
		}
		return temp;
	}

	public static ArrayList<Travel> getTravelByEntity(UUID id)
	{
		ArrayList<Travel> temp = new ArrayList<Travel>();
		for (Travel t : loaded)
		{
			if (t.from.equals(id))
			{
				temp.add(t);
			}
		}
		return temp;
	}

	public static void registerNewTravel(Travel t)
	{
		if (getByName(t.getName()) == null)
		{
			loaded.add(t);
			t.save();
		}
		else
		{
			System.out.println("Error while trying to registry new travel, name conflit: " + t.getName());
		}

	}

	public static Travel getByName(String name)
	{
		for (Travel t : loaded)
		{
			if (t.getName().equals(name))
			{
				return t;
			}
		}
		return null;
	}

	public UUID getFrom()
	{
		return from;
	}

	public UUID getTo()
	{
		return to;
	}

	public String getName()
	{
		return name;
	}

	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public int getCost(EntityPlayerMP p)
	{
		if(customPrice>0)
		{
			return customPrice;
		}
		else
		{
			PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
			FlyProfile fp = pp.getFlyprofile();
			
			Path path = this.getPath();
			PathPoint first= path.getPathPointFromIndex(0);
			PathPoint last = path.getFinalPathPoint();
			double metter = first.distanceTo(last);
			
			double metterPrice = priceMetter*Math.pow(1.8F, fp.getFlySpeed());
			double price = metter*metterPrice;
			int fiprice = (int) Math.round(price);

			return fiprice;
		}
		
	}

	public static Travel fromJson(String s)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.fromJson(s, Travel.class);
	}

	public static void loadAllTravel()
	{
		System.out.println("Loadgin travel ...");
		loaded.clear();
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/travel/");
		folder.mkdirs();
		for (File f : folder.listFiles())
		{
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				Travel t = fromJson(everything);
				System.out.println("Loaded: " + t.getName());
				loaded.add(t);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		System.out.println(loaded.size() + " travels loaded !");

	}

	private void save()
	{
		BufferedWriter writer = null;
		try
		{

			File file = new File(Pixelquest.folder.getAbsolutePath() + "/travel/" + this.getName() + ".json");
			if (!file.exists())
			{
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public static void displayGui(EntityPlayerMP p, NPCShopkeeper shop)
	{
		shop.setName("Flight master");
		Pixelmon.network.sendTo(new SetNPCData("Flight master", new ShopkeeperChat("Do you need to go somewhere ?", "Bye !"), getItemList(p, shop), new ArrayList<ShopItemWithVariation>()), p);
		p.openGui(Pixelmon.instance, EnumGui.Shopkeeper.getIndex().intValue(), p.getEntityWorld(), 0, 0, 0);
	}

	public static ArrayList<ShopItemWithVariation> getItemList(EntityPlayerMP p, NPCShopkeeper shop)
	{
		ArrayList<ShopItemWithVariation> list = new ArrayList<ShopItemWithVariation>();
		for (Travel t : Travel.getTravelByEntity(shop.getPersistentID()))
		{
			String name = t.getName();

			ItemStack is = new ItemStack(Blocks.STAINED_GLASS_PANE);
			is.setItemDamage(EnumDyeColor.GREEN.getMetadata());
			is.setStackDisplayName("Fly to " + name);
			BaseShopItem base = new BaseShopItem("flyto:" + t.getName(), is, t.getCost(p), -1);
			ShopItem shopitem = new ShopItem(base, 0, 0, false);
			ShopItemWithVariation si = new ShopItemWithVariation(shopitem);
			list.add(si);
		}
		return list;
	}

	public Path getPath()
	{
		PathPoint[] patp = new PathPoint[locationPoints.size()];
		int x = 0;
		for (Location l : locationPoints)
		{
			patp[x] = new PathPoint((int) l.x, (int) l.y, (int) l.z);
			x++;
		}

		Path pa = new Path(patp);
		return pa;
	}

	public void startFly(EntityPlayerMP p)
	{
		EntityPixelmon pokemon = (EntityPixelmon) PixelmonEntityList.createEntityByName(EnumPokemon.Fearow.name, p.worldObj);
		Path path = this.getPath();
		PathPoint firstPos = path.getPathPointFromIndex(0);

		pokemon.setPosition(firstPos.xCoord, firstPos.yCoord, firstPos.zCoord);
		pokemon.canDespawn = false;
		pokemon.setSpawnLocation(pokemon.getDefaultSpawnLocation());
		p.worldObj.spawnEntityInWorld(pokemon);

		AIHelper a = pokemon.getAIHelper();
		EntityAITasks tasks = pokemon.tasks;
		int id = 99;

		for (Object t : tasks.taskEntries.toArray().clone())
		{
			EntityAITaskEntry tt = (EntityAITaskEntry) t;
			if (!(tt.action instanceof AIFlyingPersistent))
			{
				tasks.taskEntries.remove(tt);
				continue;
			}
			else
			{
				tasks.taskEntries.remove(tt);
				id = tt.priority;
				continue;
			}

		}

		// ------------
		Entity es = new TravellerEntity(p.getServerWorld(), pokemon);
		es.setPosition(p.posX, p.posY, p.posZ);
		p.getServerWorld().spawnEntityInWorld(es);

		// --------------
		p.startRiding(es);
		// -----------

		PlayerProfile pp = PlayerProfile.getPlayerProfile(p.getName(), false);
		FlyProfile fp = pp.getFlyprofile();
		
		AIFlyingPersistentModified ai = new AIFlyingPersistentModified(pokemon, path, p, es,fp.getFlySpeed());
		tasks.addTask(id, ai);
	}

}
