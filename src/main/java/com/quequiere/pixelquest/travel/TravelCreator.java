package com.quequiere.pixelquest.travel;

import java.util.ArrayList;
import java.util.UUID;

import com.quequiere.pixelquest.Location;

import net.minecraft.pathfinding.PathPoint;

public class TravelCreator
{

	public ArrayList<Location> pathPoints = new ArrayList<Location>();
	public UUID from;
	public UUID to;
	public String name;

}
