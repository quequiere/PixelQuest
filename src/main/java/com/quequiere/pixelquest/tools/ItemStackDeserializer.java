package com.quequiere.pixelquest.tools;

import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTException;
import net.minecraft.nbt.NBTTagCompound;

public class ItemStackDeserializer implements JsonDeserializer<ItemStack>
{

	@Override
	public ItemStack deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{

		JsonObject jsonO = json.getAsJsonObject();
		JsonElement tagElement = jsonO.get("itemStackViaNbtag");
		Gson g = new Gson();
		JsonArray nbtListJson = g.fromJson(tagElement.toString(), JsonArray.class);
		String nbtStringValue = nbtListJson.get(0).toString().replace("\\", "");
		nbtStringValue = nbtStringValue.replace("?", Character.toString ((char) 167));
		

		StringBuilder myName = new StringBuilder(nbtStringValue);
		myName.deleteCharAt(0);
		myName.deleteCharAt(myName.length() - 1);

		try
		{
			NBTTagCompound nbtreconvert = (NBTTagCompound) JsonToNBT.getTagFromJson(myName.toString());

			ItemStack is = ItemStack.loadItemStackFromNBT(nbtreconvert);

			return is;
		}
		catch (NBTException e)
		{
			e.printStackTrace();
		}
		return null;
	}

}
