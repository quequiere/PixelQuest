package com.quequiere.pixelquest.tools;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.google.common.primitives.Bytes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

public class ItemStackSerializer implements JsonSerializer<ItemStack>
{
    private final static String textBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    private final static char[] alphabetBase64 = textBase64.toCharArray();
    private final static String regexBase64 = "[^" + textBase64 + "=]";

	@Override
	public JsonElement serialize(ItemStack src, Type typeOfSrc, JsonSerializationContext context)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonElement withoutTransientFields = new JsonObject();
		
		NBTTagCompound nbt = new NBTTagCompound();
    	src.writeToNBT(nbt);
		
    	JsonArray list = new JsonArray();
		list.add(gson.toJsonTree(nbt.toString()));
		withoutTransientFields.getAsJsonObject().add("itemStackViaNbtag", list);
    	
    	
		return withoutTransientFields;
	}

	
	

}
