package com.quequiere.pixelquest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.comm.packetHandlers.npc.SetNPCData;
import com.pixelmonmod.pixelmon.enums.EnumGui;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class Tools
{

	public static int getAleatInt(int Min, int Max)
	{
		return ThreadLocalRandom.current().nextInt(Min, Max + 1);
	}

	public static void openGuiMessage(EntityPlayerMP p, String title, ArrayList<String> lines)
	{
		SetNPCData npcda = new SetNPCData(title, lines);
		Pixelmon.network.sendTo(npcda, p);
		p.openGui(Pixelmon.instance, EnumGui.NPCChat.getIndex().intValue(), p.worldObj, -1, 0, 0);
	}

	public static EntityPlayerMP getPlayer(String name)
	{
		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(name);
	}

	public static WorldServer getWorldByName(String name)
	{
		try
		{
			for (WorldServer ws : FMLCommonHandler.instance().getMinecraftServerInstance().worldServers)
			{
				if (name.equals(ws.getWorldInfo().getWorldName()))
				{
					return ws;
				}
			}

		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static List<EntityPlayerMP> getAllPlayers()
	{

		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerList();
	}

	public static Entity getEntityByUUID(UUID id)
	{
		return FMLCommonHandler.instance().getMinecraftServerInstance().getEntityFromUuid(id);
	}

	public static Entity getEntityByUUID(String id)
	{
		return getEntityByUUID(UUID.fromString(id));
	}

	public static ArrayList<String> getPokemons(EntityPlayerMP p)
	{
		ArrayList<String> pnames = new ArrayList<String>();
		Optional optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage((EntityPlayerMP) p);

		if (optstorage.isPresent())
		{
			PlayerStorage storage = (PlayerStorage) optstorage.get();

			NBTTagCompound[] arg1 = storage.partyPokemon;
			int arg2 = arg1.length;

			for (int arg3 = 0; arg3 < arg2; ++arg3)
			{
				NBTTagCompound nbt = arg1[arg3];
				if (nbt != null)
				{
					pnames.add(nbt.getString("Name"));
				}
			}
		}
		return pnames;
	}
}
