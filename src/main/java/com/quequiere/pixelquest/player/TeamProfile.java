package com.quequiere.pixelquest.player;

import java.util.ArrayList;
import java.util.Optional;

import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.dungeon.config.DungeonConfig;
import com.quequiere.pixelquest.dungeon.objects.Dungeon;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;

import net.minecraft.entity.player.EntityPlayerMP;

public class TeamProfile extends PlayerProfile {


	private TeamQuestProfile questProfile;
	private ArrayList<String> players = new ArrayList<String>();
	
	public TeamProfile(String name,ArrayList<String> playerss) {
		super(name);
		for(String pn:playerss)
		{
			players.add(pn);
		}
		//teamprofiles.add(this);
		questProfile = new TeamQuestProfile(this);
	}
	
	
	public ArrayList<EntityPlayerMP> getOnlineTeamPlayers() {
		
		ArrayList<EntityPlayerMP> pl = new ArrayList<EntityPlayerMP>();
		for(String pn:this.getPlayers())
		{
			EntityPlayerMP p = Tools.getPlayer(pn);
			if(p!=null)
			{
				pl.add(p);
			}
		}
		return pl;
		
	}
	
	
	public ArrayList<String> getPlayers() {
		return players;
	}

	@Override
	public QuestPlayerProfile getQuestProfile()
	{
		return questProfile;
	}


	public static Optional<TeamProfile> getByPlayerName(String name)
	{
		for(Dungeon dl:DungeonConfig.loaded)
		{
			if(dl.getCurrentGame()!=null)
			{
				TeamProfile tp = dl.getCurrentGame().getProfile();
				if(tp.getPlayers().contains(name))
				{
					return Optional.of(tp);
				}
			}
		}
		
	
		
		return Optional.empty();
	}
	
	@Override
	public void save()
	{
		
	}

}
