package com.quequiere.pixelquest.player.license;

import com.pixelmonmod.pixelmon.entities.pixelmon.Entity3HasStats;

public enum EnumLicenseType
{
	mountGroundPokemon,
	mountFlyPokemon,
	mountSurfPokemon;

	public static EnumLicenseType getLicenseNeededForPokemon(Entity3HasStats pokemon)
	{
		if (pokemon.baseStats.isRideable)
		{
			if (pokemon.baseStats.canFly)
			{
				return mountFlyPokemon;
			}
			else if (pokemon.baseStats.canSurf)
			{
				return mountFlyPokemon;
			}
			else
			{
				return mountGroundPokemon;
			}
		}
		else
		{

			return null;
		}

	}
}
