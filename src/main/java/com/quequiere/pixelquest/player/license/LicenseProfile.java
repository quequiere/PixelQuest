package com.quequiere.pixelquest.player.license;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.TimeZone;

import com.quequiere.pixelquest.player.PlayerProfile;

import net.minecraft.entity.player.EntityPlayerMP;

public class LicenseProfile
{
	public HashMap<EnumLicenseType, Long> licenses = new HashMap<EnumLicenseType, Long>();
	// type de permis VS expiration

	public boolean hasLicense(EnumLicenseType type, PlayerProfile profile)
	{
		if (licenses.containsKey(type))
		{
			long per = licenses.get(type);

			if (per > System.currentTimeMillis())
			{
				return true;
			}
			else
			{
				licenses.remove(type);
				profile.save();
			}
		}

		return false;
	}

	public void giveLicense(EnumLicenseType type, long minutes, PlayerProfile profile)
	{
		long toAdd = minutes * 60 * 1000;
		if (licenses.containsKey(type))
		{
			long per = licenses.get(type);
			licenses.remove(type);
			licenses.put(type, per + toAdd);
		}
		else
		{
			licenses.put(type, System.currentTimeMillis() + toAdd);
		}

		profile.save();
	}
	
	public String getPerString(EntityPlayerMP p,EnumLicenseType license)
	{
		Timestamp stamp = new Timestamp(licenses.get(license));
		Date date = new Date(stamp.getTime());
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		String formattedDate = sdf.format(date);
		return formattedDate;
	}
}
