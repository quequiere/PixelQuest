package com.quequiere.pixelquest.player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quequiere.pixelquest.Pixelquest;
import com.quequiere.pixelquest.player.bonus.Bonus;
import com.quequiere.pixelquest.player.bonus.BonusDeserializer;
import com.quequiere.pixelquest.player.bonus.BonusProfile;
import com.quequiere.pixelquest.player.bonus.BonusType;
import com.quequiere.pixelquest.player.experience.ExperienceProfile;
import com.quequiere.pixelquest.player.fly.FlyProfile;
import com.quequiere.pixelquest.player.license.LicenseProfile;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.RewardDeserializer;
import com.quequiere.pixelquest.quest.reward.RewardType;

public class PlayerProfile
{

	private static ArrayList<PlayerProfile> loadedProfiles = new ArrayList<PlayerProfile>();

	private String name;
	private QuestPlayerProfile questProfile = new QuestPlayerProfile();
	private ExperienceProfile expProfile = new ExperienceProfile();
	private FlyProfile flyprofile = new FlyProfile();
	private LicenseProfile licenseProfile = new LicenseProfile();
	private BonusProfile bonusProfile;
	
	public PlayerProfile(String name)
	{
		this.name = name;
	}
	
	
	
	public BonusProfile getBonusProfile() {
		if(bonusProfile==null)
		{
			bonusProfile=new BonusProfile();
		}
		return bonusProfile;
	}

	public static Optional<BonusProfile> getBonusProfileByName(String player)
	{
		
		for (PlayerProfile pp : loadedProfiles)
		{
			if(pp==null&&pp.getName().equals(player))
			{
				return Optional.of(pp.getBonusProfile());
			}
		}
		return Optional.empty();
	}

	public static void unloadProfile(String playerName)
	{
		PlayerProfile select = null;
		
		for (PlayerProfile pp : loadedProfiles)
		{
			if(pp==null)
			{
				System.out.println("Error on a player profil in arraylist but seems to be null ..."+pp);
				continue;
			}
			
			if (playerName.equals(pp.getName()))
			{
				select=pp;
				break;
			}
		}
		
		if(select!=null)
		{
			loadedProfiles.remove(select);
		}

	}

	public static PlayerProfile getPlayerProfile(String playerName,boolean create)
	{
		Optional<TeamProfile> tp = TeamProfile.getByPlayerName(playerName);
		if(tp.isPresent())
		{
			return tp.get();
		}
		
		if(loadedProfiles==null)
		{
			System.out.println("Fatal error on loaded players not existing arraylist ... need to correct that");
		}
		
		for (PlayerProfile pp : loadedProfiles)
		{
			if(pp==null)
			{
				System.out.println("Error on a player profil in arraylist but seems to be null ..."+pp);
				continue;
			}
			
			if (playerName.equals(pp.getName()))
			{
				return pp;
			}
		}

		PlayerProfile pp = null;
		pp = loadFileProfile(playerName);
		
		if(pp==null&&create)
		{
			System.out.println("Creating config for a new player: "+playerName);
			pp = new PlayerProfile(playerName);
			pp.save();
		}
	
		if(pp!=null)
		{
			loadedProfiles.add(pp);
		}
		else
		{
			System.out.println("ERROR trying to add null player to list ! "+create);
		}
		
		return pp;
	}

	public String getName()
	{
		return name;
	}

	public QuestPlayerProfile getQuestProfile()
	{
		return questProfile;
	}
	
	public LicenseProfile getlicenseProfile()
	{
		if(licenseProfile==null)
		{
			licenseProfile=new LicenseProfile();
			this.save();
		}
		
		return licenseProfile;
	}
	
	public FlyProfile getFlyprofile()
	{
		if(flyprofile==null)
		{
			flyprofile=new FlyProfile();
			this.save();
		}
		
		return flyprofile;
	}

	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}

	public static PlayerProfile fromJson(String s)
	{
		
		BonusDeserializer rewardDeserializer = new BonusDeserializer("type");
		for (BonusType rt : BonusType.values())
		{
			rewardDeserializer.registerRaceType(rt.cla.getSimpleName(), rt.cla);
		}


		
		Gson gson = new GsonBuilder().registerTypeAdapter(Bonus.class, rewardDeserializer).setPrettyPrinting().create();
		return gson.fromJson(s, PlayerProfile.class);
	}

	private static PlayerProfile loadFileProfile(String name)
	{
		File folder = new File(Pixelquest.folder.getAbsolutePath() + "/profile/");
		folder.mkdirs();
		File f = new File(folder.getAbsolutePath()+"/"+name+".json");
		
		if(f.exists())
		{
			System.out.println("Loading "+name+" profile from files !");
			BufferedReader br = null;
			try
			{
				br = new BufferedReader(new FileReader(f));
				StringBuilder sb = new StringBuilder();
				String line = br.readLine();

				while (line != null)
				{
					sb.append(line);
					sb.append(System.lineSeparator());
					line = br.readLine();
				}
				String everything = sb.toString();
				
				PlayerProfile pp =null;
				
				if(everything==null|everything.equals(""))
				{
					System.out.println("Empty file ...");
					return null;
				}
				else
				{
					pp = PlayerProfile.fromJson(everything);
				}
				
				return pp;
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			System.out.println("Cant find: "+f.getAbsolutePath());
			try
			{
				f.createNewFile();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return null;
	}

	public void save()
	{
		BufferedWriter writer = null;
		try
		{

			File folder = new File(Pixelquest.folder.getAbsolutePath() + "/profile");
			folder.mkdirs();
			File file = new File(folder.getAbsolutePath() +"/"+ this.getName() + ".json");
			if (!file.exists())
			{
				file.createNewFile();
			}
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(this.toJson());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				writer.close();
			}
			catch (Exception e)
			{
			}
		}

	}

	public ExperienceProfile getExpProfile()
	{
		if(this.expProfile==null)
		{
			this.expProfile=new ExperienceProfile();
		}
		return expProfile;
	}
	
	

}
