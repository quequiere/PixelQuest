package com.quequiere.pixelquest.player;

import java.util.ArrayList;
import java.util.HashMap;

import com.quequiere.camerastudio.CameraStudio;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.notification.Notification;
import com.quequiere.pixelquest.player.quest.GoalAvancement;
import com.quequiere.pixelquest.player.quest.QuestPlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.reward.PercentedRewardObject;
import com.quequiere.pixelquest.quest.reward.Reward;
import com.quequiere.pixelquest.quest.reward.type.CommandServerReward;
import com.quequiere.pixelquest.quest.reward.type.PercentedReward;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class TeamQuestProfile extends QuestPlayerProfile
{

	private TeamProfile team;

	public TeamQuestProfile(TeamProfile t)
	{
		super();
		this.team = t;
	}

	@Override
	public void checkTimerQuest(EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		boolean modif = false;
		for (Quest q : this.getFollowedQuest())
		{
			if (this.isTimerFinish(q))
			{
				this.removeQuest(q, p, false, true);
				modif = true;
				for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
				{
					Notification.addNotification(pp, 7 * 1000, TextFormatting.RED + q.getName() + " quest is finished.", "You taked too mush time :( !");

					for (Reward r : q.getFailRewards())
					{
						r.give(p);
					}
				}

			}
		}

		if (modif)
		{
			profile.save();
		}
	}

	@Override
	public void addQuest(Quest q, EntityPlayerMP p, boolean force)
	{

		if (quests.containsKey(q.getId()))
		{
			System.out.println("Double ajout de quete: " + q.getId());
			for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
			{
				pp.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You already have this quest from: " + p.getName()));
			}

			return;
		}

		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

		if (!profile.getQuestProfile().canDoQuest(q) && !force)
		{
			// p.addChatMessage(new TextComponentString(TextFormatting.GREEN +
			// "You already did this quest"));
			return;
		}

		HashMap<Integer, GoalAvancement> goalsAvancement = new HashMap<Integer, GoalAvancement>();

		for (Goal g : q.getGoals())
		{
			goalsAvancement.put(g.getInternalId(), new GoalAvancement());
		}

		if (q.getIntroCinematicName() != null && !q.getIntroCinematicName().equals(""))
		{
			for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
			{
				CameraStudio.playCinematic(pp, q.getIntroCinematicName());
			}

		}
		else if (q.getIntro().size() > 0)
		{
			ArrayList<String> lines = (ArrayList<String>) q.getIntro().clone();
			lines.add("To see this again use /quest info " + q.getName());

			for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
			{
				Tools.openGuiMessage(pp, TextFormatting.RED + "Quest " + q.getName() + " info", lines);
			}

		}

		for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
		{
			pp.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Started new quest: " + q.getName() + "(id :" + q.getId() + ")"));
		}

		quests.put(q.getId(), goalsAvancement);

		this.refreshQuest(null);
	}

	@Override
	protected void checkFinishedQuest(final EntityPlayerMP p)
	{
		ArrayList<Quest> finishedQuest = new ArrayList<Quest>();
		for (Quest q : getFollowedQuest())
		{
			boolean finished = true;

			for (Goal g : q.getGoals())
			{
				GoalAvancement avancement = quests.get(q.getId()).get(g.getInternalId());
				if (!avancement.isFinished())
				{
					finished = false;
					break;
				}
			}

			if (finished)
			{
				finishedQuest.add(q);
			}
		}

		ArrayList<CommandServerReward> alreadyGiveQuest = new ArrayList<CommandServerReward>();

		ArrayList<Reward> toGive = new ArrayList<Reward>();
	

		for (Quest q : finishedQuest)
		{
			this.removeQuest(q, p, true, true);

		
			for (Reward rew : q.getRewardList())
			{

				if (rew.tryToGetWithChance())
				{

					if (rew instanceof CommandServerReward)
					{
						CommandServerReward csr = (CommandServerReward) rew;
						if (csr.getCommand().contains("quest add")||csr.getCommand().contains("q add") || csr.getCommand().contains("iz ") || csr.getCommand().contains("initzone ") || csr.getCommand().contains("dungeon close ") || csr.getCommand().contains("d close"))
						{
							if (!alreadyGiveQuest.contains(csr))
							{
								alreadyGiveQuest.add(csr);
								//toGive.add(rew);
							}
							else
							{
								System.out.println("debug: saut dans reward");
							}
						}
						else
						{
							toGive.add(rew);
						}
					}
					else if (rew instanceof PercentedReward)
					{
						PercentedReward pr = (PercentedReward) rew;

						int rand = Tools.getAleatInt(0, 100);

						for (PercentedRewardObject ri : pr.getListe())
						{
							if (ri.range.isInside(rand))
							{
								Reward r = ri.reward;

								if (r instanceof CommandServerReward)
								{
									CommandServerReward csr = (CommandServerReward) r;
									if (csr.getCommand().contains("quest add")||csr.getCommand().contains("q add") || csr.getCommand().contains("iz ") || csr.getCommand().contains("initzone ") || csr.getCommand().contains("dungeon close ") || csr.getCommand().contains("d close"))
									{
										if (!alreadyGiveQuest.contains(csr))
										{
											alreadyGiveQuest.add(csr);
											//toGive.add(r);
										}
										else
										{
											System.out.println("debug2: saut dans reward");
										}
									}
									else
									{
										toGive.add(r);
									}

									break;
								}
								else
								{
									toGive.add(r);
									break;
								}

							}
						}

					}
					else
					{
						toGive.add(rew);
					}

				}

			}
			
			
			for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
			{
				Notification.addNotification(pp, 7 * 1000, TextFormatting.GREEN + q.getName() + " quest is finished by " + p.getName());
				for(Reward r:toGive)
				{
					r.give(pp);
					System.out.println("Debug: giving "+r.getClass()+"==>"+pp.getName());
				}
				
				for(Reward r:alreadyGiveQuest)
				{
					System.out.println("Debug: giving special "+r.getClass()+"==>"+pp.getName());
					r.give(pp);
				}
				alreadyGiveQuest.clear();
			}
			
			

		}

		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		profile.save();
	}

	@Override
	public void refreshQuest(EntityPlayerMP p)
	{
		for (EntityPlayerMP pp : this.team.getOnlineTeamPlayers())
		{
			this.checkTimerQuest(pp);
			this.forceupdateScoreBoard(pp);
			this.checkFinishedQuest(pp);
		}

	}

}
