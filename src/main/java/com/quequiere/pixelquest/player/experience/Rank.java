package com.quequiere.pixelquest.player.experience;

public enum Rank
{
	Trainer(0,14), Expert(15,29), Tutor(30,59), Master(60,99), Diamond(100,999),UnknowRank(-1,-1);

	private int start, end;

	Rank(int start,int end)
	{
		this.start=start;
		this.end=end;
	}

	public int getStart()
	{
		return start;
	}

	public int getEnd()
	{
		return end;
	}

	public static Rank getRankByLvl(int level)
	{
		for(Rank r:Rank.values())
		{
			if(r.getStart()<=level&&level<=r.getEnd())
			{
				return r;
			}
		}
		
		return UnknowRank;
	}

}
