package com.quequiere.pixelquest.player.experience;

import java.util.ArrayList;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.notification.Notification;
import com.quequiere.pixelquest.player.PlayerProfile;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class ExperienceProfile
{
	private int expQuest = 0;
	private int expBattle = 0;

	public int getExpQuest()
	{
		return expQuest;
	}

	private void setExpQuest(int expQuest)
	{
		this.expQuest = expQuest;
	}

	public int getExpBattle()
	{
		return expBattle;
	}

	private void setExpBattle(int expBattle)
	{
		this.expBattle = expBattle;
	}

	public void addExpBattle(int expBattle, EntityPlayerMP p, PlayerProfile pp)
	{
		this.setExpBattle(this.getExpBattle() + expBattle);

		if (p != null)
		{
			Notification.addNotification(p, TextFormatting.GREEN + "You win " + expBattle + " expBattle !", 7000);
		}
		pp.save();
	}

	public void addExpQuest(int expQuest, EntityPlayerMP p, PlayerProfile pp)
	{
		
		String rankOld = Rank.getRankByLvl(this.getTotalLvl()).name();

		this.setExpQuest(this.getExpQuest() + expQuest);
		
		String ranknew = Rank.getRankByLvl(this.getTotalLvl()).name();
		
		if(!ranknew.equals(rankOld))
		{
			for(EntityPlayerMP pl:Tools.getAllPlayers())
			{
				pl.addChatMessage(new TextComponentString(TextFormatting.GREEN + ""+pp.getName()+" has been promoted to "+TextFormatting.AQUA+ranknew));
			}
		}

		if (p != null)
		{
			Notification.addNotification(p, TextFormatting.GREEN + "You win " + expQuest + " expQuest !", 7000);
		}
		pp.save();
	}



	public static int getLvl(int exp)
	{
		int lvl = (int) Math.pow(exp, 1.0/3.0);
		return lvl;
	}
	
	
	
	public static int getExpToNextLevel(int exp)
	{
		int currentLvl = getLvl(exp);
		double totalExpNextLvl = Math.pow(currentLvl+1.0, 3.0);
		int diff = (int) (totalExpNextLvl-exp);
		return diff;
	}

	public int getTotalLvl()
	{
		int tot =  getLvl(this.getExpQuest()) + getLvl(this.getExpBattle());
		//double moy = tot/2.0;
		//return (int) moy;
		return tot;
	}


	public static void display(PlayerProfile pp, EntityPlayerMP sendTo)
	{
		ArrayList<String> lines = new ArrayList<String>();
		EntityPlayerMP p = Tools.getPlayer(pp.getName());

		
		String rank = Rank.getRankByLvl(pp.getExpProfile().getTotalLvl()).name();
		
		lines.add("Player level: " + TextFormatting.GREEN + pp.getExpProfile().getTotalLvl() + TextFormatting.WHITE + " Rank: " + TextFormatting.RED + rank);
		lines.add("Lvl quest: " + TextFormatting.GREEN + getLvl(pp.getExpProfile().getExpQuest()) + TextFormatting.WHITE + " Exp battle: " + TextFormatting.RED + getLvl(pp.getExpProfile().getExpBattle()));
		lines.add("Needed exp quest next lvl: " + TextFormatting.GREEN + getExpToNextLevel(pp.getExpProfile().getExpQuest()) + TextFormatting.WHITE + " Needed exp battle next lvl: " + TextFormatting.RED + getExpToNextLevel(pp.getExpProfile().getExpBattle()));

		if (p != null)
		{

			String ps = "";
			for (String poke : Tools.getPokemons(p))
			{
				ps += poke + ", ";
			}
			lines.add("Current pokemon: " + TextFormatting.GREEN + ps);
		}

		Tools.openGuiMessage(sendTo, TextFormatting.RED + "Profile: " + pp.getName(), lines);
	}

}
