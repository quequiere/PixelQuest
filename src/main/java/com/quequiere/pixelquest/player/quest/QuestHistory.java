package com.quequiere.pixelquest.player.quest;

public class QuestHistory
{
	private int questId;
	private long endedTimeInMS;
	private boolean success;
	public QuestHistory(int questId, boolean success)
	{
		this.endedTimeInMS=System.currentTimeMillis();
		this.questId = questId;
		this.success = success;
	}
	public int getQuestId()
	{
		return questId;
	}
	public long getEndedTimeInMS()
	{
		return endedTimeInMS;
	}
	public boolean isSuccess()
	{
		return success;
	}
	
	

}
