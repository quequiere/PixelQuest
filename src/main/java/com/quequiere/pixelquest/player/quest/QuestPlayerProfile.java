package com.quequiere.pixelquest.player.quest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.client.gui.custom.overlays.ScoreboardLocation;
import com.pixelmonmod.pixelmon.comm.packetHandlers.customOverlays.CustomScoreboardDisplayPacket;
import com.pixelmonmod.pixelmon.comm.packetHandlers.customOverlays.CustomScoreboardUpdatePacket;
import com.quequiere.camerastudio.CameraStudio;
import com.quequiere.pixelquest.Location;
import com.quequiere.pixelquest.Tools;
import com.quequiere.pixelquest.command.QuestAdminDisplayCommand;
import com.quequiere.pixelquest.notification.Notification;
import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.QuestTimerBeforeRepeat;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.GoalRepeatable;
import com.quequiere.pixelquest.quest.goal.type.LocationGoal;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;
import com.quequiere.pixelquest.quest.reward.Reward;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class QuestPlayerProfile
{

	// quetes Et Avanement
	protected HashMap<Integer, HashMap<Integer, GoalAvancement>> quests = new HashMap<Integer, HashMap<Integer, GoalAvancement>>();
	protected ArrayList<QuestHistory> history = new ArrayList<QuestHistory>();
	protected boolean hideQuest = false;

	protected ArrayList<Integer> hiddedQuest = new ArrayList<Integer>();

	public void addQuest(Quest q, EntityPlayerMP p, boolean force)
	{

		if (quests.containsKey(q.getId()))
		{
			System.out.println("Double ajout de quete: " + q.getId());
			p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "You already have this quest"));
			return;
		}

		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);

		if (!profile.getQuestProfile().canDoQuest(q) && !force)
		{
			// p.addChatMessage(new TextComponentString(TextFormatting.GREEN +
			// "You already did this quest"));
			return;
		}

		HashMap<Integer, GoalAvancement> goalsAvancement = new HashMap<Integer, GoalAvancement>();

		for (Goal g : q.getGoals())
		{
			goalsAvancement.put(g.getInternalId(), new GoalAvancement());
		}

		if (q.getIntroCinematicName() != null && !q.getIntroCinematicName().equals(""))
		{
			CameraStudio.playCinematic(p, q.getIntroCinematicName());
		}
		else if (q.getIntro().size() > 0)
		{
			ArrayList<String> lines = (ArrayList<String>) q.getIntro().clone();
			lines.add("To see this again use /quest info " + q.getName());
			Tools.openGuiMessage(p, TextFormatting.RED + "Quest " + q.getName() + " info", lines);
		}

		p.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Started new quest: " + q.getName() + "(id :" + q.getId() + ")"));
		quests.put(q.getId(), goalsAvancement);
		this.refreshQuest(p);

		profile.save();
	}

	public boolean hasPrerequis(Quest q)
	{
		if (q == null)
		{
			System.out.println("Error no 65565");
			return false;
		}

		if (q.getPreRequis() == null)
		{
			System.out.println("Error no 885486");
		}

		if (q.getPreRequis().size() < 1)
		{
			return true;
		}

		for (Integer id : q.getPreRequis())
		{
			if (!this.hasWinQuestOneTime(Quest.getQuestById(id)))
			{
				return false;
			}
		}

		return true;
	}

	public void removeAll(EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		ArrayList<Integer> torem = new ArrayList<Integer>();

		for (Integer id : quests.keySet())
		{
			Quest q = Quest.getQuestById(id);
			if (q.isCanRemove())
			{
				torem.add(id);
			}
		}

		for (int i : torem)
		{
			quests.remove(i);
		}

		this.refreshQuest(p);
		profile.save();
	}

	public void removeQuest(Quest q, EntityPlayerMP p, boolean success, boolean registry)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		quests.remove(q.getId());
		if (registry)
		{
			this.logQuest(success, q);
		}
		this.refreshQuest(p);
		profile.save();
	}

	public boolean hasQuest(Quest q)
	{
		return hasQuest(q.getId());
	}

	public boolean hasQuest(int id)
	{
		if (quests.containsKey(id))
		{
			return true;
		}
		return false;
	}

	public GoalAvancement getGoalAvancementForQuest(int questId, int goalId)
	{
		if (quests.get(questId) == null)
		{
			System.out.println("Can't goal avancement with this quest id: " + questId + " Size: " + quests.size());
			for (Integer i : quests.keySet())
			{
				System.out.println("QuestAvancement id: " + i);
			}
			System.out.println("Injected a new one !");

			Quest q = Quest.getQuestById(questId);

			if (q.getName().equals("Starter"))
			{
				System.out.println("special quest no create bypass");
				return null;
			}

			HashMap<Integer, GoalAvancement> goalsAvancement = new HashMap<Integer, GoalAvancement>();
			for (Goal g : q.getGoals())
			{
				goalsAvancement.put(g.getInternalId(), new GoalAvancement());
			}

			quests.put(questId, goalsAvancement);

			GoalAvancement gaa = quests.get(questId).get(goalId);

			if (gaa == null)
			{
				System.out.println("Fatal big error, modified ? : qid: " + questId + " goalid:" + goalId);
				return null;
			}

			return gaa;
		}

		if (quests.get(questId) == null)
		{
			System.out.println("Error, a goalavancement can't be founded !");
		}

		return quests.get(questId).get(goalId);
	}

	public ArrayList<Quest> getFollowedQuest()
	{
		ArrayList<Quest> liste = new ArrayList<Quest>();

		for (int id : this.quests.keySet())
		{
			Quest q = Quest.getQuestById(id);

			if (q == null)
			{
				System.out.println("Fatal error, can't find quest id: " + id);
				this.quests.remove(id);
			}
			else
			{
				liste.add(q);
			}

		}
		return liste;
	}

	public void refreshQuest(EntityPlayerMP p)
	{
		this.checkTimerQuest(p);
		this.forceupdateScoreBoard(p);
		this.checkFinishedQuest(p);
	}

	protected void checkFinishedQuest(final EntityPlayerMP p)
	{
		ArrayList<Quest> finishedQuest = new ArrayList<Quest>();
		for (Quest q : getFollowedQuest())
		{
			boolean finished = true;

			for (Goal g : q.getGoals())
			{
				GoalAvancement avancement = quests.get(q.getId()).get(g.getInternalId());
				if (!avancement.isFinished())
				{
					finished = false;
					break;
				}
			}

			if (finished)
			{
				finishedQuest.add(q);
			}
		}

		// final ArrayList<String> endMessage = new ArrayList<String>();

		for (Quest q : finishedQuest)
		{
			this.removeQuest(q, p, true, true);
			Notification.addNotification(p, 7 * 1000, TextFormatting.GREEN + q.getName() + " quest is finished.");
			// endMessage.add("You have successfully completed the quest: " +
			// q.getName());
			for (Reward rew : q.getRewardList())
			{
				if (rew.tryToGetWithChance())
				{
					rew.give(p);
				}

			}
		}

		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		profile.save();
	}

	public void forceupdateScoreBoard(EntityPlayerMP sendTo, EntityPlayerMP playerToDisplay)
	{
		QuestPlayerProfile.updateScoreBoard(sendTo, playerToDisplay);
	}

	public void forceupdateScoreBoard(EntityPlayerMP p)
	{

		if (QuestAdminDisplayCommand.displayerMapDispatcher.containsKey(p.getName()))
		{
			// on annule tout l'envois des quetes vers un admin qui bind
			// quelqu'un d'autre !
			return;
		}

		if (QuestAdminDisplayCommand.displayerMapDispatcher.values().contains(p.getName()))
		{

			for (Object admino : QuestAdminDisplayCommand.displayerMapDispatcher.keySet().toArray())
			{
				String admin = (String) admino;
				// System.out.println(admin);

				if (QuestAdminDisplayCommand.displayerMapDispatcher.get(admin).equals(p.getName()))
				{
					EntityPlayerMP adminP = Tools.getPlayer(admin);
					if (admin != null)
					{
						this.forceupdateScoreBoard(adminP, p);
					}
				}

			}
		}

		this.forceupdateScoreBoard(p, p);

	}

	private static void updateScoreBoard(EntityPlayerMP sendTo, EntityPlayerMP displayedPlayer)
	{
		try
		{

			ArrayList<String> tittle = new ArrayList<String>();
			ArrayList<String> score = new ArrayList<String>();

			QuestPlayerProfile questProfile = PlayerProfile.getPlayerProfile(displayedPlayer.getName(), false).getQuestProfile();

			if (!questProfile.isHideQuest())
			{
				for (Quest q : questProfile.getFollowedQuest())
				{
					if (questProfile.isHidenQuest(q))
					{
						continue;
					}

					boolean tittleDisplayed = false;
					for (Goal g : q.getGoals())
					{

						GoalAvancement avancement = questProfile.getQuestsAvancementMap().get(q.getId()).get(g.getInternalId());
						if (!avancement.isFinished() && avancement.canUpdateWithPriority(q, g, displayedPlayer))
						{
							if (!tittleDisplayed)
							{
								if (q.getTimerSecond() > -1)
								{
									long diff = q.getTimerSecond() - questProfile.getTakedTimeInSec(q);
									diff *= 1000;

									long second = (diff / 1000) % 60;
									long minute = (diff / (1000 * 60)) % 60;
									long hour = (diff / (1000 * 60 * 60)) % 24;

									String time = String.format("%02d:%02d:%02d", hour, minute, second);
									tittle.add(TextFormatting.BOLD + "" + TextFormatting.GREEN + q.getName() + TextFormatting.RESET + " " + time);
								}
								else
								{
									tittle.add(TextFormatting.BOLD + "" + TextFormatting.GREEN + q.getName());
								}

								score.add("");
								tittleDisplayed = true;
							}

							if (g.getPriority() == -1)
							{
								tittle.add(g.getDisplayName(displayedPlayer));
							}
							else
							{
								tittle.add(g.getPriority() + ". " + g.getDisplayName(displayedPlayer));
							}

							if (g instanceof GoalRepeatable)
							{
								GoalRepeatable reap = (GoalRepeatable) g;
								String sco = avancement.getCount() + "/" + reap.getCount();
								score.add(sco);
							}
							else if (g instanceof LocationGoal)
							{
								LocationGoal lg = (LocationGoal) g;
								Location ploc = new Location(displayedPlayer);

								if (lg.getLoc().getWorld().equals(ploc.getWorld()))
								{
									score.add("" + Math.round(ploc.getDistance(lg.getLoc().getX(), lg.getLoc().getY(), lg.getLoc().getZ())) + " m");
								}
								else
								{
									score.add(" go to world: " + lg.getLoc().getWorldName());
								}

							}
							else if (g instanceof InteractEntity)
							{
								InteractEntity lg = (InteractEntity) g;
								if (lg.getEntity() == null)
								{
									score.add("Not loaded entity");
								}
								else
								{
									BlockPos ploc = new BlockPos(displayedPlayer);
									BlockPos eloc = lg.getEntity().getPosition();

									if (displayedPlayer.worldObj.equals(lg.getEntity().worldObj))
									{
										score.add("" + Math.round(ploc.getDistance(eloc.getX(), eloc.getY(), eloc.getZ())) + " m");
									}
									else
									{
										score.add(" go to world: " + lg.getEntity().worldObj.getWorldInfo().getWorldName());
									}

								}

							}
							else if (g instanceof FightEntity)
							{

								FightEntity lg = (FightEntity) g;
								if (lg.getEntity() == null)
								{
									score.add("Not loaded entity");
								}
								else
								{
									BlockPos ploc = new BlockPos(displayedPlayer);
									BlockPos eloc = lg.getEntity().getPosition();

									if (displayedPlayer.worldObj.equals(lg.getEntity().worldObj))
									{
										score.add("" + Math.round(ploc.getDistance(eloc.getX(), eloc.getY(), eloc.getZ())) + " m");
									}
									else
									{
										score.add(" go to world: " + lg.getEntity().worldObj.getWorldInfo().getWorldName());
									}

								}

							}
							else
							{
								score.add("Dev this!");
							}

						}
					}
				}
			}

			if (tittle.size() <= 0)
			{
				CustomScoreboardDisplayPacket c = new CustomScoreboardDisplayPacket(ScoreboardLocation.RIGHT_TOP, false);
				Pixelmon.network.sendTo(c, sendTo);
			}
			else
			{
				CustomScoreboardUpdatePacket up = new CustomScoreboardUpdatePacket("Current quest", tittle, score);
				Pixelmon.network.sendTo(up, sendTo);
				CustomScoreboardDisplayPacket c = new CustomScoreboardDisplayPacket(ScoreboardLocation.RIGHT_TOP, true);
				Pixelmon.network.sendTo(c, sendTo);
			}

		}
		catch (RuntimeException e)
		{
			System.out.println("error for: "+sendTo+" / "+ displayedPlayer);
		}
	}

	public void checkTimerQuest(EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(), false);
		boolean modif = false;
		for (Quest q : this.getFollowedQuest())
		{
			if (this.isTimerFinish(q))
			{
				this.removeQuest(q, p, false, true);
				modif = true;
				Notification.addNotification(p, 7 * 1000, TextFormatting.RED + q.getName() + " quest is finished.", "You taked too mush time :( !");
				for (Reward r : q.getFailRewards())
				{
					r.give(p);
				}
			}
		}

		if (modif)
		{
			profile.save();
		}
	}

	public long getTakedTimeInSec(Quest q)
	{
		GoalAvancement ga = (GoalAvancement) this.quests.get(q.getId()).values().toArray()[0];
		long takedTimeInsec = (System.currentTimeMillis() - ga.getStartedTime()) / 1000;
		return takedTimeInsec;
	}

	public boolean isTimerFinish(Quest q)
	{
		if (q.getTimerSecond() == -1)
		{
			return false;
		}

		if (this.getTakedTimeInSec(q) > q.getTimerSecond())
		{
			return true;
		}
		return false;
	}

	public void logQuest(boolean success, Quest q)
	{
		this.history.add(new QuestHistory(q.getId(), success));
	}

	public boolean hasDidtheQuestOneTime(Quest q)
	{
		for (QuestHistory h : this.history)
		{
			if (h.getQuestId() == q.getId())
			{
				return true;
			}
		}
		return false;
	}

	public boolean hasWinQuestOneTime(Quest q)
	{
		for (QuestHistory h : this.history)
		{
			if (h.getQuestId() == q.getId())
			{
				if (h.isSuccess())
				{
					return true;
				}
			}
		}
		return false;
	}

	public boolean canDoWithLastTime(Quest q)
	{
		if (q.getTimerInSecondBeforeRepeat() == -1 && !q.isHasSharedTimerBeforeRepeat())
		{
			return true;
		}

		long last = -1;
		for (QuestHistory h : this.history)
		{
			if (q.isHasSharedTimerBeforeRepeat())
			{
				QuestTimerBeforeRepeat t = QuestTimerBeforeRepeat.getQuestTimer(q.getId());
				if (t == null)
				{
					System.out.println("PixelQuest error no 453");
					return true;
				}

				for (Integer tq : t.getQuestList())
				{
					if (h.getQuestId() == tq)
					{
						if (h.getEndedTimeInMS() > last)
						{
							last = h.getEndedTimeInMS();
						}
					}
				}
			}
			else
			{
				if (h.getQuestId() == q.getId())
				{
					if (h.getEndedTimeInMS() > last)
					{
						last = h.getEndedTimeInMS();
					}
				}
			}

		}

		long diff = (System.currentTimeMillis() / 1000) - last / 1000;

		if (last == -1)
		{
			return true;
		}
		else if (q.isHasSharedTimerBeforeRepeat())
		{
			QuestTimerBeforeRepeat t = QuestTimerBeforeRepeat.getQuestTimer(q.getId());

			if (diff > t.getTimerInSecondBeforeRepeat())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (diff > q.getTimerInSecondBeforeRepeat())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean canDoQuest(Quest q)
	{

		if (!this.hasPrerequis(q))
		{
			return false;
		}

		if (q.isHasSharedTimerBeforeRepeat())
		{
			QuestTimerBeforeRepeat t = QuestTimerBeforeRepeat.getQuestTimer(q.getId());
			if (t == null)
			{
				System.out.println("Fatal error no 552");
				return false;
			}

			for (Integer id : t.getQuestList())
			{
				if (this.hasQuest(id))
				{
					return false;
				}
			}
		}

		if (this.hasQuest(q.getId()))
		{
			return false;
		}

		if (q.isCanBeRepeated())
		{
			if (q.isCanBeRepeatedIfWin())
			{
				if (this.canDoWithLastTime(q))
				{
					return true;
				}
				else
				{
					return false;
				}

			}
			else if (hasWinQuestOneTime(q))
			{
				return false;
			}
			else
			{
				if (this.canDoWithLastTime(q))
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		else if (this.hasDidtheQuestOneTime(q))
		{
			return false;
		}
		else
		{
			if (this.canDoWithLastTime(q))
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	}

	public boolean isHideQuest()
	{
		return hideQuest;
	}

	public void setHideQuest(boolean hideQuest)
	{
		this.hideQuest = hideQuest;
	}

	private ArrayList<Integer> getHiddedQuest()
	{
		if (hiddedQuest == null)
		{
			hiddedQuest = new ArrayList<Integer>();
		}

		return hiddedQuest;
	}

	public void addQuestHiden(Quest q)
	{
		int id = q.getId();
		if (!this.getHiddedQuest().contains(id))
		{
			this.getHiddedQuest().add(id);
		}

	}

	public void removeQuestHiden(Quest q)
	{
		int id = q.getId();
		this.getHiddedQuest().remove((Object) q.getId());

	}

	public boolean isHidenQuest(Quest q)
	{
		if (this.isHideQuest())
		{
			return true;
		}
		else if (this.getHiddedQuest().contains(q.getId()))
		{
			return true;
		}

		return false;
	}

	public HashMap<Integer, HashMap<Integer, GoalAvancement>> getQuestsAvancementMap()
	{
		return quests;
	}

}
