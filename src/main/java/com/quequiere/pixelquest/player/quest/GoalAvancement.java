package com.quequiere.pixelquest.player.quest;

import com.quequiere.pixelquest.player.PlayerProfile;
import com.quequiere.pixelquest.quest.Quest;
import com.quequiere.pixelquest.quest.goal.Goal;
import com.quequiere.pixelquest.quest.goal.type.FightEntity;
import com.quequiere.pixelquest.quest.goal.type.GoalRepeatable;
import com.quequiere.pixelquest.quest.goal.type.LocationGoal;
import com.quequiere.pixelquest.quest.goal.type.InteractEntity;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class GoalAvancement
{

	private boolean isFinished = false;
	private int count = 0;
	private long startedTime;

	public GoalAvancement()
	{
		this.startedTime = System.currentTimeMillis();
	}

	public boolean canUpdateWithPriority(Quest q, Goal g, EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(),false);
		QuestPlayerProfile questProfile = profile.getQuestProfile();

		int priority = g.getPriority();
		if (priority == -1)
		{
			return true;
		}

		if (priority <= getNexPriorityPossible(q, p))
		{
			return true;
		}

		return false;
	}
	
	public void forceFinish()
	{
		this.isFinished=true;
	}

	public int getNexPriorityPossible(Quest q, EntityPlayerMP p)
	{
		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(),false);
		QuestPlayerProfile questProfile = profile.getQuestProfile();

		int min = -1;

		for (Goal g : q.getGoals())
		{
			GoalAvancement av = questProfile.getGoalAvancementForQuest(q.getId(), g.getInternalId());
			if (av.isFinished())
			{
				if (g.getPriority() > min)
				{
					min = g.getPriority();
				}
			}
		}

		boolean hasFinishedallThisPriority = true;

		int next = -1;

		for (Goal g : q.getGoals())
		{
			GoalAvancement av = questProfile.getGoalAvancementForQuest(q.getId(), g.getInternalId());
			if (g.getPriority() == min)
			{
				if (!av.isFinished())
				{
					hasFinishedallThisPriority = false;
				}
			}
			else
			{
				if (g.getPriority() > min)
				{
					if (!(g.getPriority() > next) || next == -1)
					{
						next = g.getPriority();
					}
				}
			}
		}

		if (!hasFinishedallThisPriority&&min!=-1)
		{
			return min;
		}

		return next;
	}

	public void update(Goal g, EntityPlayerMP p)
	{

		if (!this.canUpdateWithPriority(g.getQuest(g), g, p))
		{
			return;
		}

		if (g instanceof GoalRepeatable)
		{
			GoalRepeatable gr = (GoalRepeatable) g;
			count++;
			if (count >= gr.getCount())
			{
				isFinished = true;
			}
		}
		else if (g instanceof LocationGoal)
		{
			isFinished = true;
		}
		else if (g instanceof InteractEntity)
		{
			isFinished = true;
		}
		else if (g instanceof FightEntity)
		{
			isFinished=true;
		}
		else
		{
			p.addChatComponentMessage(new TextComponentString(TextFormatting.RED + "This need to be dev please report to an admin"));
			System.out.println("ATTENTION A CODER pour ! " + g.getClass());
		}

		if (isFinished)
		{
			p.addChatComponentMessage(new TextComponentString(TextFormatting.BLUE + "You've finished a goal"));
		}

		PlayerProfile profile = PlayerProfile.getPlayerProfile(p.getName(),false);
		QuestPlayerProfile questProfile = profile.getQuestProfile();
		questProfile.refreshQuest(p);
	}
	


	public boolean isFinished()
	{
		return isFinished;
	}

	public int getCount()
	{
		return count;
	}

	public long getStartedTime()
	{
		return startedTime;
	}

}
