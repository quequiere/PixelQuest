package com.quequiere.pixelquest.player.bonus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.quequiere.pixelquest.player.PlayerProfile;

import net.minecraft.entity.player.EntityPlayerMP;

public class BonusProfile {

	private HashMap<Bonus, Long> bonusList = new HashMap<Bonus, Long>();
	
	public void addBonus(EntityPlayerMP p, PlayerProfile pp,long endTime)
	{
		pp.save();
	}
	
	private void checkBonus()
	{
		long now = System.currentTimeMillis();
		ArrayList<Bonus> torem = new ArrayList<Bonus>();
		for(Bonus b:this.bonusList.keySet())
		{
			long end = this.bonusList.get(b);
			if(now>end)
			{
				torem.add(b);
			}
		}
		
		for(Bonus b:torem)
		{
			bonusList.remove(b);
		}
	}
	
	public Set<Bonus> getCurrentBonus()
	{
		this.checkBonus();
		return this.bonusList.keySet();
	}
}
