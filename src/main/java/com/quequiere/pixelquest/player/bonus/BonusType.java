package com.quequiere.pixelquest.player.bonus;

public enum BonusType
{
	AttackBonus(AttackBonus.class),
	ExpBonus(ExpBonus.class),
	CatchBonus(CatchBonus.class),
	EndBattleHealBonus(EndBattleHealBonus.class);
		
	public Class cla;
	
	BonusType(Class<? extends Bonus> c)
	{
		this.cla=c;
	}
}
