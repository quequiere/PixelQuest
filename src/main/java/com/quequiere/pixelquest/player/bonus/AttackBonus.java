package com.quequiere.pixelquest.player.bonus;

public class AttackBonus extends Bonus{

	private int percent=0;
	

	public AttackBonus(int percent) {
		super();
		this.percent = percent;
	}



	public int getPercent() {
		return percent;
	}
	
	

}
