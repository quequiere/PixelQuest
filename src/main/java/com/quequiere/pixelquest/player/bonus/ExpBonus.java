package com.quequiere.pixelquest.player.bonus;

public class ExpBonus extends Bonus{

	private int percent=0;
	

	public ExpBonus(int percent) {
		super();
		this.percent = percent;
	}



	public int getPercent() {
		return percent;
	}
	
	

}
