package com.quequiere.pixelquest.player.bonus;

public class CatchBonus extends Bonus{

	private int percent=0;
	

	public CatchBonus(int percent) {
		super();
		this.percent = percent;
	}



	public int getPercent() {
		return percent;
	}
	
	

}
