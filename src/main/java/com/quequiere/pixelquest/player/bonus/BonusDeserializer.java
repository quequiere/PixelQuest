package com.quequiere.pixelquest.player.bonus;

import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.quequiere.pixelquest.quest.goal.Goal;

public class BonusDeserializer implements JsonDeserializer<Bonus>
{
	
	private final String auctionTypeElementName;
	private final HashMap<String, Class<? extends Bonus>> raceTypeRegistry;
	private final Gson gson;

	public BonusDeserializer(String AuctionTypeElementName)
	{
		this.auctionTypeElementName = AuctionTypeElementName;
		
		
	

		gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Bonus.class, this).create();

		
		
		
		raceTypeRegistry = new HashMap<String, Class<? extends Bonus>>();
	}
	
	public void registerRaceType(String auctionTypeName, Class<? extends Bonus> auctionType)
	{
		raceTypeRegistry.put(auctionTypeName, auctionType);
	}
	
	@Override
	public Bonus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
	{
		JsonObject object = json.getAsJsonObject();
		JsonElement animalTypeElement = object.get(auctionTypeElementName);
		String type = animalTypeElement.getAsString(); 
		
		
		
		
		
		
		Class<? extends Bonus> raceType = raceTypeRegistry.get(type);
		
	
		
		
		Bonus auction = gson.fromJson(object, raceType);
		
		
		return auction;
	}

}
