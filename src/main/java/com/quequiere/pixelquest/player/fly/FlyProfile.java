package com.quequiere.pixelquest.player.fly;

import java.util.ArrayList;

public class FlyProfile
{
	private float flySpeed=2F;
	private ArrayList<String> accessibleFlight = new ArrayList<String>();
	
	
	public float getFlySpeed()
	{
		return flySpeed;
	}


	public void setFlySpeed(float flySpeed)
	{
		this.flySpeed = flySpeed;
	}

	
	
}
